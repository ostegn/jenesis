package com.ostegn.jenesis.junit.base;

/**
 * Main class used to authenticate over the framework test unit.
 * 
 * @author Thiago Ricciardi
 * 
 * @see ServerEnvironmentTest
 */
public class TestCredential implements ITestCredential {
	
	private String username;
	private String password;
	
	/** Creates a new instance of TestCredential. */
	public TestCredential() {};
	/**
	 * Creates a new instance of TestCredential.
	 * @param username The username used to login.
	 * @param password The password used to login.
	 */
	public TestCredential(String username, String password) {
		setUsername(username);
		setPassword(password);
	}
	
	/** Username used to login. */
	public String getUsername() {
		return username;
	}
	/** Username used to login. */
	public void setUsername(String username) {
		this.username = username;
	}
	/** Password used to login. */
	public String getPassword() {
		return password;
	}
	/** Password used to login. */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
