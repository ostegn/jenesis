package com.ostegn.jenesis.junit.base;

/**
 * Main interface used to authenticate over the framework test unit.
 * 
 * @author Thiago Ricciardi
 * 
 * @see ServerEnvironmentTest
 * @see TestCredential
 */
public interface ITestCredential {
	
	/** Username used to login. */
	public String getUsername();
	
	/** Password used to login. */
	public String getPassword();
	
}
