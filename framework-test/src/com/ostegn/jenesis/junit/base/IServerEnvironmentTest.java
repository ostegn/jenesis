package com.ostegn.jenesis.junit.base;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

/**
 * Define the jenesis server environment test
 * @author Thiago Ricciardi
 *
 */
public interface IServerEnvironmentTest {
	
	/**
	 * Used to authenticate the credential over the spring framework.
	 * @return The user credentials in ITestCredential format.
	 */
	ITestCredential getTestCredential();
	
	/**
	 * Used with the anonymous user (not logged in).
	 * @return The authorities an anonymous user has.
	 */
	Collection<? extends GrantedAuthority> getAnonymousRoles();
	
}
