package com.ostegn.jenesis.junit.base;

import java.io.InvalidClassException;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * 
 * Core class for test purposes on this framework.<br><br>
 * Usage:<br>
 * - Extend this class.<br>
 * - Configure the context through {@link ContextConfiguration}<br>
 * - Override the method called getTestCredential with the following signature:<br>
 * {@code public ITestCredential getTestCredential()}<br>
 * This method must return an {@link ITestCredential} that is used to test the system.
 * 
 * @author Thiago Ricciardi
 *
 */
@Configuration
@ComponentScan({"com.ostegn.jenesis.server.base","com.ostegn.jenesis.server"})
public abstract class ServerEnvironmentTest extends AbstractTransactionalJUnit4SpringContextTests implements IServerEnvironmentTest {
	
	private final Logger logger = Logger.getLogger(this.getClass().getName());
	
	@Autowired private UserDetailsService uds;
	
	/** Authenticates the credentials over spring framework. 
	 * @throws InvalidClassException If found problems with the user authentication declaration.
	 */
	@Before
	public void authenticate() throws InvalidClassException {
		ITestCredential c = getTestCredential();
		UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(null, null, getAnonymousRoles());
		if (c == null) {
			throw new InvalidClassException(this.getClass().getName(), String.format("Missing proper user configuration on %1$s.getTestCredential(), did you forget to override it?", this.getClass().getName()));
		}
		else if ((c.getUsername() == null || c.getPassword() == null)) {
			logger.warning("Username and/or password blank. Going anonymous...");
		}
		else {
			logger.config(String.format("Using username [%1$s]", c.getUsername()));
			UserDetails ud = uds.loadUserByUsername(c.getUsername());
			upat = new UsernamePasswordAuthenticationToken(ud, c.getPassword(), ud.getAuthorities());
		}
		SecurityContextHolder.getContext().setAuthentication(upat);
	}
	
	/** Cleans the authentication and everything attached to it. */
	@After
	public void clearContext() {
		SecurityContextHolder.clearContext();
	}
	
}
