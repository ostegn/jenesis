package com.ostegn.jenesis.junit.shared.base;

import org.junit.Assert;
import org.junit.Test;

import com.ostegn.jenesis.shared.base.TagReplacer;

public class TagReplacerTest {
	
	@Test
	public void testReplace() {
		TagReplacer tr = new TagReplacer();
		String out = tr.replace("replacement", "$TagReplacer", "path/to/${replacement}.test\npath/to/another/${Replacement}.test");
		Assert.assertTrue("Problem replacing tag", "path/to/$TagReplacer.test\npath/to/another/$TagReplacer.test".equals(out));
	}

}
