package com.ostegn.jenesis.junit.shared.base;

import java.util.Random;


import org.junit.Assert;
import org.junit.Test;

import com.ostegn.jenesis.shared.base.SimpleFunctions;

public class SimpleFunctionsTest {
	
	@Test
	public void testIntSmartRandom() {
		Random r = new Random();
		int min = r.nextInt();
		int max = r.nextInt();
		while (!(max > min)) {
			min = r.nextInt();
			max = r.nextInt();
		}
		int test = SimpleFunctions.smartRandom(min, max);
		System.out.println(String.format("testIntSmartRandom: min [%1$d], max [%2$d], random [%3$d]", min, max, test));
		Assert.assertFalse("Minimum value problem.", test < min);
		Assert.assertFalse("Maximum value problem.", test > max);
	}
	
	@Test
	public void testNewStringToken() {
		int rnd = SimpleFunctions.smartRandom(10, 100);
		String test = SimpleFunctions.newStringToken(rnd);
		System.out.println(String.format("testNewStringToken: [%1$s], length [%2$d]", test, test.length()));
		Assert.assertTrue(test, test.length() == rnd);
	}

}
