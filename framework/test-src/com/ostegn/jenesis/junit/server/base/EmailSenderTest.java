package com.ostegn.jenesis.junit.server.base;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;

import org.junit.Test;

import com.ostegn.jenesis.server.base.EmailSender;

public class EmailSenderTest {

	@Test
	public void testSend() {
		try {
			EmailSender es = new EmailSender();
			es.usr = "noreply@jenesis";
			es.pwd = "superstrongpasword";
			es.from = EmailSender.createAddress(es.usr, "Jenesis Mailer");
			es.to = EmailSender.createAddress("user@jenesis", "Jenesis User");
			es.subject = "Testing " + es.getClass().getSimpleName();
			es.body = "This is the body";
			es.host = "smtp.server.address";
			es.port = 25;
			es.ssl = false;
			es.send();
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
		}
	}

}
