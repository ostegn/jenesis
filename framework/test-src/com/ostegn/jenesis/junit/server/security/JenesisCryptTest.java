package com.ostegn.jenesis.junit.server.security;

import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Test;

import com.ostegn.jenesis.server.security.JenesisCrypt;

//import com.google.gwt.junit.client.GWTTestCase;

public class JenesisCryptTest {
	
	private static final String INPUT = "test";
	private static final String RESULT = "a164de67801ac5900f89d07e0a2ff319c1311f20c5d4a3a0c1c3c0cc45eab613";
	
	@Test
	public void testEnCrypt() {
		try {
			String test = JenesisCrypt.EnCrypt(INPUT);
			Assert.assertTrue(String.format("Problem encrypting!\n\tExpected: %1$s\n\tGot: %2$s", RESULT, test), RESULT.equals(test));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			Assert.fail("Failed encrypting, your system does not support the encrypt algorithm!");
		}
	}

}
