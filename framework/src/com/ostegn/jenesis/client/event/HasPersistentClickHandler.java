package com.ostegn.jenesis.client.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

/**
 * Define that an object has a PersistentClickHandler
 * @author Thiago Ricciardi
 * @see PersistentClickEvent
 * @see PersistentClickHandler
 */
public interface HasPersistentClickHandler extends HasHandlers {
	
	/**
	 * Register a PersistentClickHandler
	 * @param handler the persistent click handler to register
	 * @return the handler registration to manipulate it
	 * @see PersistentClickHandler
	 */
	HandlerRegistration addPersistentClickHandler(PersistentClickHandler handler);

}
