package com.ostegn.jenesis.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * An event handler to get information of an IPersistentObject action
 * @author Thiago Ricciardi
 * @see HasPersistentClickHandler
 * @see PersistentClickEvent
 */
public interface PersistentClickHandler extends EventHandler {
	
	/**
	 * Fires on an IPersistentObject action
	 * @param event the event information
	 * @see PersistentClickEvent
	 */
	void onPersistentClick(PersistentClickEvent event);
	
}
