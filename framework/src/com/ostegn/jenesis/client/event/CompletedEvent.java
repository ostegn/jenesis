package com.ostegn.jenesis.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * An event to inform when something has been completed<br>
 * It can send a informational {@link Object} to the {@link CompletedHandler}
 * @author Thiago Ricciardi
 * @see CompletedHandler
 * @see HasCompletedHandler
 */
public class CompletedEvent extends GwtEvent<CompletedHandler> {
	
	private static final Type<CompletedHandler> TYPE = new Type<CompletedHandler>();
	
	private final Object info;
	
	/** Create a new instance of CompletedEvent with the informational Object set to null */
	public CompletedEvent() {
		this.info = null;
	}
	/**
	 * Create a new instance of CompletedEvent with the informational Object set
	 * @param info the informational object
	 */
	public CompletedEvent(Object info) {
		this.info = info;
	}

	@Override
	public Type<CompletedHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(CompletedHandler handler) {
		handler.onCompleted(this);
	}
	
	/**
	 * Get the informational object
	 * @return the informational object
	 */
	public Object getInfo() {
		return info;
	}
	
	/**
	 * Get the event type
	 * @return the event type
	 */
	public static Type<CompletedHandler> getType() {
		return TYPE;
	}

}
