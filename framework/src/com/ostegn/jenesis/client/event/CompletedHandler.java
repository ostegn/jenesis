package com.ostegn.jenesis.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * An event handler to get information when something has been completed
 * @author Thiago Ricciardi
 * @see CompletedEvent
 * @see HasCompletedHandler
 */
public interface CompletedHandler extends EventHandler {
	
	/**
	 * Fires when something has been completed
	 * @param event the event information
	 * @see CompletedEvent
	 */
	void onCompleted(CompletedEvent event);
	
}
