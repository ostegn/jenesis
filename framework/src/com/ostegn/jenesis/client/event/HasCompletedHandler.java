package com.ostegn.jenesis.client.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

/**
 * Define that an object has a CompletedHandler
 * @author Thiago Ricciardi
 * @see CompletedEvent
 * @see CompletedHandler
 */
public interface HasCompletedHandler extends HasHandlers {
	
	/**
	 * Register a CompletedHandler
	 * @param handler the completed handler to register
	 * @return the handler registration to manipulate it
	 * @see CompletedHandler
	 */
	HandlerRegistration addCompletedHandler(CompletedHandler handler);

}
