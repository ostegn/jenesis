package com.ostegn.jenesis.client.event;


import com.google.gwt.event.shared.GwtEvent;
import com.ostegn.jenesis.shared.base.IPersistentObject;

/**
 * An event that carries a {@link IPersistentObject} with it
 * @author Thiago Ricciardi
 * @see HasPersistentClickHandler
 * @see PersistentClickHandler
 * @see IPersistentObject
 */
public class PersistentClickEvent extends GwtEvent<PersistentClickHandler> {
	
	private static final Type<PersistentClickHandler> TYPE = new Type<PersistentClickHandler>();
	
	private final IPersistentObject persistentObject;
	
	/**
	 * Create a new instance of PersistentClickEvent
	 * @param persistentObject the IPersistentObject to assign to it
	 * @see IPersistentObject
	 */
	public PersistentClickEvent(IPersistentObject persistentObject) {
		this.persistentObject = persistentObject;
	}

	@Override
	public Type<PersistentClickHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(PersistentClickHandler handler) {
		handler.onPersistentClick(this);
	}
	
	/**
	 * Get the IPersistentObject attached to this event
	 * @return the IPersistentObject attached to this event
	 * @see IPersistentObject
	 */
	public IPersistentObject getPersistentObject() {
		return persistentObject;
	}
	
	/**
	 * Get the event type
	 * @return the event type
	 */
	public static Type<PersistentClickHandler> getType() {
		return TYPE;
	}

}
