package com.ostegn.jenesis.client.base;


import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.ostegn.jenesis.client.ui.base.EditableWidget;

/**
 * 
 * This is a helper class that defines a converter to be used with {@link EditableWidget}.
 * 
 * @author Thiago Ricciardi
 *
 * @param <D> The display widget class.
 * @param <E> The edit widget class.
 * @param <V> The object that is interchangeable between display and edit widgets.
 */
public interface EditableConverter<D extends Widget, E extends Widget, V> {
	
	/**
	 * Converts the edit widget state into the interchangeable object.
	 * @param editWidget The edit widget.
	 * @return The interchangeable object.
	 */
	V getEditValue(E editWidget);
	/**
	 * Sets the edit widget state according to the interchangeable object value.
	 * @param editWidget The edit widget.
	 * @param value The interchangeable object.
	 */
	void setEditValue(E editWidget, V value);
	/**
	 * Converts the display widget state into the interchangeable object.
	 * @param displayWidget The display widget.
	 * @return The interchangeable object.
	 */
	V getDisplayValue(D displayWidget);
	/**
	 * Sets the display widget state according to the interchangeable object value.
	 * @param displayWidget The display widget.
	 * @param value The interchangeable object.
	 */
	void setDisplayValue(D displayWidget, V value);
	
	/**
	 * This converter is used to convert Label <-> TextBox EditableWidgets using String as the interchangeable object.
	 * <br>This is one of the simplest converter, see this one's source to better understand how to build a converter.
	 */
	static final EditableConverter<Label, TextBox, String> LABEL_TBOX_CONVERTER = new EditableConverter<Label, TextBox, String>() {
		@Override
		public String getEditValue(TextBox editWidget) {
			return editWidget.getText();
		}
		@Override
		public void setEditValue(TextBox editWidget, String value) {
			editWidget.setText(value);
		}
		@Override
		public String getDisplayValue(Label displayWidget) {
			return displayWidget.getText();
		}
		@Override
		public void setDisplayValue(Label displayWidget, String value) {
			displayWidget.setText(value);
		}
	};
	
}
