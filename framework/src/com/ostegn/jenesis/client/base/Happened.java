package com.ostegn.jenesis.client.base;

import java.util.Date;

/**
 * Automagically understand what is the best unit of time to show the distance of a given Date.<br>
 * Also has the ability to set the base date (now) to compare to.
 * @author Thiago Ricciardi
 * @see Happening
 */
public class Happened extends Happening {
	
	/**
	 * The time unit used to express the distance from a Date
	 * @author Thiago Ricciardi
	 *
	 */
	public enum TimeUnit {
		/** Value is expressed in year(s) */
		YEAR,
		/** Value is expressed in month(s) */
		MONTH,
		/** Value is expressed in day(s) */
		DAY,
		/** Value is expressed in hour(s) */
		HOUR,
		/** Value is expressed in minute(s) */
		MINUTE,
		/** Value is expressed in second(s) */
		SECOND
	}
	
	private Date now;
	
	/** Create a new instance of Happened without any information */
	public Happened() {
		super();
	}
	/**
	 * Create a new instance of Happened with the happen date
	 * @param happenDate the date when the event happened
	 */
	public Happened(Date happenDate) {
		super(happenDate);
	}
	/**
	 * Create a new instance of Happened with the base (now) date and the happen date
	 * @param now the base date to compare to happenDate
	 * @param happenDate the date when the event happened
	 */
	public Happened(Date now, Date happenDate) {
		super(happenDate);
		this.now = now;
	}
	
	@Override
	public Date getNow() {
		if (now == null) return super.getNow();
		else return now;
	}
	/**
	 * Gives the ability to change the base of the Happening comparison <br/><br/>
	 * <b>Once you set it, it will not update itself automatically anymore, unless you set it back to null</b>
	 * @param now
	 */
	public void setNow(Date now) {
		this.now = now;
	}
	
	/**
	 * Get the unit of the value given in {@code getValue()}
	 * @return a TimeUnit object representing the unit of the value
	 */
	public TimeUnit getUnit() {
		if (getYears() != 0) return TimeUnit.YEAR;
		else if (getFullMonths() != 0) return TimeUnit.MONTH;
		else if (getFullDays() != 0) return TimeUnit.DAY;
		else if (getFullHours() != 0) return TimeUnit.HOUR;
		else if (getFullMinutes() != 0) return TimeUnit.MINUTE;
		else return TimeUnit.SECOND;
	}
	
	/**
	 * Get the value that represents the difference between now and happenDate
	 * @return a positive number if happenDate is in the future, negative if it's in the past and zero if happenDate is less than a second difference from now
	 */
	public Integer getValue() {
		if (getYears() != 0) return getYears();
		else if (getFullMonths() != 0) return getFullMonths();
		else if (getFullDays() != 0) return getFullDays();
		else if (getFullHours() != 0) return getFullHours();
		else if (getFullMinutes() != 0) return getFullMinutes();
		else return getFullSeconds();
	}
	
	/**
	 * Get the absolute value that represents the difference between now and happenDate
	 * @return a positive number representing the difference between happenDate and now or zero if the difference is less than a second
	 */
	public Integer getAbsValue() {
		if (getYears() != 0) return getAbsYears();
		else if (getFullMonths() != 0) return getAbsFullMonths();
		else if (getFullDays() != 0) return getAbsFullDays();
		else if (getFullHours() != 0) return getAbsFullHours();
		else if (getFullMinutes() != 0) return getAbsFullMinutes();
		else return getAbsFullSeconds();
	}
	
	@Override
	public Date getHappenDate() {
		return super.getHappenDate();
	}
	@Override
	public void setHappenDate(Date happenDate) {
		super.setHappenDate(happenDate);
	}

}
