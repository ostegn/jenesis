package com.ostegn.jenesis.client.base;

/**
 * A {@link com.google.gwt.user.client.Timer} implementation with a {@link Runnable} that runs when elapsed
 * @author Thiago Ricciardi
 *
 */
public class Timer extends com.google.gwt.user.client.Timer implements Runnable {

	private Runnable runnable;

	/** Instantiate Timer without a runnable.<br>
	 *  <b>Needs to use setRunnable otherwise it will raise NullPointerException when elapsed</b> */
	public Timer() {
		runnable = null;
	}

	/**
	 * Instantiate Timer with a runnable to run when elapsed
	 * @param runnable the Runnable
	 */
	public Timer(Runnable runnable) {
		this.runnable = runnable;
	}

	/** This will run when the Timer elapses */
	@Override
	public void run() {
		runnable.run();
	}

	/**
	 * Set the runnable to run when elapsed
	 * @param runnable the Runnable
	 */
	public void setRunnable(Runnable runnable) {
		this.runnable = runnable;
	}

}
