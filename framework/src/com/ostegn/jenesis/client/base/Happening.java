package com.ostegn.jenesis.client.base;

import java.util.Date;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.ostegn.jenesis.shared.base.SimpleConstants;
import com.ostegn.jenesis.shared.base.SimpleFunctions;

/**
 * A class that helps to count time relative to a Date
 * @author Thiago Ricciardi
 * @see Happened
 */
public class Happening {
	
	private Date happenDate;
	
	/** Create a new instance of Happening without any information */
	public Happening() {}
	/**
	 * Create a new instance of Happening with the happen date
	 * @param happenDate the date when the event happened
	 */
	public Happening(Date happenDate) {
		this.happenDate = happenDate;
	}
	
	/**
	 * Get the Date that is right now (updates every time you call it)
	 * @return a Date object representing the current Date
	 */
	public Date getNow() {
		return new Date();
	}
	
	/**
	 * Get the difference in years from now to the happenDate
	 * @return a positive number of year(s) if happenDate is in the future, negative if it is in the past and zero if happenDate is less than a year difference from now
	 */
	public Integer getYears() {
		Integer ret = null;
		if (happenDate != null) {
			Date now = getNow();
			ret = SimpleFunctions.getYear(now) - SimpleFunctions.getYear(happenDate);
			Integer monthDiff = SimpleFunctions.getMonth(now) - SimpleFunctions.getMonth(happenDate);
			if ( (ret > 0) && ( (monthDiff < 0) || ( (monthDiff == 0) && (SimpleFunctions.getDay(now) < SimpleFunctions.getDay(happenDate)) ) ) ) {
				ret--;
			}
			else if ( (ret < 0) && ( (monthDiff > 0) || ( (monthDiff == 0) && (SimpleFunctions.getDay(now) > SimpleFunctions.getDay(happenDate)) ) ) ) {
				ret++;
			}
		}
		return ret;
	}
	/**
	 * Get the absolute difference in years from now to the happenDate
	 * @return a positive number of year(s) from the difference between happenDate and now or zero if the difference is less than a year
	 */
	public Integer getAbsYears() {
		return Math.abs(getYears());
	}
	
	/**
	 * Get the fraction difference in months from now to the happenDate
	 * @return (ranging from -11 to 11) <br> a positive number of month(s) if happenDate is in the future, negative if it is in the past and zero if the fraction difference is less than a month
	 */
	public Integer getMonths() {
		Integer ret = null;
		if (happenDate != null) {
			Date now = getNow();
			Integer yearDiff = SimpleFunctions.getYear(now) - SimpleFunctions.getYear(happenDate);
			ret = SimpleFunctions.getMonth(now) - SimpleFunctions.getMonth(happenDate);
			Integer dayDiff = SimpleFunctions.getDay(now) - SimpleFunctions.getDay(happenDate);
			if ( (ret < 0 && now.after(happenDate)) || (ret == 0 && yearDiff > 0 && dayDiff < 0) ) ret += (int) SimpleConstants.ONE_YEAR_MTH;
			if ( (ret > 0 && now.before(happenDate)) || (ret == 0 && yearDiff < 0 && dayDiff > 0) ) ret -= (int) SimpleConstants.ONE_YEAR_MTH;
			if (dayDiff < 0 && ret > 0) ret--;
			else if (dayDiff > 0 && ret < 0) ret++;
		}
		return ret;
	}
	/**
	 * Get the absolute fraction difference in months from now to the happenDate
	 * @return (ranging from 0 to 11) <br> a positive number of month(s) from the fraction difference between happenDate and now or zero if the difference is less than a month
	 */
	public Integer getAbsMonths() {
		return Math.abs(getMonths());
	}
	
	/**
	 * Get the full difference in months from now to the happenDate
	 * @return a positive number of month(s) if happenDate is in the future, negative if it is in the past and zero if happenDate is less than a month difference from now
	 */
	public Integer getFullMonths() {
		Integer ret = null;
		if (happenDate != null) {
			ret = getMonths();
			ret += getYears() * (int) SimpleConstants.ONE_YEAR_MTH;
		}
		return ret;
	}
	/**
	 * Get the absolute full difference in months from now to the happenDate
	 * @return a positive number of month(s) from the difference between happenDate and now or zero if the difference is less than a month
	 */
	public Integer getAbsFullMonths() {
		return Math.abs(getFullMonths());
	}
	
	/**
	 * Get the fraction difference in days from now to the happenDate
	 * @return (ranging from -31 to 31) <br> a positive number of day(s) if happenDate is in the future, negative if it is in the past and zero if the fraction difference is less than a day
	 */
	public Integer getDays() {
		Integer ret = null;
		if (happenDate != null) {
			Date now = getNow();
			ret = SimpleFunctions.getDay(now) - SimpleFunctions.getDay(happenDate);
			if (ret < 0 && now.after(happenDate)) ret += (int) SimpleConstants.ONE_MONTH_DAY;
			else if (ret > 0 && now.before(happenDate)) ret -= (int) SimpleConstants.ONE_MONTH_DAY;
		}
		return ret;
	}
	/**
	 * Get the absolute fraction difference in days from now to the happenDate
	 * @return (ranging from 0 to 31) <br> a positive number of day(s) from the fraction difference between happenDate and now or zero if the difference is less than a day
	 */
	public Integer getAbsDays() {
		return Math.abs(getAbsDays());
	}
	
	/**
	 * Get the full difference in days from now to the happenDate
	 * @return a positive number of day(s) if happenDate is in the future, negative if it is in the past and zero if happenDate is less than a day difference from now
	 */
	public Integer getFullDays() {
		Integer ret = null;
		if (happenDate != null) {
			ret = CalendarUtil.getDaysBetween(happenDate, getNow());
		}
		return ret;
	}
	/**
	 * Get the absolute full difference in days from now to the happenDate
	 * @return a positive number of day(s) from the difference between happenDate and now or zero if the difference is less than a day
	 */
	public Integer getAbsFullDays() {
		return Math.abs(getFullDays());
	}
	
	/**
	 * Get the full difference in hours from now to the happenDate
	 * @return a positive number of hour(s) if happenDate is in the future, negative if it is in the past and zero if happenDate is less than a hour difference from now
	 */
	public Integer getFullHours() {
		Integer ret = null;
		if (happenDate != null) {
			ret = (int) Math.floor( ( (getNow().getTime() - happenDate.getTime()) / SimpleConstants.ONE_HOUR_MSEC ) );
		}
		return ret;
	}
	/**
	 * Get the absolute full difference in hours from now to the happenDate
	 * @return a positive number of hour(s) from the difference between happenDate and now or zero if the difference is less than a hour
	 */
	public Integer getAbsFullHours() {
		return Math.abs(getFullHours());
	}
	
	/**
	 * Get the full difference in minutes from now to the happenDate
	 * @return a positive number of minute(s) if happenDate is in the future, negative if it is in the past and zero if happenDate is less than a minute difference from now
	 */
	public Integer getFullMinutes() {
		Integer ret = null;
		if (happenDate != null) {
			ret = (int) Math.floor( ( (getNow().getTime() - happenDate.getTime()) / SimpleConstants.ONE_MINUTE_MSEC ) );
		}
		return ret;
	}
	/**
	 * Get the absolute full difference in minutes from now to the happenDate
	 * @return a positive number of minute(s) from the difference between happenDate and now or zero if the difference is less than a minute
	 */
	public Integer getAbsFullMinutes() {
		return Math.abs(getFullMinutes());
	}
	
	/**
	 * Get the full difference in seconds from now to the happenDate
	 * @return a positive number of second(s) if happenDate is in the future, negative if it is in the past and zero if happenDate is less than a second difference from now
	 */
	public Integer getFullSeconds() {
		Integer ret = null;
		if (happenDate != null) {
			ret = (int) Math.floor( ( (getNow().getTime() - happenDate.getTime()) / SimpleConstants.ONE_SECOND_MSEC ) );
		}
		return ret;
	}
	/**
	 * Get the absolute full difference in seconds from now to the happenDate
	 * @return a positive number of second(s) from the difference between happenDate and now or zero if the difference is less than a second
	 */
	public Integer getAbsFullSeconds() {
		return Math.abs(getFullSeconds());
	}
	
	/**
	 * Checks if happenDate is in the future
	 * @return true if it's future, false otherwise
	 */
	public Boolean isFuture() {
		return getNow().before(happenDate);
	}
	
	/**
	 * Checks if happenDate is in the past
	 * @return true if it's past, false otherwise
	 */
	public Boolean isPast() {
		return getNow().after(happenDate);
	}
	
	/**
	 * Get the happenDate
	 * @return a Date object representing an event
	 */
	public Date getHappenDate() {
		return happenDate;
	}
	/**
	 * Set the happenDate
	 * @param happenDate a Date object representing an event
	 */
	public void setHappenDate(Date happenDate) {
		this.happenDate = happenDate;
	}
	
}
