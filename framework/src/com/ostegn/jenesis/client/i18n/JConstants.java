package com.ostegn.jenesis.client.i18n;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Constants;

public interface JConstants extends Constants {
	
	JConstants instance = GWT.create(JConstants.class);
	
	Map<String, String> happeningLabel();

}
