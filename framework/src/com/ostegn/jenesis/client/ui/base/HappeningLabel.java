package com.ostegn.jenesis.client.ui.base;

import java.util.Date;
import java.util.Map;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.ostegn.jenesis.client.base.Happening;
import com.ostegn.jenesis.client.i18n.JConstants;
import com.ostegn.jenesis.shared.base.SimpleConstants;

/**
 * A label that implements the Happening class
 * @author Thiago Ricciardi
 * @see Happening
 */
public class HappeningLabel extends Label {
	
	private Map<String, String> msg = JConstants.instance.happeningLabel();
	
	private Happening happening;
	
	private Timer timer = new Timer() {
		@Override
		public void run() {
			init(happening);
		}
	};
	
	/**
	 * Create a new instance of HappeningLabel setting the happen Date
	 * @param happenDate the event Date
	 */
	public HappeningLabel(Date happenDate) {
		init(happenDate);
	}
	/**
	 * Create a new instance of HappeningLabel setting the happening object
	 * @param happening the event Happening object
	 */
	public HappeningLabel(Happening happening) {
		init(happening);
	}
	
	private void init(Date happenDate) {
		init(new Happening(happenDate));
	}
	private void init(Happening happening) {
		this.happening = happening;
		
		timer.cancel();
		Boolean visible = false;
		
		String text = new String();
		if ( (happening != null) ? (happening.getHappenDate() != null) : false ) {
			
			visible = true;
			
			if (happening.getYears() != 0) {
				Integer years = happening.getAbsYears();
				text += years.toString() + " " + ( (years > 1) ? msg.get("years").trim() : msg.get("year").trim() );
			}
			else if (happening.getFullMonths() != 0) {
				Integer months = happening.getAbsFullMonths();
				text += months.toString() + " " + ( (months > 1) ? msg.get("months").trim() : msg.get("month").trim() );
			}
			else if (happening.getFullDays() != 0) {
				Integer days = happening.getAbsFullDays();
				text += days.toString() + " " + ( (days > 1) ? msg.get("days").trim() : msg.get("day").trim() );
				timer.scheduleRepeating((int) SimpleConstants.ONE_HOUR_MSEC);
			}
			else if (happening.getFullHours() != 0) {
				Integer hours = happening.getAbsFullHours();
				text += hours.toString() + " " + ( (hours > 1) ? msg.get("hours").trim() : msg.get("hour").trim() );
				timer.scheduleRepeating((int) SimpleConstants.ONE_MINUTE_MSEC);
			}
			else if (happening.getFullMinutes() != 0) {
				Integer mins = happening.getAbsFullMinutes();
				text += mins.toString() + " " + ( (mins > 1) ? msg.get("minutes").trim() : msg.get("minute").trim() );
				timer.scheduleRepeating((int) SimpleConstants.ONE_SECOND_MSEC);
			}
			else if (happening.getFullSeconds() != 0) {
				Integer secs = happening.getAbsFullSeconds();
				text += secs.toString() + " " + ( (secs > 1) ? msg.get("seconds").trim() : msg.get("second").trim() );
				timer.scheduleRepeating((int) SimpleConstants.ONE_SECOND_MSEC);
			}
			else {
				visible = false;
				timer.scheduleRepeating((int) SimpleConstants.ONE_SECOND_MSEC);
			}
			
			if (happening.isFuture()) {
				text = msg.get("future").trim().replace("{0}", text);
			}
			else {
				text = msg.get("past").trim().replace("{0}", text);
			}
			
			setTitle(DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_MEDIUM).format(happening.getHappenDate()));
		}
		
		setText(text);
		setVisible(visible);
	}

	/**
	 * Get the Happening object
	 * @return the event Happening object
	 */
	public Happening getHappening() {
		return happening;
	}
	/**
	 * Set the Happening object
	 * @param happening the event Happening object
	 */
	public void setHappening(Happening happening) {
		init(happening);
	}
	/**
	 * Get the happen Date
	 * @return the event Date
	 */
	public Date getHappenDate() {
		return (happening != null) ? happening.getHappenDate() : null;
	}
	/**
	 * Set the happen Date
	 * @param happenDate the event Date
	 */
	public void setHappenDate(Date happenDate) {
		init(happenDate);
	}

}
