package com.ostegn.jenesis.client.ui.base;


import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.ostegn.jenesis.client.base.EditableConverter;
import com.ostegn.jenesis.client.event.CompletedEvent;
import com.ostegn.jenesis.client.event.CompletedHandler;
import com.ostegn.jenesis.client.event.HasCompletedHandler;

/**
 * This class creates a double widget, one for displaying and another for editing.<br>
 * At first it uses the display widget to show data. When the user clicks on it, it changes to the edit widget using a editable converter.
 * After the edit, when the user blurs the editable widget, it reverts back to the display widget passing the value using the editable converter.
 * @author Thiago Ricciardi
 *
 * @param <D> The display widget class.
 * @param <E> The edit widget class.
 * @param <V> The object class that is interchangeable between display and edit widgets.
 */
public class EditableWidget<D extends Widget, E extends Widget, V> extends SimplePanel implements HasCompletedHandler, ClickHandler, BlurHandler {
	
	private HandlerManager handlerManager = new HandlerManager(this);
	
	private D displayWidget;
	private E editWidget;
	
	private EditableConverter<D, E, V> converter;
	
	private Boolean editable = true;
	
	/**
	 * Get the display widget
	 * @return the display interchangeable widget
	 */
	public D getDisplayWidget() {
		return displayWidget;
	}
	/**
	 * Set the display widget
	 * @param displayWidget the display interchangeable widget
	 */
	public void setDisplayWidget(D displayWidget) {
		displayWidget.sinkEvents(Event.ONCLICK);
		displayWidget.addHandler(this, ClickEvent.getType());
		if (getWidget() == null || getWidget() == this.displayWidget) {
			setWidget(displayWidget);
		}
		this.displayWidget = displayWidget;
	}
	/**
	 * Get the edit widget
	 * @return the edit interchangeable widget
	 */
	public E getEditWidget() {
		return editWidget;
	}
	/**
	 * Set the edit widget
	 * @param editWidget the edit interchangeable widget
	 */
	public void setEditWidget(E editWidget) {
		editWidget.sinkEvents(Event.ONBLUR);
		editWidget.addHandler(this, BlurEvent.getType());
		if (getWidget() == this.editWidget) {
			setWidget(editWidget);
		}
		this.editWidget = editWidget;
	}
	/**
	 * Get the converter
	 * @return the converter definition
	 * @see EditableConverter
	 */
	public EditableConverter<D, E, V> getConverter() {
		return converter;
	}
	/**
	 * Get the converter
	 * @param converter the converter definition
	 * @see EditableConverter
	 */
	public void setConverter(EditableConverter<D, E, V> converter) {
		this.converter = converter;
	}
	/**
	 * Get whether this EditableWidget is editable or not
	 * @return true if it's editable, false if not
	 */
	public Boolean getEditable() {
		return editable;
	}
	/**
	 * Set whether this EditableWidget is editable or not
	 * @param editable true if it's editable, false if not
	 */
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}
	/**
	 * Get the interchangeable data value
	 * @return the interchangeable data value
	 */
	public V getValue() {
		V ret = null;
		if (getWidget() == displayWidget) {
			ret = converter.getDisplayValue(displayWidget);
		}
		else if (getWidget() == editWidget) {
			ret = converter.getEditValue(editWidget);
		}
		return ret;
	}
	/**
	 * Set the interchangeable data value
	 * @param value the interchangeable data value
	 */
	public void setValue(V value) {
		if (getWidget() == displayWidget) {
			converter.setDisplayValue(displayWidget, value);
		}
		else if (getWidget() == editWidget) {
			converter.setEditValue(editWidget, value);
		}
	}
	
	/**
	 * Create a new instance of EditableWidget setting the converter
	 * @param converter the converter definition
	 */
	public EditableWidget(EditableConverter<D, E, V> converter) {
		this(null, null, converter);
	}
	/**
	 * Create a new instance of EditableWidget setting the display and edit widgets
	 * @param displayWidget the display interchangeable widget
	 * @param editWidget the edit interchangeable widget
	 */
	public EditableWidget(D displayWidget, E editWidget) {
		this(displayWidget, editWidget, null);
	}
	/**
	 * Create a new instance of EditableWidget setting the display, edit widgets and the converter
	 * @param displayWidget the display interchangeable widget
	 * @param editWidget the edit interchangeable widget
	 * @param converter the converter definition
	 */
	public EditableWidget(D displayWidget, E editWidget, EditableConverter<D, E, V> converter) {
		if (displayWidget != null) setDisplayWidget(displayWidget);
		if (editWidget != null) setEditWidget(editWidget);
		this.converter = converter;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		Object o = event.getSource();
		if (o == displayWidget && editWidget != null) {
			converter.setEditValue(editWidget, converter.getDisplayValue(displayWidget));
			setWidget(editWidget);
			((FocusWidget) editWidget).setFocus(true);
			fireEvent(new CompletedEvent(editWidget));
		}
	}
	
	@Override
	public void onBlur(BlurEvent event) {
		Object o = event.getSource();
		if (o == editWidget && displayWidget != null) {
			converter.setDisplayValue(displayWidget, converter.getEditValue(editWidget));
			setWidget(displayWidget);
			fireEvent(new CompletedEvent(displayWidget));
		}
	}
	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		super.fireEvent(event);
		handlerManager.fireEvent(event);
	}
	
	@Override
	public HandlerRegistration addCompletedHandler(CompletedHandler handler) {
		return handlerManager.addHandler(CompletedEvent.getType(), handler);
	}
	
}
