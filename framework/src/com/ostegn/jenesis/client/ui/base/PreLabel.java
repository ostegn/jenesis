package com.ostegn.jenesis.client.ui.base;

import com.google.gwt.dom.client.Element;
import com.google.gwt.i18n.shared.DirectionEstimator;
import com.google.gwt.user.client.ui.DirectionalTextHelper;
import com.google.gwt.user.client.ui.Label;

/**
 * This class implements a label using the &lt;pre&gt; html tag.
 * @author Thiago Ricciardi
 * @see Label
 */
public class PreLabel extends Label {
	
	private DirectionalTextHelper directionalTextHelper;
	
	private String text = "";
	
	/** Create a new instance of PreLabel */
	public PreLabel() {
		super();
	}

	protected PreLabel(Element element) {
		super(element);
	}

	/**
	 * Create a new instance of PreLabel setting the text and word wrap
	 * @param text the text of the label
	 * @param wordWrap if it's word wrapping (true) or not (false)
	 */
	public PreLabel(String text, boolean wordWrap) {
		super(text, wordWrap);
	}

	/**
	 * Create a new instance of PreLabel setting the text and direction
	 * @param text the text of the label
	 * @param dir the direction (RTL, LTR)
	 */
	public PreLabel(String text, Direction dir) {
		super(text, dir);
	}

	/**
	 * Create a new instance of PreLabel setting the text and direction estimator
	 * @param text the text of the label
	 * @param directionEstimator the direction estimator
	 */
	public PreLabel(String text, DirectionEstimator directionEstimator) {
		super(text, directionEstimator);
	}

	/**
	 * Create a new instance of PreLabel setting the text
	 * @param text the text of the label
	 */
	public PreLabel(String text) {
		super(text);
	}

	private DirectionalTextHelper getDirectionalTextHelper() {
		if (directionalTextHelper == null) {
			directionalTextHelper = new DirectionalTextHelper(getElement(), false);
		}
		return directionalTextHelper;
	}

	@Override
	public String getText() {
		return text;
	}
	
	@Override
	public Direction getTextDirection() {
		return getDirectionalTextHelper().getTextDirection();
	}
	
	@Deprecated
	public void setDirection(Direction direction) {
		getDirectionalTextHelper().setDirection(direction);
		updateHorizontalAlignment();
	}

	@Override
	public void setText(String text) {
		this.text = text;
		getDirectionalTextHelper().setTextOrHtml("<pre>"+text+"</pre>", true);
		updateHorizontalAlignment();
	}

	@Override
	public void setText(String text, Direction dir) {
		this.text = text;
		getDirectionalTextHelper().setTextOrHtml("<pre>"+text+"</pre>", dir, true);
		updateHorizontalAlignment();
	}

}
