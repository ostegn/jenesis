package com.ostegn.jenesis.client.ui.base;

import com.google.gwt.i18n.shared.DirectionEstimator;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.HTML;

/**
 * Create a HTML Heading label (&lt;hN&gt;)
 * @author Thiago Ricciardi
 *
 */
public class HeadingLabel extends HTML {
	
	private final static int DEFAULT_TYPE = 1;
	
	private String html;
	private int type;

	/** Create a new HeadingLabel of the default type */
	public HeadingLabel() {
		this(DEFAULT_TYPE);
	}
	/**
	 * Create a new HeadingLabel of the desired type
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(int type) {
		super();
		init(type);
	}

	/**
	 * Create a new HeadingLabel with the HTML contents and the default type
	 * @param html the HTML contents
	 */
	public HeadingLabel(String html) {
		this(html, DEFAULT_TYPE);
	}
	/**
	 * Create a new HeadingLabel with the HTML contents and the desired type
	 * @param html the HTML contents
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(String html, int type) {
		super(html);
		init(type);
	}

	/**
	 * Create a new HeadingLabel with the HTML contents, the content's direction and the default type
	 * @param html the HTML contents
	 * @param dir the content's direction
	 */
	public HeadingLabel(String html, Direction dir) {
		this(html, dir, DEFAULT_TYPE);
	}
	/**
	 * Create a new HeadingLabel with the HTML contents, the content's direction and the desired type
	 * @param html the HTML contents
	 * @param dir the content's direction
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(String html, Direction dir, int type) {
		super(html, dir);
		init(type);
	}

	/**
	 * Create a new HeadingLabel with the HTML contents and the default type
	 * @param html the SafeHtml contents
	 */
	public HeadingLabel(SafeHtml html) {
		this(html, DEFAULT_TYPE);
	}
	/**
	 * Create a new HeadingLabel with the HTML contents and the desired type
	 * @param html the SafeHtml contents
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(SafeHtml html, int type) {
		super(html);
		init(type);
	}
	
	/**
	 * Create a new HeadingLabel with the HTML contents, the content's direction and the default type
	 * @param html the SafeHtml contents
	 * @param dir the content's direction
	 */
	public HeadingLabel(SafeHtml html, Direction dir) {
		this(html, dir, DEFAULT_TYPE);
	}
	/**
	 * Create a new HeadingLabel with the HTML contents, the content's direction and the desired type
	 * @param html the SafeHtml contents
	 * @param dir the content's direction
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(SafeHtml html, Direction dir, int type) {
		super(html, dir);
		init(type);
	}

	/**
	 * Create a new HeadingLabel with the HTML contents, the DirectionEstimator and the default type
	 * @param html the SafeHtml contents
	 * @param directionEstimator A DirectionEstimator object used for automatic direction adjustment
	 */
	public HeadingLabel(SafeHtml html, DirectionEstimator directionEstimator) {
		this(html, directionEstimator, DEFAULT_TYPE);
	}
	/**
	 * Create a new HeadingLabel with the HTML contents, the DirectionEstimator and the desired type
	 * @param html the SafeHtml contents
	 * @param directionEstimator A DirectionEstimator object used for automatic direction adjustment
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(SafeHtml html, DirectionEstimator directionEstimator, int type) {
		super(html, directionEstimator);
		init(type);
	}

	/**
	 * Create a new HeadingLabel with the HTML contents, the word wrapping and the default type
	 * @param html the HTML contents
	 * @param wordWrap if the label is going to do word wrapping
	 */
	public HeadingLabel(String html, boolean wordWrap) {
		super(html, wordWrap);
	}
	/**
	 * Create a new HeadingLabel with the HTML contents, the word wrapping and the desired type
	 * @param html the HTML contents
	 * @param wordWrap if the label is going to do word wrapping
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public HeadingLabel(String html, boolean wordWrap, int type) {
		this(html, wordWrap);
		init(type);
	}
	
	private void init(int type) {
		setStyleName("gwt-Label");
		setType(type);
	}
	
	private String createInnerHTML() {
		String ret = new String();
		ret += "<h";
		ret += Integer.toString(type);
		ret += ">";
		ret += html;
		ret += "</h";
		ret += Integer.toString(type);
		ret += ">";
		return ret;
	}
	
	/**
	 * Set the heading type
	 * @param type the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public void setType(int type) {
		if (type < 1) type = 1;
		if (type > 6) type = 6;
		this.type = type;
		super.setHTML(createInnerHTML());
	}
	/**
	 * Get the heading type
	 * @return the number of heading: &lt;h<b>1</b>&gt;, &lt;h<b>2</b>&gt;, &lt;h<b>3</b>&gt; ...
	 */
	public int getType() {
		return type;
	}
	
	@Override
	public void setHTML(SafeHtml html) {
		this.html = html.asString();
		super.setHTML(createInnerHTML());
	}
	@Override
	public void setHTML(SafeHtml html, Direction dir) {
		this.html = html.asString();
		super.setHTML(createInnerHTML(), dir);
	}
	@Override
	public void setHTML(String html) {
		this.html = html;
		super.setHTML(createInnerHTML());
	}
	@Override
	public void setHTML(String html, Direction dir) {
		this.html = html;
		super.setHTML(createInnerHTML(), dir);
	}
	@Override
	public String getHTML() {
		return html;
	}
	
	@Override
	public void setText(String text) {
		html = text;
		super.setHTML(createInnerHTML());
	}
	@Override
	public void setText(String text, Direction dir) {
		html = text;
		super.setText(createInnerHTML(), dir);
	}

}
