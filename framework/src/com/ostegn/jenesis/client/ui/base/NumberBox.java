package com.ostegn.jenesis.client.ui.base;


import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.TextBox;
import com.ostegn.jenesis.shared.base.SimpleFunctions;

/**
 * Helper box that implements a number validation.
 * @author Thiago Ricciardi
 * 
 * @see TextBox
 */
public class NumberBox extends TextBox {
	
	/**
	 * Acceptable NumberTypes of NumberBox.<br>
	 * Long doesn't accept fraction digits, Double does.
	 * @author Thiago Ricciardi
	 * @see NumberBox
	 */
	public enum NumberType {
		Long, Double
	}
	
	private NumberType numType = NumberType.Double;
	
	/** Creates a new NumberBox with default NumberType Double */
	public NumberBox() {
		super();
		init();
	}
	
	/**
	 * Creates a new NumberBox
	 * @param numType NumberType to define acceptable format.
	 * @see NumberType
	 */
	public NumberBox(NumberType numType) {
		super();
		this.numType = numType;
		init();
	}
	
	private void init() {
		this.addKeyPressHandler(keyPressHandler);
	}
	
	/**
	 * Gets the Long value of the field.
	 * @return A Long value representing the value of the field.
	 */
	public Long getLongValue() {
		String s = this.getText().replace(",", ".");
		return SimpleFunctions.getDoubleValue(s).longValue();
	}
	 /**
	  * Sets a Long value to the field.
	  * @param l A Long value.
	  */
	public void setLongValue(Long l) {
		setText((l != null) ? l.toString() : null);
	}
	 /**
	  * Gets the Double value of the field.
	  * @return A Double value representing the value of the field.
	  */
	public Double getDoubleValue() {
		String s = this.getText().replace(",", ".");
		return SimpleFunctions.getDoubleValue(s);
	}
	
	/**
	 * Sets a Double value to the field.
	 * @param d A Double value.
	 */
	public void setDoubleValue(Double d) {
		setText((d != null) ? d.toString() : null);
	}
	
	private KeyPressHandler keyPressHandler = new KeyPressHandler() {
		@Override
		public void onKeyPress(KeyPressEvent event) {
			char c = event.getCharCode();
			switch (c) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					break;
				case ',':
				case '.':
					if (numType == NumberType.Double) {
						if (getText().contains(",") || getText().contains(".")) event.preventDefault();
					}
					else {
						event.preventDefault();
					}
					break;
				default:
					event.preventDefault();
					break;
			}
		}
	};

}
