package com.ostegn.jenesis.client.ui.base;

import com.google.gwt.user.client.ui.SimplePanel;

/**
 * A UI CSS Class helper.<br>
 * Add "ui-module" and "module-{CLASSNAME}" style names to it to make it easier to CSS Style the UI modules.<br>
 * For example, if your class name is MainMenu, then your style will become {@code style="ui-module module-MainMenu"}
 * @author Thiago Ricciardi
 *
 */
public class UIModule extends SimplePanel {

	{
		addStyleName();
	}

	private void addStyleName() {
		addStyleName("ui-module");
		addStyleName("module-" + getClass().getName().substring(getClass().getName().lastIndexOf('.')+1).replace("$", "-"));
	}

	@Override
	public void setStyleName(String style) {
		super.setStyleName(style);
		addStyleName();
	}

}
