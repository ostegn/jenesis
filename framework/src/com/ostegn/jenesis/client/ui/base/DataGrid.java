package com.ostegn.jenesis.client.ui.base;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class is used to display a Data Grid of objects of type &lt;T&gt;<br>
 * It's commonly used to show results of a query for instance
 * @author Thiago Ricciardi
 *
 * @param <T> the type of row objects
 */
public class DataGrid<T> extends SimplePanel implements HasClickHandlers {
	
	private FlexTable dataTable;
	
	private Widget emptyWidget;
	
	private List<T> data;
	
	private List<IColumn<T>> columns;
	
	/** Create a new instance of an empty DataGrid<br>No columns, no data. */
	public DataGrid() {
		this(null, null);
	}
	/**
	 * Create a new instance of DataGrid
	 * @param columns the list of column definitions
	 * @param data the data to display
	 */
	public DataGrid(List<IColumn<T>> columns, List<T> data) {
		this.columns = columns;
		this.data = data;
		
		dataTable = new FlexTable();
		
		dataTable.addStyleName("table");
		dataTable.addStyleName("table-striped");
		dataTable.addStyleName("table-hover");
		
		drawAll();
	}
	
	/**
	 * Add a new column definition
	 * @param column the column definition
	 */
	public void addColumn(IColumn<T> column) {
		if (columns == null) columns = new ArrayList<IColumn<T>>();
		if (columns.indexOf(column) < 0) columns.add(column);
		
		if (column != null) drawColumn(column);
	}
	
	private void drawColumn(IColumn<T> column) {
		int j = ((columns != null) ? columns.indexOf(column) : -1);
		if (!(j < 0)) {
			int i = 0;
			dataTable.setWidget(i, j, column.getHeader());
			dataTable.getCellFormatter().setStyleName(i, j, "thead");
			i++;
			if (data != null) {
				for (T obj : data) {
					dataTable.setWidget(i, j, column.getValue(obj));
					dataTable.getCellFormatter().setStyleName(i, j, "tbody");
					i++;
				}
			}
		}
	}
	
	private void drawAll() {
		if (columns != null && (data != null ? !data.isEmpty() : false)) {
			dataTable.removeAllRows();
			for (IColumn<T> c : columns) {
				drawColumn(c);
			}
			setWidget(dataTable);
		}
		else {
			setWidget(emptyWidget);
		}
	}
	
	/**
	 * Set the list of column definitions
	 * @param columns the list of column definitions
	 */
	public void setColumns(List<IColumn<T>> columns) {
		this.columns = columns;
		drawAll();
	}

	/**
	 * Get the displayed data
	 * @return a list of T objects
	 */
	public List<T> getData() {
		return data;
	}
	/**
	 * Set the data to display
	 * @param data the list of T objects to display
	 */
	public void setData(List<T> data) {
		this.data = data;
		drawAll();
	}
	
	/**
	 * Get the widget displayed when there is no data
	 * @return a Widget that is displayed when no data is available
	 */
	public Widget getEmptyWidget() {
		return emptyWidget;
	}
	/**
	 * Set the widget to display when there is no data
	 * @param widget a Widget to display when no data is available
	 */
	public void setEmptyWidget(Widget widget) {
		emptyWidget = widget;
		if ((data != null) ? !(data.size() > 0) : true) {
			setWidget(emptyWidget);
		}
	}
	
	/** Does nothing */
	@Override
	public void add(IsWidget child) {
		// TODO Auto-generated method stub
		//super.add(child);
	}
	/** Does nothing */
	@Override
	public void add(Widget w) {
		// TODO Auto-generated method stub
		//super.add(w);
	}
	
	/** Clear the data, the columns and display the emptyWidget */
	@Override
	public void clear() {
		if (data != null) data.clear();
		if (columns != null) columns.clear();
		drawAll();
	}
	
	/** Update the data display */
	public void refresh() {
		drawAll();
	}
	
	/**
	 * Given a click event, return the T data that was clicked, or null if the event did not hit this DataGrid.
	 * The data can also be null if the click event does not occur on a specific cell.
	 * @param event A click event of indeterminate origin
	 * @return The appropriate data, or null
	 */
	public T getDataForEvent(ClickEvent event) {
		T ret = null;
		Cell c = dataTable.getCellForEvent(event);
		if (c != null && data != null) {
			ret = data.get(c.getRowIndex()-1);
		}
		return ret;
	}
	
	/**
	 * Given a click event, return the widget that was clicked, or null if the event did not hit this DataGrid.
	 * The widget can also be null if the click event does not occur on a specific cell.
	 * @param event A click event of indeterminate origin
	 * @return The appropriate widget, or null
	 */
	public Widget getWidgetForEvent(ClickEvent event) {
		Widget ret = null;
		Cell c = dataTable.getCellForEvent(event);
		if (c != null) {
			ret = dataTable.getWidget(c.getRowIndex(), c.getCellIndex());
		}
		return ret;
	}
	
	/**
	 * Interface for the column definition
	 * @author Thiago Ricciardi
	 *
	 * @param <T> the type of row objects
	 */
	public interface IColumn<T> {
		
		public Widget getHeader();
		
		public Widget getValue(T value);
		
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return dataTable.addClickHandler(handler);
	}

}
