package com.ostegn.jenesis.client.ui.base;

import java.util.ArrayList;

import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.user.client.ui.ListBox;

/**
 * 
 * An extension of ListBox's features.<br>
 * This component can attach an object of T class to every item added to the ListBox.<br>
 * If you do not use a method that attaches a binded object {@code o}, it will attach {@code null}.
 * 
 * @author Thiago Ricciardi
 *
 * @param <T> Class of the stored object.
 * 
 * @see ListBox
 */
public class ObjectListBox<T> extends ListBox {
	
	private static final int INSERT_AT_END = -1;
	
	private ArrayList<T> objects = new ArrayList<T>();
	
	/*private void addObject(T o) {
		addObject(o,  null);
	}*/
	
	private void addObject(T o, Integer index) {
		if (index == null) index = objects.size();
		while (index > objects.size()) objects.add(null);
		objects.add(index, o);
	}
	private void removeObject(Integer index) {
		objects.remove(index);
	}
	
	@Override public void addItem(String item) {addItem(item, (T) null);}
	@Override public void addItem(String item, Direction dir) {addItem(item, dir, (T) null);}
	@Override public void addItem(String item, Direction dir, String value) {addItem(item, dir, value, (T) null);}
	@Override public void addItem(String item, String value) {addItem(item, value, (T) null);}
	
	/**
	 * Adds an item to the list box.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be added.
	 * @param o The object to be binded to the item.
	 */
	public void addItem(String item, T o) {
		insertItem(item, o, INSERT_AT_END);
	}
	
	/**
	 * Adds an item to the list box, specifying its direction.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be added.
	 * @param dir The item's direction.
	 * @param o The object to be binded to the item.
	 */
	public void addItem(String item, Direction dir, T o) {
		insertItem(item, dir, o, INSERT_AT_END);
	}
	
	/**
	 * Adds an item to the list box, specifying its direction and an initial value for the item.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be added.
	 * @param dir The item's direction.
	 * @param value The item's value, to be submitted if it is part of a FormPanel; cannot be null
	 * @param o The object to be binded to the item.
	 */
	public void addItem(String item, Direction dir, String value, T o) {
		insertItem(item, dir, value, o, INSERT_AT_END);
	}
	
	/**
	 * Adds an item to the list box, specifying an initial value for the item.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be added.
	 * @param value The item's value, to be submitted if it is part of a FormPanel; cannot be null
	 * @param o The object to be binded to the item.
	 */
	public void addItem(String item, String value, T o) {
		insertItem(item, value, o, INSERT_AT_END);
	}
	
	@Override public void insertItem(String item, int index) {insertItem(item, (T) null, index);}
	@Override public void insertItem(String item, Direction dir, int index) {insertItem(item, dir, (T) null, index);}
	@Override public void insertItem(String item, Direction dir, String value, int index) {insertItem(item, dir, value, (T) null, index);}
	@Override public void insertItem(String item, String value, int index) {insertItem(item, value, (T) null, index);}
	
	/**
	 * Inserts an item into the list box.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be inserted.
	 * @param o The object to be binded to the item.
	 * @param index The index at which to insert it.
	 */
	public void insertItem(String item, T o, int index) {
		insertItem(item, null, item, o, index);
	}
	
	/**
	 * Inserts an item into the list box, specifying its direction.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be inserted.
	 * @param dir The item's direction
	 * @param o The object to be binded to the item.
	 * @param index The index at which to insert it.
	 */
	public void insertItem(String item, Direction dir, T o, int index) {
		insertItem(item, dir, item, o, index);
	}
	
	/**
	 * Inserts an item into the list box, specifying its direction and an initial value for the item.
	 * If the index is less than zero, or greater than or equal to the length of the list,
	 * then the item will be appended to the end of the list.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be inserted.
	 * @param dir The item's direction. If null, the item is displayed in the widget's overall direction,
	 * 			or, if a direction estimator has been set, in the item's estimated direction.
	 * @param value The item's value, to be submitted if it is part of a FormPanel.
	 * @param o The object to be binded to the item.
	 * @param index The index at which to insert it.
	 */
	public void insertItem(String item, Direction dir, String value, T o, int index) {
		addObject(o, (index < 0) ? null : index);
		super.insertItem(item, dir, value, index);
	}
	
	/**
	 * Inserts an item into the list box, specifying an initial value for the item.<br>
	 * Adds a T object binded to it.
	 * @param item The text of the item to be inserted.
	 * @param value The item's value, to be submitted if it is part of a FormPanel.
	 * @param o The object to be binded to the item.
	 * @param index The index at which to insert it.
	 */
	public void insertItem(String item, String value, T o, int index) {
		insertItem(item, null, value, o, index);
	}
	
	/**
	 * Get the selected item's binded object.
	 * @return An object of T class.
	 */
	public T getSelectedObject() {
		T ret = null;
		int selected = getSelectedIndex();
		if ( (!(selected < 0)) && (selected < objects.size()) ) {
			ret = objects.get(selected);
		}
		return ret;
	}
	
	/**
	 * Sets the selected item trough the binded object.
	 * @param o binded object to be selected.
	 */
	public void setSelectedObject(T o) {
		setSelectedIndex(objects.indexOf(o));
	}
	
	/**
	 * Gets the selected item's value.
	 * @return A String representing the value.
	 */
	public String getSelectedValue() {
		return getValue(getSelectedIndex());
	}
	
	/**
	 * Sets the selected item trough the item's value.
	 * @param value The item's value to be selected.
	 */
	public void setSelectedValue(String value) {
		for (int i = 0; i < getItemCount(); i++) {
			if (getValue(i).equals(value)) {
				setSelectedIndex(i);
				break;
			}
		}
	}
	
	@Override
	public void removeItem(int index) {
		removeObject(index);
		super.removeItem(index);
	}
	
	@Override
	public void clear() {
		objects.clear();
		super.clear();
	}
	
}
