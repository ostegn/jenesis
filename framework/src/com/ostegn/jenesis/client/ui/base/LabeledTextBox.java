package com.ostegn.jenesis.client.ui.base;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Extends the functionality of a {@link TextBox} adding the ability to use it as a label when it's empty.
 * @author Thiago Ricciardi
 * @see TextBox
 */
public class LabeledTextBox extends TextBox {
	
	private Boolean isLabelVisible;
	private Boolean isEditing;
	
	private String label;
	private Boolean isPassword = false;
	
	/** Creates an empty LabeledTextBox with no label and no content */
	public LabeledTextBox() {
		super();
		init(null, null);
	}
	/**
	 * Creates a LabeledTextBox with the label string set
	 * @param label the TextBox label
	 */
	public LabeledTextBox(String label) {
		super();
		init(label, null);
	}
	/**
	 * Creates a LabeledTextBox with the label string set and instructs about the password content
	 * @param label the TextBox label
	 * @param isPassword true if it's a password field, false otherwise
	 */
	public LabeledTextBox(String label, Boolean isPassword) {
		init(label, isPassword);
	}
	
	private void init(String label, Boolean isPassword) {
		if (isPassword != null) this.isPassword = isPassword;
		setLabel(label);
		showLabel();
		isEditing = false;
		addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				if (isLabelVisible) {
					hideLabel();
				}
				isEditing = true;
			}
		});
		addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				isLabelVisible = !(getText().length() > 0);
				if (isLabelVisible) {
					showLabel();
				}
				isEditing = false;
			}
		});
	}
	
	private void showLabel() {
		if (isPassword) getElement().setAttribute("type", "text");
		addStyleDependentName("label");
		addStyleName("muted");
		super.setText(getLabel());
		isLabelVisible = true;
	}
	private void hideLabel() {
		if (isPassword) getElement().setAttribute("type", "password");
		removeStyleDependentName("label");
		removeStyleName("muted");
		super.setText("");
		isLabelVisible = false;
	}
	
	/**
	 * Get this component label
	 * @return this label string
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * Set the label
	 * @param label the label string
	 */
	public void setLabel(String label) {
		this.label = label;
		setTitle(label);
		getElement().setAttribute("placeholder", (label != null) ? label : "");
	}
	
	@Override
	public void setText(String text) {
		if (isLabelVisible) {
			if (text.length() > 0) {
				hideLabel();
				super.setText(text);
			}
		}
		else if (!(text.length() > 0) && !isEditing) {
			showLabel();
		}
		else {
			super.setText(text);
		}
	}
	@Override
	public String getText() {
		if (isLabelVisible) return "";
		else return super.getText();
	}
	
}
