package com.ostegn.jenesis.client.ui.base;

import java.util.Date;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

/**
 * Implements a HappeningLabel using an AbstractCell
 * @author Thiago Ricciardi
 * @see HappeningLabel
 */
public class HappeningCell extends AbstractCell<Date> {

	@Override
	public void render(Context context, Date value, SafeHtmlBuilder sb) {
		sb.appendEscaped(new HappeningLabel(value).getElement().getInnerHTML());
	}

}
