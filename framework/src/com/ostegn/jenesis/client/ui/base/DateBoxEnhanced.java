package com.ostegn.jenesis.client.ui.base;

import java.util.Date;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.datepicker.client.CalendarModel;
import com.google.gwt.user.datepicker.client.CalendarView;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.gwt.user.datepicker.client.DefaultCalendarView;
import com.google.gwt.user.datepicker.client.MonthSelector;

/**
 * An Enhanced DateBox with a label and a configurable DatePicker
 * @author Thiago Ricciardi
 * @see DatePickerEnhanced
 * @see MonthYearSelector
 */
public class DateBoxEnhanced extends DateBox {
	
	private Boolean isLabelVisible = false;
	private Boolean isEditing = false;
	
	private String label;

	/** Create a new instance of DateBoxEnhanced with no label, DatePickerEnhanced, no Date and the default format */
	public DateBoxEnhanced() {
		this(null);
	}
	
	/**
	 * Create a new instance of DateBoxEnhanced with the label, DatePickerEnhanced, no Date and the default format
	 * @param label the label string
	 */
	public DateBoxEnhanced(String label) {
		this(label, new DatePickerEnhanced(), null, new DefaultFormat());
	}

	/**
	 * Create a new instance of DateBoxEnhanced with no label, the DatePicker picker, the Date date and the Format format
	 * @param picker the DatePicker
	 * @param date the Date
	 * @param format the Format
	 */
	public DateBoxEnhanced(DatePicker picker, Date date, Format format) {
		this(null, picker, date, format);
	}
	
	/**
	 * Create a new instance of DateBoxEnhanced with the label, the DatePicker picker, the Date date and the Format format
	 * @param label the label string
	 * @param picker the DatePicker
	 * @param date the Date
	 * @param format the Format
	 */
	public DateBoxEnhanced(String label, DatePicker picker, Date date, Format format) {
		super(picker, date, format);
		init(label);
	}
	
	private void init(String label) {
		setLabel(label);
		showLabel();
		sinkEvents(Event.FOCUSEVENTS);
		addHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				if (isLabelVisible) {
					hideLabel();
				}
				isEditing = true;
			}
		}, FocusEvent.getType());
		addHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				isLabelVisible = (getValue() == null);
				if (isLabelVisible) {
					showLabel();
				}
				isEditing = false;
			}
		}, BlurEvent.getType());
	}
	
	private void showLabel() {
		addStyleDependentName("label");
		addStyleName("muted");
		getElement().setPropertyString("value", (getLabel() != null) ? getLabel() : "");
		isLabelVisible = true;
	}
	private void hideLabel() {
		removeStyleDependentName("label");
		removeStyleName("muted");
		super.setValue(super.getValue());
		isLabelVisible = false;
	}
	
	@Override
	public void setValue(Date date) {
		if ((isLabelVisible != null) ? isLabelVisible : false) {
			if (date != null) {
				hideLabel();
				super.setValue(date);
			}
		}
		else if (date == null && !((isEditing != null) ? isEditing : false)) {
			showLabel();
		}
		else {
			super.setValue(date);
		}
	}
	@Override
	public Date getValue() {
		if ((isLabelVisible != null) ? isLabelVisible : false) return null;
		else return super.getValue();
	}
	
	/**
	 * Get the label string
	 * @return a string
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * Set the label string
	 * @param label the label string
	 */
	public void setLabel(String label) {
		this.label = label;
		setTitle(label);
		getElement().setAttribute("placeholder", (label != null) ? label : "");
	}
	
}

/**
 * An Enhanced DatePicker with a customizable MonthSelector
 * @author Thiago Ricciardi
 * @see DateBoxEnhanced
 * @see MonthYearSelector
 */
class DatePickerEnhanced extends DatePicker {

	/** Create a new instance of DatePickerEnhanced with the month and year selector */
	public DatePickerEnhanced() {
		super(new MonthYearSelector(), new DefaultCalendarView(), new CalendarModel());
		MonthYearSelector monthSelector = (MonthYearSelector) this.getMonthSelector();
		monthSelector.setPicker(this);
		monthSelector.setModel(this.getModel());
	}

	/**
	 * Create a new instance of DatePickerEnhanced
	 * @param monthSelector the month/year selector
	 * @param view the calendar view
	 * @param model the calendar model
	 */
	public DatePickerEnhanced(MonthSelector monthSelector, CalendarView view, CalendarModel model) {
		super(monthSelector, view, model);
	}

	/** Refresh all components */
	public void refreshComponents() {
		super.refreshAll();
	}

}

/**
 * A MonthSelector that is easier to navigate between years and months
 * @author Thiago Ricciardi
 * @see DateBoxEnhanced
 * @see DatePickerEnhanced
 */
class MonthYearSelector extends MonthSelector {
	
	private static String BASE_NAME = "datePicker";
	
	private PushButton backwards;
	private PushButton forwards;
	private PushButton backwardsYear;
	private PushButton forwardsYear;
	private Grid grid;
	private int previousYearColumn = 0;
	private int previousMonthColumn = 1;
	private int monthColumn = 2;
	private int nextMonthColumn = 3;
	private int nextYearColumn = 4;
	private CalendarModel model;
	private DatePickerEnhanced picker;
	
	/**
	 * Set the calendar model
	 * @param model the calendar model
	 */
	public void setModel(CalendarModel model) {
		this.model = model;
	}
	
	/**
	 * Set the DatePicker
	 * @param picker
	 */
	public void setPicker(DatePickerEnhanced picker) {
		this.picker = picker;
	}
	
	@Override
	protected void refresh() {
		String formattedMonth = getModel().formatCurrentMonth();
		grid.setText(0, monthColumn, formattedMonth);
	}

	@Override
	protected void setup() {
		// Set up backwards.
		backwards = new PushButton();
		backwards.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addMonths(-1);
			}
		});
	
		backwards.getUpFace().setHTML("&lsaquo;");
		backwards.setStyleName(BASE_NAME + "PreviousButton");
	
		forwards = new PushButton();
		forwards.getUpFace().setHTML("&rsaquo;");
		forwards.setStyleName(BASE_NAME + "NextButton");
		forwards.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addMonths(+1);
			}
		});
	
	
		// Set up backwards year
		backwardsYear = new PushButton();
		backwardsYear.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addMonths(-12);
			}
		});
	
		backwardsYear.getUpFace().setHTML("&laquo;");
		backwardsYear.setStyleName(BASE_NAME + "PreviousButton");
	
		forwardsYear = new PushButton();
		forwardsYear.getUpFace().setHTML("&raquo;");
		forwardsYear.setStyleName(BASE_NAME + "NextButton");
		forwardsYear.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addMonths(+12);
			}
		});
	
		// Set up grid.
		grid = new Grid(1, 5);
		grid.setWidget(0, previousYearColumn, backwardsYear);
		grid.setWidget(0, previousMonthColumn, backwards);
		grid.setWidget(0, nextMonthColumn, forwards);
		grid.setWidget(0, nextYearColumn, forwardsYear);
	
		CellFormatter formatter = grid.getCellFormatter();
		formatter.setStyleName(0, monthColumn, BASE_NAME + "Month");
		formatter.setWidth(0, previousYearColumn, "1");
		formatter.setWidth(0, previousMonthColumn, "1");
		formatter.setWidth(0, monthColumn, "100%");
		formatter.setWidth(0, nextMonthColumn, "1");
		formatter.setWidth(0, nextYearColumn, "1");
		grid.setStyleName(BASE_NAME + "MonthSelector");
		initWidget(grid);
	}
	
	@Override
	public void addMonths(int numMonths) {
		model.shiftCurrentMonth(numMonths);
		picker.refreshComponents();
	}
	
}