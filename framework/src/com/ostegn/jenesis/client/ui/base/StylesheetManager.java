package com.ostegn.jenesis.client.ui.base;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;

/**
 * This class gives the ability to manipulate the page's stylesheets
 * @author Thiago Ricciardi
 *
 */
public class StylesheetManager {
	
	private static final String STYLESHEET_TAG = "link";
	private static final String STYLESHEET_REL = "rel";
	private static final String STYLESHEET_REL_VALUE = "stylesheet";
	private static final String STYLESHEET_HREF = "href";
	
	//private static final String THEME_ID = SimpleFunctions.newStringToken(10);
	
	private static Element currentTheme;
	
	/**
	 * Add or change a internally tracked stylesheet to serve as a theme stylesheet 
	 * @param href the address of the new stylesheet
	 */
	public static void setThemeStylesheet(String href) {
		if ( (href != null) ? href.length() > 0 : false) {
			if (currentTheme == null) {
				//currentTheme = addStylesheet(href, THEME_ID);
				currentTheme = addStylesheet(href);
			}
			else {
				currentTheme.setAttribute(STYLESHEET_HREF, href);
			}
		}
		else {
			removeThemeStylesheet();
		}
	}
	
	/** Remove the internally tracked stylesheet from the page */
	public static void removeThemeStylesheet() {
		if (currentTheme != null) {
			currentTheme.removeFromParent();
			currentTheme = null;
		}
	}
	
	/**
	 * Add a new stylesheet to the page without an id (or find a stylesheet by href if it already exists)
	 * @param href the address of the stylesheet to add
	 * @return the stylesheet element added (or found)
	 */
	public static Element addStylesheet(String href) {
		Element ret = findStylesheet(href);
		if (ret == null) {
			ret = addStylesheet(href, null);
		}
		return ret;
	}
	/**
	 * Add a new stylesheet to the page with an id (or find a stylesheet by id or href and change it)<br>
	 * If the an stylesheet with href address already exists, it will change the id of that stylesheet<br>
	 * If an id already exists on the page and it is an stylesheet, it will change that stylesheet's href (or remove it from the page if href is null)
	 * @param href the stylesheet address
	 * @param id the stylesheet id
	 * @return the added (or found) stylesheet
	 */
	public static Element addStylesheet(String href, String id) {
		Element ret = getStylesheet(id);
		if ((href != null) ? href.length() > 0 : false) {
			if (ret == null) ret = findStylesheet(href);
			if (ret == null) {
				ret = Document.get().createElement(STYLESHEET_TAG);
				ret.setAttribute(STYLESHEET_REL, STYLESHEET_REL_VALUE);
				Document.get().getHead().appendChild(ret);
			}
			if (ret != null && (id != null) ? (id.length() > 0) : false ) {
				ret.setId(id);
			}
			if (ret != null) {
				ret.setAttribute(STYLESHEET_HREF, href);
			}
		}
		else if (ret != null) {
			ret.removeFromParent();
		}
		return ret;
	}
	
	/**
	 * Find a stylesheet by a href address
	 * @param href a stylesheet address
	 * @return the stylesheet found or null
	 */
	public static Element findStylesheet(String href) {
		Element ret = null;
		if ( (href != null) ? href.length() > 0 : false ) {
			NodeList<Element> elements = Document.get().getHead().getElementsByTagName(STYLESHEET_TAG);
			for (int i = 0; i < elements.getLength(); i++) {
				Element element = elements.getItem(i);
				if (STYLESHEET_REL_VALUE.equalsIgnoreCase(element.getAttribute(STYLESHEET_REL)) && href.equals(element.getAttribute(STYLESHEET_HREF))) {
					ret = element;
					break;
				}
			}
		}
		return ret;
	}
	
	/**
	 * Get a stylesheet by id
	 * @param id the stylesheet id
	 * @return the stylesheet or null if the id does not exist or if it's not a stylesheet
	 */
	public static Element getStylesheet(String id) {
		Element ret = null;
		if ((id != null) ? (id.length() > 0) : false) {
			ret = Document.get().getElementById(id);
			if (!STYLESHEET_TAG.equalsIgnoreCase(ret.getTagName())) ret = null;
			else if (!STYLESHEET_REL_VALUE.equalsIgnoreCase(ret.getAttribute(STYLESHEET_REL))) ret = null;
		}
		return ret;
	}
	
	/**
	 * Remove the stylesheet from the page given it's id<br>
	 * Has the same behavior of {@code addStylesheet(null, id)}
	 * @param id the stylesheet id
	 * @return the element removed
	 */
	public static Element removeStylesheetById(String id) {
		Element e = getStylesheet(id);
		if (e != null) e.removeFromParent();
		return e;
	}
	
	/**
	 * Remove the stylesheet from the page given it's href
	 * @param href a stylesheet address
	 * @return the element removed
	 */
	public static Element removeStylesheetByHref(String href) {
		Element e = findStylesheet(href);
		if (e != null) e.removeFromParent();
		return e;
	}

}
