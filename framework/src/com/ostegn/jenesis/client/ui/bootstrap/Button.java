package com.ostegn.jenesis.client.ui.bootstrap;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;

/**
 * 
 * Creates a simple way to use Bootstrap styles on GWT Buttons.
 * 
 * @author Thiago Pereira Ricciardi
 *
 */
@Deprecated
public class Button extends com.google.gwt.user.client.ui.Button {
	
	/** Enumerated types that can be applied to the Bootstrap button */
	public enum Type {
		
		/** Standard gray button with gradient */
		DEFAULT(null),
		/** Provides extra visual weight and identifies the primary action in a set of buttons */
		PRIMARY("btn-primary"),
		/** Used as an alternative to the default styles */
		INFO("btn-info"),
		/** Indicates a successful or positive action */
		SUCCESS("btn-success"),
		/** Indicates caution should be taken with this action */
		WARNING("btn-warning"),
		/** Indicates a dangerous or potentially negative action */
		DANGER("btn-danger"),
		/** Alternate dark gray button, not tied to a semantic action or use */
		INVERSE("btn-inverse"),
		/** Deemphasize a button by making it look like a link while maintaining button behavior */
		LINK("btn-link");
		
		private final String css;
		
		Type(String css) {
			this.css = css;
		}
		
		public String getCss() {
			return css;
		}
		
	}
	
	/** Enumerated sizes that can be applied to the Bootstrap button */
	public enum Size {
		
		/** Larger than DEFAULT */
		LARGE("btn-large"),
		/** The default size, initializes with this value */
		DEFAULT(null),
		/** Smaller than DEFAULT */
		SMALL("btn-small"),
		/** The smallest size */
		MINI("btn-mini");
		
		private final String css;
		
		Size(String css) {
			this.css = css;
		}
		
		public String getCss() {
			return css;
		}
		
	}
	
	public Button() {
		super();
		init();
	}
	public Button(Type type) {
		super();
		init(type);
	}
	public Button(Size size) {
		super();
		init(size);
	}
	public Button(Type type, Size size) {
		super();
		init(type, size);
	}
	
	public Button(String text) {
		super(text);
		init();
	}
	public Button(String text, Type type) {
		super(text);
		init(type);
	}
	public Button(String text, Size size) {
		super(text);
		init(size);
	}
	public Button(String text, Type type, Size size) {
		super(text);
		init(type, size);
	}
	
	public Button(SafeHtml safeHtml) {
		super(safeHtml);
		init();
	}
	public Button(SafeHtml safeHtml, Type type) {
		super(safeHtml);
		init(type);
	}
	public Button(SafeHtml safeHtml, Size size) {
		super(safeHtml);
		init(size);
	}
	public Button(SafeHtml safeHtml, Type type, Size size) {
		super(safeHtml);
		init(type, size);
	}
	
	public Button(String text, ClickHandler handler) {
		super(text, handler);
		init();
	}
	public Button(String text, ClickHandler handler, Type type) {
		super(text, handler);
		init(type);
	}
	public Button(String text, ClickHandler handler, Size size) {
		super(text, handler);
		init(size);
	}
	public Button(String text, ClickHandler handler, Type type, Size size) {
		super(text, handler);
		init(type, size);
	}
	
	public Button(SafeHtml safeHtml, ClickHandler handler) {
		super(safeHtml, handler);
		init();
	}
	public Button(SafeHtml safeHtml, ClickHandler handler, Type type) {
		super(safeHtml, handler);
		init(type);
	}
	public Button(SafeHtml safeHtml, ClickHandler handler, Size size) {
		super(safeHtml, handler);
		init(size);
	}
	public Button(SafeHtml safeHtml, ClickHandler handler, Type type, Size size) {
		super(safeHtml, handler);
		init(type, size);
	}
	
	private Type type = Type.DEFAULT;
	
	/** Sets the type of the button */
	public void setType(Type type) {
		if (this.type == null) this.type = Type.DEFAULT;
		if (type == null) type = Type.DEFAULT;
		if (this.type.getCss() != null) removeStyleName(this.type.getCss());
		this.type = type;
		if (type.getCss() != null) addStyleName(type.getCss());
	}
	/** Gets the type of the button */
	public Type getType() {
		return type;
	}
	
	private Size size = Size.DEFAULT;
	
	/** Sets the size of the button */
	public void setSize(Size size) {
		if (this.size == null) this.size = Size.DEFAULT;
		if (size == null) size = Size.DEFAULT;
		if (this.size.getCss() != null) removeStyleName(this.size.getCss());
		this.size = size;
		if (size.getCss() != null) addStyleName(size.getCss());
	}
	/** Gets the size of the button */
	public Size getSize() {
		return size;
	}
	
	private Boolean blockDisplay = false;
	
	/** Create block level buttons (those that span the full width of a parent) */
	public void setBlockDisplay(Boolean blockDisplay) {
		if (blockDisplay == null) blockDisplay = false;
		if (blockDisplay) addStyleName("btn-block");
		else removeStyleName("btn-block");
		this.blockDisplay = blockDisplay;
	}
	/** Gets the block state of a button */
	public Boolean getBlockDisplay() {
		return blockDisplay;
	}
	
	private void init() {
		init(null, null);
	}
	private void init(Type type) {
		init(type, null);
	}
	private void init(Size size) {
		init(null, size);
	}
	private void init(Type type, Size size) {
		setStyleName();
		setType(type);
		setSize(size);
	}
	
	private void setStyleName() {
		addStyleName("btn");
		setType(type);
		setSize(size);
		setBlockDisplay(blockDisplay);
	}
	
	@Override
	public void setStyleName(String style) {
		super.setStyleName(style);
		setStyleName();
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (enabled) removeStyleName("disabled");
		else addStyleName("disabled");
	}
	
}
