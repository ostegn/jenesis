package com.ostegn.jenesis.client.ui.bootstrap;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class Icon extends Widget {
	
	private Element iconElement;
	
	public Icon(Icons icon) {
		this(icon, null);
	}
	
	public Icon(Icons icon, Colors color) {
		iconElement = Document.get().createElement("i");
		setElement(iconElement);
		setIcon(icon);
		setColor(color);
	}
	
	private Icons icon = Icons.NONE;
	public void setIcon(Icons icon) {
		if (this.icon == null) this.icon = Icons.NONE;
		if (icon == null) icon = Icons.NONE;
		if (this.icon.getCss() != null) iconElement.removeClassName(this.icon.getCss());
		this.icon = icon;
		if (this.icon.getCss() != null) iconElement.addClassName(this.icon.getCss());
	}
	public Icons getIcon() {
		return icon;
	}
	
	private Colors color = Colors.BLACK;
	public void setColor(Colors color) {
		if (this.color == null) this.color = Colors.BLACK;
		if (color == null) color = Colors.BLACK;
		if (this.color.getCss() != null) iconElement.removeClassName(this.color.getCss());
		this.color = color;
		if (color.getCss() != null) iconElement.addClassName(color.getCss());
	}
	public Colors getColor() {
		return color;
	}
	
	public enum Colors {
		
		BLACK(null),
		WHITE("icon-white");
		
		private final String css;
		
		Colors(String css) {
			this.css = css;
		}
		
		public String getCss() {
			return css;
		}
		
	}
	
	public enum Icons {
		
		NONE(null),
		GLASS("icon-glass"),
		MUSIC("icon-music"),
		SEARCH("icon-search"),
		ENVELOPE("icon-envelope"),
		HEART("icon-heart"),
		STAR("icon-star"),
		STAR_EMPTY("icon-star-empty"),
		USER("icon-user"),
		FILM("icon-film"),
		TH_LARGE("icon-th-large"),
		TH("icon-th"),
		TH_LIST("icon-th-list"),
		OK("icon-ok"),
		REMOVE("icon-remove"),
		ZOOM_IN("icon-zoom-in"),
		ZOOM_OUT("icon-zoom-out"),
		OFF("icon-off"),
		SIGNAL("icon-signal"),
		COG("icon-cog"),
		TRASH("icon-trash"),
		HOME("icon-home"),
		FILE("icon-file"),
		TIME("icon-time"),
		ROAD("icon-road"),
		DOWNLOAD_ALT("icon-download-alt"),
		DOWNLOAD("icon-download"),
		UPLOAD("icon-upload"),
		INBOX("icon-inbox"),
		PLAY_CIRCLE("icon-play-circle"),
		REPEAT("icon-repeat"),
		REFRESH("icon-refresh"),
		LIST_ALT("icon-list-alt"),
		LOCK("icon-lock"),
		FLAG("icon-flag"),
		HEADPHONES("icon-headphones"),
		VOLUME_OFF("icon-volume-off"),
		VOLUME_DOWN("icon-volume-down"),
		VOLUME_UP("icon-volume-up"),
		QRCODE("icon-qrcode"),
		BARCODE("icon-barcode"),
		TAG("icon-tag"),
		TAGS("icon-tags"),
		BOOK("icon-book"),
		BOOKMARK("icon-bookmark"),
		PRINT("icon-print"),
		CAMERA("icon-camera"),
		FONT("icon-font"),
		BOLD("icon-bold"),
		ITALIC("icon-italic"),
		TEXT_HEIGHT("icon-text-height"),
		TEXT_WIDTH("icon-text-width"),
		ALIGN_LEFT("icon-align-left"),
		ALIGN_CENTER("icon-align-center"),
		ALIGN_RIGHT("icon-align-right"),
		ALIGN_JUSTIFY("icon-align-justify"),
		LIST("icon-list"),
		INDENT_LEFT("icon-indent-left"),
		INDENT_RIGHT("icon-indent-right"),
		FACETIME_VIDEO("icon-facetime-video"),
		PICTURE("icon-picture"),
		PENCIL("icon-pencil"),
		MAP_MARKER("icon-map-marker"),
		ADJUST("icon-adjust"),
		TINT("icon-tint"),
		EDIT("icon-edit"),
		SHARE("icon-share"),
		CHECK("icon-check"),
		MOVE("icon-move"),
		STEP_BACKWARD("icon-step-backward"),
		FAST_BACKWARD("icon-fast-backward"),
		BACKWARD("icon-backward"),
		PLAY("icon-play"),
		PAUSE("icon-pause"),
		STOP("icon-stop"),
		FORWARD("icon-forward"),
		FAST_FORWARD("icon-fast-forward"),
		STEP_FORWARD("icon-step-forward"),
		EJECT("icon-eject"),
		CHEVRON_LEFT("icon-chevron-left"),
		CHEVRON_RIGHT("icon-chevron-right"),
		PLUS_SIGN("icon-plus-sign"),
		MINUS_SIGN("icon-minus-sign"),
		REMOVE_SIGN("icon-remove-sign"),
		OK_SIGN("icon-ok-sign"),
		QUESTION_SIGN("icon-question-sign"),
		INFO_SIGN("icon-info-sign"),
		SCREENSHOT("icon-screenshot"),
		REMOVE_CIRCLE("icon-remove-circle"),
		OK_CIRCLE("icon-ok-circle"),
		BAN_CIRCLE("icon-ban-circle"),
		ARROW_LEFT("icon-arrow-left"),
		ARROW_RIGHT("icon-arrow-right"),
		ARROW_UP("icon-arrow-up"),
		ARROW_DOWN("icon-arrow-down"),
		SHARE_ALT("icon-share-alt"),
		RESIZE_FULL("icon-resize-full"),
		RESIZE_SMALL("icon-resize-small"),
		PLUS("icon-plus"),
		MINUS("icon-minus"),
		ASTERISK("icon-asterisk"),
		EXCLAMATION_SIGN("icon-exclamation-sign"),
		GIFT("icon-gift"),
		LEAF("icon-leaf"),
		FIRE("icon-fire"),
		EYE_OPEN("icon-eye-open"),
		EYE_CLOSE("icon-eye-close"),
		WARNING_SIGN("icon-warning-sign"),
		PLANE("icon-plane"),
		CALENDAR("icon-calendar"),
		RANDOM("icon-random"),
		COMMENT("icon-comment"),
		MAGNET("icon-magnet"),
		CHEVRON_UP("icon-chevron-up"),
		CHEVRON_DOWN("icon-chevron-down"),
		RETWEET("icon-retweet"),
		SHOPPING_CART("icon-shopping-cart"),
		FOLDER_CLOSE("icon-folder-close"),
		FOLDER_OPEN("icon-folder-open"),
		RESIZE_VERTICAL("icon-resize-vertical"),
		RESIZE_HORIZONTAL("icon-resize-horizontal"),
		HDD("icon-hdd"),
		BULLHORN("icon-bullhorn"),
		BELL("icon-bell"),
		CERTIFICATE("icon-certificate"),
		THUMBS_UP("icon-thumbs-up"),
		THUMBS_DOWN("icon-thumbs-down"),
		HAND_RIGHT("icon-hand-right"),
		HAND_LEFT("icon-hand-left"),
		HAND_UP("icon-hand-up"),
		HAND_DOWN("icon-hand-down"),
		CIRCLE_ARROW_RIGHT("icon-circle-arrow-right"),
		CIRCLE_ARROW_LEFT("icon-circle-arrow-left"),
		CIRCLE_ARROW_UP("icon-circle-arrow-up"),
		CIRCLE_ARROW_DOWN("icon-circle-arrow-down"),
		GLOBE("icon-globe"),
		WRENCH("icon-wrench"),
		TASKS("icon-tasks"),
		FILTER("icon-filter"),
		BRIEFCASE("icon-briefcase"),
		FULLSCREEN("icon-fullscreen");
		
		private String css;
		
		Icons(String css) {
			this.css = css;
		}
		
		public String getCss() {
			return css;
		}
		
	}

}
