package com.ostegn.jenesis.client.ui.bootstrap;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class CombinedWidget extends Composite {
	
	//TODO: addPrepend, addAppend (like styleName - setStyleName, *addStylename)
	
	private final FlowPanel me = new FlowPanel();
	
	private Widget prependAddOn;
	private Widget appendAddOn;
	
	private Widget prependWidget;
	private Widget mainWidget;
	private Widget appendWidget;
	
	public CombinedWidget() {
		initWidget(me);
	}
	public CombinedWidget(Widget mainWidget) {
		this();
		setMainWidget(mainWidget);
	}
	public CombinedWidget(Widget prependWidget, Widget mainWidget, Widget appendWidget) {
		this();
		setPrependWidget(prependWidget);
		setMainWidget(mainWidget);
		setAppendWidget(appendWidget);
	}
	
	public Widget getPrependWidget() {
		return prependWidget;
	}
	public void setPrependWidget(Widget prependWidget) {
		if (this.prependWidget != null) {
			if (prependAddOn != null) {
				remove(prependAddOn);
				removeChildren(prependAddOn);
				prependAddOn = null;
			}
			else {
				remove(this.prependWidget);
				this.prependWidget.removeStyleName("add-on");
			}
			removeStyleName("input-prepend");
		}
		this.prependWidget = prependWidget;
		if (this.prependWidget != null) {
			prependAddOn = createAddOn(this.prependWidget);
			if (prependAddOn != null) {
				add(prependAddOn);
			}
			else {
				this.prependWidget.addStyleName("add-on");
				add(this.prependWidget);
			}
			addStyleName("input-prepend");
		}
	}
	public Widget getMainWidget() {
		return mainWidget;
	}
	public void setMainWidget(Widget mainWidget) {
		if (this.mainWidget != null) {
			remove(mainWidget);
		}
		this.mainWidget = mainWidget;
		if (this.mainWidget != null) {
			add(this.mainWidget);
		}
	}
	public Widget getAppendWidget() {
		return appendWidget;
	}
	public void setAppendWidget(Widget appendWidget) {
		if (this.appendWidget != null) {
			if (appendAddOn != null) {
				remove(appendAddOn);
				removeChildren(appendAddOn);
				appendAddOn = null;
			}
			else {
				remove(this.appendWidget);
				this.appendWidget.removeStyleName("add-on");
			}
			removeStyleName("input-append");
		}
		this.appendWidget = appendWidget;
		if (this.appendWidget != null) {
			appendAddOn = createAddOn(this.appendWidget);
			if (appendAddOn != null) {
				add(appendAddOn);
			}
			else {
				this.appendWidget.addStyleName("add-on");
				add(this.appendWidget);
			}
			addStyleName("input-append");
		}
	}
	
	private void add(Widget child) {
		int idx = -1;
		if (child.equals(prependAddOn) || child.equals(prependWidget)) {
			idx = me.getWidgetIndex(mainWidget);
		}
		else if (child.equals(mainWidget)) {
			idx = me.getWidgetIndex(appendAddOn) * me.getWidgetIndex(appendWidget) * -1;
		}
		else if (child.equals(appendAddOn) || child.equals(appendWidget)) {
			idx = me.getWidgetCount();
		}
		if ((!(idx < 0)) && idx < me.getWidgetCount()) me.insert(child, idx);
		else me.add(child);
	}
	@SuppressWarnings("unused")
	private void add(Element child) {
		add(child, getElement());
	}
	private void add(Element child, Element container) {
		child.removeFromParent();
		//DOM.appendChild(container, child);
		container.appendChild(child);
	}
	
	private void remove(Widget child) {
		me.remove(child);
	}
	
	private void removeChildren(Widget parent) {
		removeChildren(parent.getElement());
	}
	private void removeChildren(Element parent) {
		for (int i = 0; i < parent.getChildCount(); i++) {
			parent.removeChild(parent.getChild(i));
		}
	}
	
	private Widget createAddOn(Widget child) {
		Widget ret = null;
		Element addOn = createAddOn(child.getElement());
		if (addOn != null) {
			ret = Label.wrap(addOn);
		}
		return ret;
	}
	private Element createAddOn(Element child) {
		Element ret = null;
		if (child.getTagName().equalsIgnoreCase("i")) {
			//ret = DOM.createSpan();
			ret = Document.get().createSpanElement();
			Document.get().getBody().appendChild(ret);
			ret.addClassName("add-on");
			add(child, ret);
		}
		return ret;
	}
	
}
