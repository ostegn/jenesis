package com.ostegn.jenesis.client.ui.bootstrap;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class ButtonGroup extends FlowPanel {
	
	public ButtonGroup() {
		super();
		setStyleName();
	}
	
	@Override
	public void add(Widget w) {
		if (w instanceof Button) super.add(w);
	}
	public void add(Button b) {
		super.add(b);
	}
	
	@Override
	public void insert(IsWidget w, int beforeIndex) {
		if (w instanceof Button) super.insert(w, beforeIndex);
	}
	@Override
	public void insert(Widget w, int beforeIndex) {
		// TODO Auto-generated method stub
		if (w instanceof Button) super.insert(w, beforeIndex);
	}
	public void insert(Button b, int beforeIndex) {
		super.insert(b, beforeIndex);
	}
	
	private void setStyleName() {
		addStyleName("btn-group");
	}
	@Override
	public void setStyleName(String style) {
		super.setStyleName(style);
		setStyleName();
	}

}
