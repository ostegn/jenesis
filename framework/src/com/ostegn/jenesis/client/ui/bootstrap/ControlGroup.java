package com.ostegn.jenesis.client.ui.bootstrap;

import java.util.Collection;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class ControlGroup extends FlowPanel {
	
	private States state = States.NONE;
	
	public ControlGroup() {
		super();
		setStyleName();
	}
	public ControlGroup(Widget widget) {
		this();
		add(widget);
	}
	public ControlGroup(Widget[] widgets) {
		this();
		for (Widget widget : widgets) {
			add(widget);
		}
	}
	public ControlGroup(Collection<Widget> widgets) {
		this();
		for (Widget widget : widgets) {
			add(widget);
		}
	}
	
	private void setStyleName() {
		addStyleName("control-group");
	}
	@Override
	public void setStyleName(String style) {
		super.setStyleName(style);
		setStyleName();
	}
	
	public void setState(States state) {
		if (this.state == null) this.state = States.NONE;
		if (state == null) state = States.NONE;
		if (this.state.getCss() != null) removeStyleName(this.state.getCss());
		this.state = state;
		if (state.getCss() != null) addStyleName(state.getCss());
	}
	public States getState() {
		return state;
	}
	
	public enum States {
		
		NONE(null),
		WARNING("warning"),
		ERROR("error"),
		INFO("info"),
		SUCCESS("success");
		
		private String css;
		
		States(String css) {
			this.css = css;
		}
		
		public String getCss() {
			return css;
		}
		
	}

}
