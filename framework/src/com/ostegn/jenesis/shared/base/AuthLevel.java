package com.ostegn.jenesis.shared.base;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 
 * This is a simple class with 8+1 levels for access control.<br>
 * <b>This model has NOT been made to be editable at runtime.</b><br>
 * Between LEVEL and SUPER_LEVEL we have room for 2 more levels
 * that are unimplemented (meaning that this model supports 15+1 levels).<br>
 * Every higher level contains all the lower levels.
 * <br><br>
 * Example: A {@code SUPER_USER} contains {@code SUPER_USER},
 * {@code USER} and {@code ANONYMOUS} levels.
 * <br><br>
 * Hierarchy:<br>
 * |_ <b>SUPER_ADMIN</b> -> the higest level of this hierarchy.<br>
 * |__ <b>ADMIN</b> -> the first level higher than SUPER_MANAGER<br>
 * |___ <b>SUPER_MANAGER</b> -> the highest MANAGER level<br>
 * |____ <b>MANAGER</b> -> the first level higher than SUPER_EDITOR<br>
 * |_____ <b>SUPER_EDITOR</b> -> the highest EDITOR level<br>
 * |______ <b>EDITOR</b> -> the first level higher than SUPER_USER<br>
 * |_______ <b>SUPER_USER</b> -> the highest USER level<br>
 * |________ <b>USER</b> -> the lowest signed in level<br>
 * |_________ <b>ANONYMOUS</b> -> lowest level of this hierarchy, <b>not signed in</b><br>
 * 
 * @author Thiago Ricciardi
 *
 */
public enum AuthLevel {
	
	//Bitwise with hierarchy!
	//0x1, 0x3, 0x7, 0xF (every higher level contains all lower)
	//0x1, 0x2, 0x4, 0x8 (every level independent)
	/** 0x0000 <br> The lowest level of this hierarchy, <b>not signed in</b>.<br> 
	 * Contains only ANONYMOUS level. */
	ANONYMOUS(0x0000, "ROLE_ANONYMOUS"),
	
	/** 0x0001 <br> The lowest signed in level.<br>
	 * Contains USER and ANONYMOUS levels. */
	USER(0x0001, "ROLE_USER"),
	
	/** 0x000F <br> The highest USER level.<br>
	 * Contains SUPER_USER, USER and ANONYMOUS levels. */
	SUPER_USER(0x000F, "ROLE_SUPERUSER"),
	
	/** 0x001F <br> The first level higher than SUPER_USER.<br>
	 * Contains EDITOR, SUPER_USER, USER and ANONYMOUS levels. */
	EDITOR(0x001F, "ROLE_EDITOR"),
	
	/** The highest EDITOR level.<br>
	 * Contains SUPER_EDITOR, EDITOR, SUPER_USER, USER and ANONYMOUS levels. */
	SUPER_EDITOR(0x001F, "ROLE_SUPEREDITOR"),
	
	/** The first level higher than SUPER_EDITOR.<br>
	 * Contains MANAGER, SUPER_EDITOR, EDITOR, SUPER_USER, USER and ANONYMOUS levels. */
	MANAGER(0x01FF, "ROLE_MANAGER"),
	
	/** The highest MANAGER level.<br>
	 * Contains SUPER_MANAGER, MANAGER, SUPER_EDITOR, EDITOR, SUPER_USER, USER and ANONYMOUS levels. */
	SUPER_MANAGER(0x0FFF, "ROLE_SUPERMANAGER"),
	
	/** The first level higher than SUPER_MANAGER.<br>
	 * Contains ADMIN, SUPER_MANAGER, MANAGER, SUPER_EDITOR, EDITOR, SUPER_USER, USER and ANONYMOUS levels. */
	ADMIN(0x1FFF, "ROLE_ADMIN"),
	
	/** The higest level of this hierarchy.<br>
	 * Contains SUPER_ADMIN, ADMIN, SUPER_MANAGER, MANAGER, SUPER_EDITOR, EDITOR, SUPER_USER, USER and ANONYMOUS levels. (All levels) */
	SUPER_ADMIN(0xFFFF, "ROLE_SUPERADMIN");
	
	private final Integer id;
	private final String role;
	
	AuthLevel(Integer id, String role) {
		this.id = id;
		this.role = role;
	}
	
	/** Gets the Bitwise ID of the level. */
	public Integer getId() {
		return id;
	}
	/** <b>DEPRECATED</b> - Use {@code getRole()} instead.<br><br>
	 * Gets the Human Readable string that represent the level. */
	@Deprecated
	public String getHumanReadable() {
		return getRole();
	}
	/** Gets the Role string that represents the level. */
	public String getRole() {
		return role;
	}
	
	/** 
	 * Verifies if the level contains the authlevel.
	 * @param authlevel The level to verify.
	 * @return true if it contains, false otherwise.
	 */
	public Boolean hasAuthLevel(Integer authlevel) {
		return ( (authlevel & id) == id );
	}
	
	/**
	 * Generates the Roles Collection containing all the levels that it represents.
	 * @return a collection containing all the string roles.
	 */
	public Collection<String> getRoles() {
		return getRoles(id);
	}
	
	/**
	 * <b>DEPRECATED</b> - Use {@code getRoles(Integer authlevel)} instead.<br><br>
	 * Generates the Human Readable Collection containing all the levels that the authlevel represents.
	 * @param authlevel The level of analysis.
	 * @return a collection containing all the human readable levels.
	 */
	@Deprecated
	public static Collection<String> toHumanReadable(Integer authlevel) {
		return getRoles(authlevel);
	}
	/**
	 * Generates the Roles Collection containing all the levels that the authlevel represents.
	 * @param authlevel The level of analysis.
	 * @return a collection containing all the role levels.
	 */
	public static Collection<String> getRoles(Integer authlevel) {
		ArrayList<String> ret = new ArrayList<String>();
		for (AuthLevel role : AuthLevel.getAuthLevel(authlevel)) {
			ret.add(role.getRole());
		}
		return ret;
	}
	
	/**
	 * Generates the AuthLevel Collection containing all the levels that the authlevel value represents.
	 * @param authlevel The level of analysis.
	 * @return a collection containing all the AuthLevel levels.
	 */
	public static Collection<AuthLevel> getAuthLevel(Integer authlevel) {
		ArrayList<AuthLevel> ret = new ArrayList<AuthLevel>();
		for (AuthLevel role : AuthLevel.values()) {
			if (role.hasAuthLevel(authlevel)) {
				ret.add(role);
			}
		}
		return ret;
	}
	
	/**
	 * Generates the ID that contains all the roles in the collection.
	 * @param roles The roles to be joined.
	 * @return An ID.
	 */
	public static Integer toId(Collection<AuthLevel> roles) {
		Integer ret = 0;
		for (AuthLevel role : roles) {
			ret |= role.getId();
		}
		return ret;
	}
	
	/**
	 * Converts a String Role into an AuthLevel.
	 * @param role The String representing the role.
	 * @return The equivalent AuthLevel.
	 */
	public static AuthLevel toAuthLevel(String role) {
		AuthLevel ret = null;
		for (AuthLevel a : AuthLevel.values()) {
			if (a.getRole().equalsIgnoreCase(role)) {
				ret = a;
				break;
			}
		}
		return ret;
	}
	
}
	
