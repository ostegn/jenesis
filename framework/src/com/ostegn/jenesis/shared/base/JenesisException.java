package com.ostegn.jenesis.shared.base;

import java.io.Serializable;

/**
 * This class creates an exception that can be shared between the server and the client (GWT)
 * @author Thiago Ricciardi
 *
 */
@SuppressWarnings("serial")
public class JenesisException extends Exception implements Serializable {
	
	/** Predefined generic error type */
	public static final String GENERIC = "GENERIC";
	/** Predefined unexpected error type */
	public static final String UNEXPECTED = "UNEXPECTED";
	
	private String type = GENERIC;
	
	/** Create a new JenesisException with no message */
	public JenesisException() {}
	
	/**
	 * Create a new JenesisException the string message
	 * @param message the string message
	 */
	public JenesisException(String message) {
		super(message);
	}
	/**
	 * Create a new JenesisException the throwable
	 * @param throwable the throwable object
	 */
	public JenesisException(Throwable throwable) {
		super(throwable);
	}
	/**
	 * Create a new JenesisException the string message and a throwable
	 * @param message the string message
	 * @param throwable the throwable object
	 */
	public JenesisException(String message, Throwable throwable) {
		super(message, throwable);
	}
	/**
	 * Create a new JenesisException the string message and the string type
	 * @param message the string message
	 * @param type the string type
	 */
	public JenesisException(String message, String type) {
		super(message);
		this.type = type;
	}

	/**
	 * Get the string type
	 * @return The predefined string type or a custom one
	 */
	public String getType() {
		return type;
	}
	
}
