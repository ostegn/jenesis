package com.ostegn.jenesis.shared.base;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.regexp.shared.RegExp;

/**
 * Utility to replace tags.<br>
 * i.e. Replacing variable tags in a template.
 * 
 * @author Thiago Ricciardi
 *
 */
public class TagReplacer {
	
	/** Specify the case sensitivity */
	public enum Case {
		/** Does not ignore case */
		SENSITIVE,
		/** Ignore case */
		INSENSITIVE("(?i)");
		
		public String value = "";
		
		Case() {}
		Case(String value) {
			this.value = value;
		}
	}
	
	private static final String REGEXP_SPECIAL = "[]\\^$.|?*+(){}";
	
	private String prefix = "${";
	private String suffix = "}";
	private Case sensitivity = Case.INSENSITIVE;
	
	/**
	 * Creates a {@link TagReplacer} with the default values:<br>
	 * <b>${</b> as {@code prefix}, <b>}</b> as {@code suffix} and <b>Case.INSENSITIVE</b> as {@code sensitivity}.<br><br>
	 * The tag to be replaced will become ${tag} and the case will be insensitive.
	 */
	public TagReplacer() { }
	
	/**
	 * Creates a {@link TagReplacer} initializing the insensitivity.
	 * @param sensitivity The case sensitivity to consider.
	 */
	public TagReplacer(Case sensitivity) {
		this.sensitivity = sensitivity;
	}
	
	/**
	 * Creates a {@link TagReplacer} initializing the prefix and suffix.
	 * @param prefix The prefix to be added to the tag.
	 * @param suffix The suffix to be added to the tag.
	 */
	public TagReplacer(String prefix, String suffix) {
		this.prefix = prefix;
		this.suffix = suffix;
	}
	
	/**
	 * Creates a {@link TagReplacer} initializing the prefix, suffix and sensitivity.
	 * @param prefix The prefix to be added to the tag.
	 * @param suffix The suffix to be added to the tag.
	 * @param sensitivity The case sensitivity to consider.
	 */
	public TagReplacer(String prefix, String suffix, Case sensitivity) {
		this.prefix = prefix;
		this.suffix = suffix;
		this.sensitivity = sensitivity;
	}
	
	/**
	 * Replaces all the tags with the replacements in the str.
	 * @param keyValue A key/value map containing the tag(key) and the replacement(value).
	 * @param str The string to replace the tags.
	 * @return A new string with the tags replaced by the replacements.
	 */
	public String replace(Map<String, String> keyValue, String str) {
		String ret = str;
		if (ret != null && keyValue != null) {
			String prefix = replaceRegExp(getPrefix());
			String suffix = replaceRegExp(getSuffix());
			for (String key : keyValue.keySet()) {
				String repl = prefix + replaceRegExp((key != null) ? key : "") + suffix;
				String replacement = keyValue.get(key);
				replacement = quoteReplacement((replacement != null) ? replacement : "");
				try {
					ret = removeQuoteReplacement(ret.replaceAll(getSensitivity().value + repl, replacement));
				}
				catch (Exception e) {
					ret = removeQuoteReplacement(RegExp.compile(repl, "g" + (getSensitivity() == Case.INSENSITIVE ? "i" : "")).replace(ret, replacement));
					continue;
				}
			}
		}
		return ret;
	}
	
	/**
	 * Replaces a single tag with the replacement inside the str.
	 * @param tag The tag to replace.
	 * @param replacement The replacement string.
	 * @param str The string containing the tag.
	 * @return A new string with the tag replaced by replacement.
	 */
	public String replace(String tag, String replacement, String str) {
		Map<String, String> keyValue = new HashMap<>();
		keyValue.put(tag, replacement);
		return replace(keyValue, str);
	}
	
	private String replaceRegExp(String str) {
		String ret = str;
		for (char c : REGEXP_SPECIAL.toCharArray()) {
			String target = new String(new char[] {c});
			String replacement = "\\" + target;
			ret = ret.replace(target, replacement);
		}
		return ret;
	}
	
	private String quoteReplacement(String str) {
		if ((str.indexOf('\\') == -1) && (str.indexOf('$') == -1)) return str;
		String ret = "";
		for (int i=0; i<str.length(); i++) {
			char c = str.charAt(i);
			if (c == '\\' || c == '$') {
				ret += '\\';
			}
			ret += c;
		}
		return ret;
	}
	
	private String removeQuoteReplacement(String str) {
		if ((str.indexOf('\\') == -1) && (str.indexOf('$') == -1)) return str;
		String ret = "";
		for (int i=0; i<(str.length()-1); i++) {
			char c = str.charAt(i);
			char next = str.charAt(i+1);
			if (!(c == '\\' && (next == '\\' || next == '$'))) {
				ret += c;
			}
		}
		ret += str.charAt(str.length()-1);
		return ret;
	}
	
	/** The prefix to be added to the tag. */
	public String getPrefix() {
		return prefix;
	}
	/** The prefix to be added to the tag. */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	/** The suffix to be added to the tag. */
	public String getSuffix() {
		return suffix;
	}
	/** The suffix to be added to the tag. */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	/** The case sensitivity to consider. */
	public Case getSensitivity() {
		return sensitivity;
	}
	/** The case sensitivity to consider. */
	public void setSensitivity(Case sensitivity) {
		this.sensitivity = sensitivity;
	}

}
