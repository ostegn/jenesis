package com.ostegn.jenesis.shared.base;

/**
 * Define that it has a PersistentObject attached to it
 * @author Thiago Ricciardi
 *
 */
public interface HasPersistentObject {

	/** Get the Persistent Object attached to this object */
	public IPersistentObject getPersistentObject();
	/** Set the Persistent Object attached to this object */
	public void setPersistentObject(IPersistentObject persistentObject);

}
