package com.ostegn.jenesis.shared.base;

import java.math.BigInteger;
import java.util.Date;

import com.google.gwt.i18n.shared.DateTimeFormat;

/**
 * 
 * Simple shared (client and server) functions.
 * 
 * @author Thiago Ricciardi
 *
 */
public class SimpleFunctions {
	
	/**
	 * Tries to get the boolean value given a number or string.<br>
	 * 1/yes/true are interpreted as <b>true</b>.<br>
	 * 0/no/false are interpreted as <b>false</b>.<br>
	 * All other values are considered invalid and return <i>null</i>.
	 * @param bol An object representing a boolean value.
	 * @return The boolean object translated. Null if not a boolean value.
	 */
	public static Boolean getBooleanValue(Object bol) {
		Boolean ret = null;
		if (bol != null) {
			Integer i = getIntegerValue(bol);
			if (i != null) {
				if (i == 1) ret = true;
				else if (i == 0) ret = false;
			}
			else if (bol instanceof String) {
				if ( "true".equalsIgnoreCase((String) bol) || "yes".equalsIgnoreCase((String) bol) ) ret = true;
				else if ( "false".equalsIgnoreCase((String) bol) || "no".equalsIgnoreCase((String) bol) ) ret = false;
			}
			else if (bol.getClass() == Boolean.class) {
				ret = (Boolean) bol;
			}
		}
		return ret;
	}
	
	/**
	 * Tries to get the integer value given a number.
	 * @param num An object representing a number.
	 * @return The integer object translated. Null if not an integer.
	 */
	public static Integer getIntegerValue(Object num) {
		Integer ret = null;
		if (num != null) {
			if (num.getClass() == Long.class) {
				ret = ((Long) num).intValue();
			}
			else if (num.getClass() == Integer.class) {
				ret = ((Integer) num);
			}
			else if (num.getClass() == BigInteger.class) {
				ret = ((BigInteger) num).intValue();
			}
			else if (num.getClass() == Double.class) {
				ret = ((Double) num).intValue();
			}
			else if (num.getClass() == Float.class) {
				ret = ((Float) num).intValue();
			}
			else {
				try {
					ret = Integer.parseInt(num.toString());
				} catch (NumberFormatException e) {}
			}
		}
		return ret;
	}
	
	/**
	 * Tries to get the long value given a number.
	 * @param num An object representing a number.
	 * @return The long object translated. Null if not a long.
	 */
	public static Long getLongValue(Object num) {
		Long ret = null;
		if (num != null) {
			if (num.getClass() == Long.class) {
				ret = (Long) num;
			}
			else if (num.getClass() == Integer.class) {
				ret = ((Integer) num).longValue();
			}
			else if (num.getClass() == BigInteger.class) {
				ret = ((BigInteger) num).longValue();
			}
			else if (num.getClass() == Double.class) {
				ret = ((Double) num).longValue();
			}
			else if (num.getClass() == Float.class) {
				ret = ((Float) num).longValue();
			}
			else {
				try {
					ret = Long.parseLong(num.toString());
				} catch (NumberFormatException e) {}
			}
		}
		return ret;
	}
	
	/**
	 * Tries to get the big integer value given a number.
	 * @param num An object representing a number.
	 * @return The BigInteger object translated. Null if not a BigInteger.
	 */
	public static BigInteger getBigIntegerValue(Object num) {
		BigInteger ret = null;
		if (num != null) {
			if (num.getClass() == BigInteger.class) {
				ret = (BigInteger) num;
			}
			else {
				try {
					ret = new BigInteger(num.toString());
				} catch (NumberFormatException e) {}
			}
		}
		return ret;
	}
	
	/**
	 * Tries to get the double value given a number.
	 * @param num An object representing a number.
	 * @return The long object translated. Null if not a double.
	 */
	public static Double getDoubleValue(Object num) {
		Double ret = null;
		if (num != null) {
			if (num.getClass() == Double.class) {
				ret = (Double) num;
			}
			else if (num.getClass() == Float.class) {
				ret = ((Float) num).doubleValue();
			}
			else if (num.getClass() == Long.class) {
				ret = ((Long) num).doubleValue();
			}
			else if (num.getClass() == Integer.class) {
				ret = ((Integer) num).doubleValue();
			}
			else if (num.getClass() == BigInteger.class) {
				ret = ((BigInteger) num).doubleValue();
			}
			else {
				try {
					ret = Double.parseDouble(num.toString());
				} catch (NumberFormatException e) {}
			}
		}
		return ret;
	}
	
	/**
	 * Tries to get the float value given a number.
	 * @param num An object representing a number.
	 * @return The float object translated. Null if not a double.
	 */
	public static Float getFloatValue(Object num) {
		Float ret = null;
		if (num != null) {
			if (num.getClass() == Double.class) {
				ret = ((Double) num).floatValue();
			}
			else if (num.getClass() == Float.class) {
				ret = (Float) num;
			}
			else if (num.getClass() == Long.class) {
				ret = ((Long) num).floatValue();
			}
			else if (num.getClass() == Integer.class) {
				ret = ((Integer) num).floatValue();
			}
			else if (num.getClass() == BigInteger.class) {
				ret = ((BigInteger) num).floatValue();
			}
			else {
				try {
					ret = Float.parseFloat(num.toString());
				} catch (NumberFormatException e) {}
			}
		}
		return ret;
	}
	
	/**
	 * Generates an integer random number given the range. <b>(min <= random <= max)</b>
	 * @param min Minimum value for the range.
	 * @param max Maximum value for the range.
	 * @return A random number inside the range.
	 */
	public static int smartRandom(int min, int max) {
		return Double.valueOf(smartRandom(Integer.valueOf(min).doubleValue(), Integer.valueOf(max).doubleValue())).intValue();
	}
	
	/**
	 * Generates a double random number given the range. <b>(min <= random < max)</b><br>
	 * As the {@code Math.random()} doesn't ever return 1, this function does never return the max value.
	 * @param min Minimum value for the range.
	 * @param max Maximum value for the range.
	 * @return A random number inside the range.
	 */
	public static double smartRandom(double min, double max) {
		if (max < min) {
			double aux = min;
			min = max;
			max = aux;
		}
		return min + (Math.random() * (max - min));
	}
	
	/**
	 * Generates a String token given the length for it.<br>
	 * It only generates alpha-numeric characters.
	 * @param length The length of the token.
	 * @return The String token.
	 */
	public static String newStringToken(int length) {
		String ret = new String();
		for (int i = 0; i < length; i++) {
			int rnd = smartRandom(0, 60);
			if (rnd < 25) {
				ret += (char) smartRandom(0x41, 0x5A);
			}
			else if (rnd < 50) {
				ret += (char) smartRandom(0x61, 0x7A);
			}
			else {
				ret += (char) smartRandom(0x30, 0x39);
			}
		}
		return ret;
	}
	
	/**
	 * Verify if an e-mail is valid or not.
	 * @param email The e-mail of verification.
	 * @return <b>true</b> only if the e-mail is considered valid. If it's {@code null} returns <b>false</b>.
	 */
	public static Boolean validateEmail(String email) {
		Boolean ret = false;
		if (email != null) {
			ret = email.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$");
		}
		return ret;
	}
	
	/**
	 * Verify if a string meets the minimum length requirements.
	 * @param str The string of verification.
	 * @param minimum_length The minimum length requirements.
	 * @return <b>true</b> if the string's length is above or equal the minimum length. {@code null} is considered invalid.
	 */
	public static Boolean validateMinLength(String str, int minimum_length) {
		Boolean ret = false;
		if (str != null) {
			ret = !(str.length() < minimum_length);
		}
		return ret;
	}
	
	/**
	 * Verify if a string meets the maximum length requirements.
	 * @param str The string of verification.
	 * @param maximum_length The maximum length requirements.
	 * @return <b>true</b> if the string's length is below or equal the maximum length. {@code null} is considered invalid.
	 */
	public static Boolean validateMaxLength(String str, int maximum_length) {
		Boolean ret = false;
		if (str != null) {
			ret = !(str.length() > maximum_length);
		}
		return ret;
	}
	
	/**
	 * Gets the file name and extension from a URL or path.
	 * @param path The URL or path to parse.
	 * @return The file name and extension.
	 */
	public static String getFile(String path) {
		String ret = null;
		if (path != null) {
			ret = path;
			int b = path.lastIndexOf("\\");
			int r = path.lastIndexOf("/");
			int startIndex = -1;
			if (b > r) {
				startIndex = b;
			}
			else {
				startIndex = r;
			}
			if (!(startIndex < 0) && startIndex < path.length()) {
				ret = path.substring(startIndex+1);
			}
		}
		return ret;
	}
	
	/**
	 * Gets only the file name without extension from a URL or path.
	 * @param path The URL or path to parse.
	 * @return The file name.
	 */
	public static String getFileName(String path) {
		String ret = null;
		String file = getFile(path);
		if (file != null) {
			ret = file;
			int endIndex = file.lastIndexOf(".");
			if (!(endIndex < 0)) {
				ret = file.substring(0, endIndex);
			}
		}
		return ret;
	}
	
	/**
	 * Gets only the file extension (without the dot ".") from a URL or path.
	 * @param path The URL or path to parse.
	 * @return The file extension.
	 */
	public static String getFileExtension(String path) {
		String ret = null;
		String file = getFile(path);
		if (file != null) {
			ret = "";
			int startIndex = file.lastIndexOf(".");
			if (!(startIndex < 0)) {
				ret = file.substring(startIndex+1);
			}
		}
		return ret;
	}
	
	/**
	 * Sets the time for a given date.
	 * @param target The date representing the date/time to be changed.
	 * @param hour The hour to set.
	 * @param min The minute to set.
	 * @param sec The second to set.
	 * @param millisec The milliseconds to set.
	 * @return The same input date changed (for chain purposes).
	 */
	public static Date setTime(Date target, Integer hour, Integer min, Integer sec, Integer millisec) {
		target = addHours(target, (hour - getHour(target)));
		target = addMinutes(target, (min - getMin(target)));
		target = addSeconds(target, (sec - getSec(target)));
		target = addMillisec(target, (millisec - getMillisec(target)));
		return target;
	}
	
	/**
	 * Sets the minimum time for the given date day.
	 * @param target The date representing the date/time to be changed.
	 * @return The same input date (for chain purposes).
	 */
	public static Date setTimeMin(Date target) {
		return setTime(target, 0, 0, 0, 0);
	}
	/**
	 * Sets the maximum time for the given date day.
	 * @param target The date representing the date/time to be changed.
	 * @return The same input date (for chain purposes).
	 */
	public static Date setTimeMax(Date target) {
		return setTime(target, 23, 59, 59, 999);
	}
	
	/**
	 * Creates a new date with the time given for an input base date.
	 * @param base The date representing the date/time of the new date to be created.
	 * @param hour The hour to set.
	 * @param min The minute to set.
	 * @param sec The second to set.
	 * @param millisec The milliseconds to set.
	 * @return A new date object with the changes.
	 */
	public static Date getTime(Date base, Integer hour, Integer min, Integer sec, Integer millisec) {
		Date ret = new Date(base.getTime());
		return setTime(ret, hour, min, sec, millisec);
	}
	
	/**
	 * Creates a new date with the minimum time for the given input day.
	 * @param base The date representing the date/time of the new date to be created.
	 * @return A new date object with the changes.
	 */
	public static Date getTimeMin(Date base) {
		Date ret = new Date(base.getTime());
		return setTimeMin(ret);
	}
	/**
	 * Creates a new date with the maximum time for the given input day.
	 * @param base The date representing the date/time of the new date to be created.
	 * @return A new date object with the changes.
	 */
	public static Date getTimeMax(Date base) {
		Date ret = new Date(base.getTime());
		return setTimeMax(ret);
	}
	
	/**
	 * Gets the year of the given date.
	 * @param date The date to extract the year.
	 * @return The full year (4-digit).
	 */
	public static Integer getYear(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("yyyy").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Gets the month number of the given date.
	 * @param date The date to extract the month.
	 * @return The month number (1 = January, 2 = February, ...).
	 */
	public static Integer getMonth(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("MM").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Gets the month day of the given date.
	 * @param date The date to extract the day.
	 * @return The month day (0~31).
	 */
	public static Integer getDay(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("dd").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Gets the hours of the day from the given date.
	 * @param date The date to extract the hour.
	 * @return The hour (0~23).
	 */
	public static Integer getHour(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("HH").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Gets the minutes of the hour from the given date.
	 * @param date The date to extract the minutes.
	 * @return The minute (0~59).
	 */
	public static Integer getMin(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("mm").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Gets the seconds of the minute from the given date.
	 * @param date The date to extract the seconds.
	 * @return The seconds (0~59).
	 */
	public static Integer getSec(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("ss").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Gets the milliseconds of the second from the given date.
	 * @param date The date to extract the milliseconds.
	 * @return The milliseconds (0~999).
	 */
	public static Integer getMillisec(Date date) {
		Integer ret = null;
		try {
			ret = Integer.valueOf(DateTimeFormat.getFormat("SSS").format(date));
		} catch (Exception e) {}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the milliseconds.
	 * @param date The source date to add.
	 * @param milliseconds The milliseconds to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addMillisec(Date date, int milliseconds) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + milliseconds);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object removing the TimeZone offset.
	 * @param source the source Date
	 * @return a new Date without the TimeZone offset.
	 */
	@SuppressWarnings("deprecation")
	public static Date removeTimeZoneOffset(Date source) {
		Date ret = null;
		if (source != null) {
			ret = new Date(source.getTime() + source.getTimezoneOffset() * SimpleConstants.ONE_MINUTE_MSEC);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the years.
	 * @param date The source date to add.
	 * @param years The years to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addYears(Date date, int years) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + SimpleConstants.ONE_YEAR_MSEC * years);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the months.
	 * @param date The source date to add.
	 * @param months The months to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addMonths(Date date, int months) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + SimpleConstants.ONE_MONTH_MSEC * months);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the days.
	 * @param date The source date to add.
	 * @param days The days to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addDays(Date date, int days) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + SimpleConstants.ONE_DAY_MSEC * days);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the hours.
	 * @param date The source date to add.
	 * @param hours The hours to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addHours(Date date, int hours) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + SimpleConstants.ONE_HOUR_MSEC * hours);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the minutes.
	 * @param date The source date to add.
	 * @param minutes The minutes to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addMinutes(Date date, int minutes) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + SimpleConstants.ONE_MINUTE_MSEC * minutes);
		}
		return ret;
	}
	
	/**
	 * Creates a new Date object adding the seconds.
	 * @param date The source date to add.
	 * @param seconds The seconds to add (can be negative).
	 * @return a new Date.
	 */
	public static Date addSeconds(Date date, int seconds) {
		Date ret = null;
		if (date != null) {
			ret = new Date(date.getTime() + SimpleConstants.ONE_SECOND_MSEC * seconds);
		}
		return ret;
	}

}
