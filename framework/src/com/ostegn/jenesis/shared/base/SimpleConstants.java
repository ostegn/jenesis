package com.ostegn.jenesis.shared.base;

/**
 * 
 * Simple useful constants class, the class and variable names are self explanatory.
 * 
 * @author Thiago Ricciardi
 *
 */
public class SimpleConstants {
	
	//Date and Time constants
	
	public static final long ONE_SECOND_MSEC = 1000;
	
	public static final long ONE_MINUTE_SEC = 60;
	public static final long ONE_MINUTE_MSEC = ONE_MINUTE_SEC * ONE_SECOND_MSEC;
	
	public static final long ONE_HOUR_MIN = 60;
	public static final long ONE_HOUR_SEC = ONE_HOUR_MIN * ONE_MINUTE_SEC;
	public static final long ONE_HOUR_MSEC = ONE_HOUR_MIN * ONE_MINUTE_MSEC;
	
	public static final long ONE_DAY_HR = 24;
	public static final long ONE_DAY_MIN = ONE_DAY_HR * ONE_HOUR_MIN;
	public static final long ONE_DAY_SEC = ONE_DAY_HR * ONE_HOUR_SEC;
	public static final long ONE_DAY_MSEC = ONE_DAY_HR * ONE_HOUR_MSEC;
	
	public static final long ONE_WEEK_DAY = 7;
	public static final long ONE_WEEK_HR = ONE_WEEK_DAY * ONE_DAY_HR;
	public static final long ONE_WEEK_MIN = ONE_WEEK_DAY * ONE_DAY_MIN;
	public static final long ONE_WEEK_SEC = ONE_WEEK_DAY * ONE_DAY_SEC;
	public static final long ONE_WEEK_MSEC = ONE_WEEK_DAY * ONE_DAY_MSEC;
	
	public static final long ONE_MONTH_WK = 4;
	public static final long ONE_MONTH_DAY = 30;
	public static final long ONE_MONTH_HR = ONE_MONTH_DAY * ONE_DAY_HR;
	public static final long ONE_MONTH_MIN = ONE_MONTH_DAY * ONE_DAY_MIN;
	public static final long ONE_MONTH_SEC = ONE_MONTH_DAY * ONE_DAY_SEC;
	public static final long ONE_MONTH_MSEC = ONE_MONTH_DAY * ONE_DAY_MSEC;
	
	public static final long ONE_YEAR_MTH = 12;
	public static final long ONE_YEAR_WK = 52;
	public static final long ONE_YEAR_DAY = 365;
	public static final long ONE_YEAR_HR = ONE_YEAR_DAY * ONE_DAY_HR;
	public static final long ONE_YEAR_MIN = ONE_YEAR_DAY * ONE_DAY_MIN;
	public static final long ONE_YEAR_SEC = ONE_YEAR_DAY * ONE_DAY_SEC;
	public static final long ONE_YEAR_MSEC = ONE_YEAR_DAY * ONE_DAY_MSEC;
	
	
	//SI and other standards
	
	public static final long SI_KILO = 1000;
	public static final long SI_MEGA = SI_KILO * SI_KILO;
	public static final long SI_GIGA = SI_KILO * SI_MEGA;
	public static final long SI_TERA = SI_KILO * SI_GIGA;
	public static final long SI_PETA = SI_KILO * SI_TERA;
	
	public static final double SI_MILI = 0.001;
	public static final double SI_MICRO = SI_MILI * SI_MILI;
	public static final double SI_NANO = SI_MILI * SI_MICRO;
	public static final double SI_PICO = SI_MILI * SI_NANO;
	
	public static final long BIN_KILO = 1024;
	public static final long BIN_MEGA = BIN_KILO * BIN_KILO;
	public static final long BIN_GIGA = BIN_KILO * BIN_MEGA;
	public static final long BIN_TERA = BIN_KILO * BIN_GIGA;
	public static final long BIN_PETA = BIN_KILO * BIN_TERA;
	
	
	//Byte and bit constants
	
	public static final long ONE_BYTE_BIT = 8;
	
	public static final long ONE_KIBIBYTE_BYTE = BIN_KILO;
	
	public static final long ONE_MEBIBYTE_KIBIBYTE = BIN_KILO;
	public static final long ONE_MEBIBYTE_BYTE = ONE_MEBIBYTE_KIBIBYTE * ONE_KIBIBYTE_BYTE;
	
	public static final long ONE_GIBIBYTE_MEBIBYTE = BIN_KILO;
	public static final long ONE_GIBIBYTE_KIBIBYTE = ONE_GIBIBYTE_MEBIBYTE * ONE_MEBIBYTE_KIBIBYTE;
	public static final long ONE_GIBIBYTE_BYTE = ONE_GIBIBYTE_MEBIBYTE * ONE_MEBIBYTE_BYTE;
	
	public static final long ONE_TEBIBYTE_GIBIBYTE = BIN_KILO;
	public static final long ONE_TEBIBYTE_MEBIBYTE = ONE_TEBIBYTE_GIBIBYTE * ONE_GIBIBYTE_MEBIBYTE;
	public static final long ONE_TEBIBYTE_KIBIBYTE = ONE_TEBIBYTE_GIBIBYTE * ONE_GIBIBYTE_KIBIBYTE;
	public static final long ONE_TEBIBYTE_BYTE = ONE_TEBIBYTE_GIBIBYTE * ONE_GIBIBYTE_BYTE;
	
	public static final long ONE_PEBIBYTE_TEBIBYTE = BIN_KILO;
	public static final long ONE_PEBIBYTE_GIBIBYTE = ONE_PEBIBYTE_TEBIBYTE * ONE_TEBIBYTE_GIBIBYTE;
	public static final long ONE_PEBIBYTE_MEBIBYTE = ONE_PEBIBYTE_TEBIBYTE * ONE_TEBIBYTE_MEBIBYTE;
	public static final long ONE_PEBIBYTE_KIBIBYTE = ONE_PEBIBYTE_TEBIBYTE * ONE_TEBIBYTE_KIBIBYTE;
	public static final long ONE_PEBIBYTE_BYTE = ONE_PEBIBYTE_TEBIBYTE * ONE_TEBIBYTE_BYTE;

}
