package com.ostegn.jenesis.shared.base;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.i18n.shared.CustomDateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormatInfo;
import com.google.gwt.i18n.shared.DefaultDateTimeFormatInfo;

public class DateTimeFormat extends com.google.gwt.i18n.shared.DateTimeFormat {
	
	protected DateTimeFormat(String pattern) {
		super(pattern, new DefaultDateTimeFormatInfo());
	}

	protected DateTimeFormat(String pattern, DateTimeFormatInfo dtfi) {
		super(pattern, dtfi);
	}
	
	private static final Map<String, DateTimeFormat> cache;
	
	static {
		cache = new HashMap<String, DateTimeFormat>();
	}
	
	/**
	 * Get a DateTimeFormat instance for a predefined format.
	 * 
	 * <p>See {@link CustomDateTimeFormat} if you need a localized format that is
	 * not supported here.
	 * 
	 * @param predef PredefinedFormat describing desired format
	 * @return a DateTimeFormat instance for the specified format
	 */
	public static com.google.gwt.i18n.shared.DateTimeFormat getFormat(PredefinedFormat predef) {
		DateTimeFormatInfo dtfi = getDefaultDateTimeFormatInfo();
		String pattern;
		switch (predef) {
			case RFC_2822:
				pattern = RFC2822_PATTERN;
				break;
			case ISO_8601:
				pattern = ISO8601_PATTERN;
				break;
			case DATE_FULL:
				pattern = dtfi.dateFormatFull();
				break;
			case DATE_LONG:
				pattern = dtfi.dateFormatLong();
				break;
			case DATE_MEDIUM:
				pattern = dtfi.dateFormatMedium();
				break;
			case DATE_SHORT:
				pattern = dtfi.dateFormatShort();
				break;
			case DATE_TIME_FULL:
				pattern = dtfi.dateTimeFull(dtfi.timeFormatFull(), dtfi.dateFormatFull());
				break;
			case DATE_TIME_LONG:
				pattern = dtfi.dateTimeLong(dtfi.timeFormatLong(), dtfi.dateFormatLong());
				break;
			case DATE_TIME_MEDIUM:
				pattern = dtfi.dateTimeMedium(dtfi.timeFormatMedium(), dtfi.dateFormatMedium());
				break;
			case DATE_TIME_SHORT:
				pattern = dtfi.dateTimeShort(dtfi.timeFormatShort(), dtfi.dateFormatShort());
				break;
			case DAY:
				pattern = dtfi.formatDay();
				break;
			case HOUR24_MINUTE:
				pattern = dtfi.formatHour24Minute();
				break;
			case HOUR24_MINUTE_SECOND:
				pattern = dtfi.formatHour24MinuteSecond();
				break;
			case HOUR_MINUTE:
				pattern = dtfi.formatHour12Minute();
				break;
			case HOUR_MINUTE_SECOND:
				pattern = dtfi.formatHour12MinuteSecond();
				break;
			case MINUTE_SECOND:
				pattern = dtfi.formatMinuteSecond();
				break;
			case MONTH:
				pattern = dtfi.formatMonthFull();
				break;
			case MONTH_ABBR:
				pattern = dtfi.formatMonthAbbrev();
				break;
			case MONTH_ABBR_DAY:
				pattern = dtfi.formatMonthAbbrevDay();
				break;
			case MONTH_DAY:
				pattern = dtfi.formatMonthFullDay();
				break;
			case MONTH_NUM_DAY:
				pattern = dtfi.formatMonthNumDay();
				break;
			case MONTH_WEEKDAY_DAY:
				pattern = dtfi.formatMonthFullWeekdayDay();
				break;
			case TIME_FULL:
				pattern = dtfi.timeFormatFull();
				break;
			case TIME_LONG:
				pattern = dtfi.timeFormatLong();
				break;
			case TIME_MEDIUM:
				pattern = dtfi.timeFormatMedium();
				break;
			case TIME_SHORT:
				pattern = dtfi.timeFormatShort();
				break;
			case YEAR:
				pattern = dtfi.formatYear();
				break;
			case YEAR_MONTH:
				pattern = dtfi.formatYearMonthFull();
				break;
			case YEAR_MONTH_ABBR:
				pattern = dtfi.formatYearMonthAbbrev();
				break;
			case YEAR_MONTH_ABBR_DAY:
				pattern = dtfi.formatYearMonthAbbrevDay();
				break;
			case YEAR_MONTH_DAY:
				pattern = dtfi.formatYearMonthFullDay();
				break;
			case YEAR_MONTH_NUM:
				pattern = dtfi.formatYearMonthNum();
				break;
			case YEAR_MONTH_NUM_DAY:
				pattern = dtfi.formatYearMonthNumDay();
				break;
			case YEAR_MONTH_WEEKDAY_DAY:
				pattern = dtfi.formatYearMonthWeekdayDay();
				break;
			case YEAR_QUARTER:
				pattern = dtfi.formatYearQuarterFull();
				break;
			case YEAR_QUARTER_ABBR:
				pattern = dtfi.formatYearQuarterShort();
				break;
			default:
				throw new IllegalArgumentException("Unexpected predefined format " + predef);
		}
		return getFormat(pattern, dtfi);
	}
	
	/**
	 * Returns a DateTimeFormat object using the specified pattern. If you need to
	 * format or parse repeatedly using the same pattern, it is highly recommended
	 * that you cache the returned <code>DateTimeFormat</code> object and reuse it
	 * rather than calling this method repeatedly.
	 * 
	 * <p>Note that the pattern supplied is used as-is -- for example, if you
	 * supply "MM/dd/yyyy" as the pattern, that is the order you will get the
	 * fields, even in locales where the order is different.  It is recommended to
	 * use {@link #getFormat(PredefinedFormat)} instead -- if you use this method,
	 * you are taking responsibility for localizing the patterns yourself.
	 * 
	 * @param pattern string to specify how the date should be formatted
	 * 
	 * @return a <code>DateTimeFormat</code> object that can be used for format or
	 *         parse date/time values matching the specified pattern
	 * 
	 * @throws IllegalArgumentException if the specified pattern could not be
	 *           parsed
	 */
	public static com.google.gwt.i18n.shared.DateTimeFormat getFormat(String pattern) {
		return getFormat(pattern, getDefaultDateTimeFormatInfo());
	}
	
	/**
	 * Internal factory method that provides caching.
	 * 
	 * @param pattern
	 * @param dtfi
	 * @return DateTimeFormat instance
	 */
	protected static com.google.gwt.i18n.shared.DateTimeFormat getFormat(String pattern, DateTimeFormatInfo dtfi) {
		DateTimeFormatInfo defaultDtfi = getDefaultDateTimeFormatInfo();
		DateTimeFormat dtf = null;
		if (dtfi == defaultDtfi) {
			dtf = cache.get(pattern);
		}
		if (dtf == null) {
			dtf = new DateTimeFormat(pattern, dtfi);
			if (dtfi == defaultDtfi) {
				cache.put(pattern, dtf);
			}
		}
		return dtf;
	}
	
	private static DateTimeFormatInfo getDefaultDateTimeFormatInfo() {
		return new DefaultDateTimeFormatInfo();
	}

}
