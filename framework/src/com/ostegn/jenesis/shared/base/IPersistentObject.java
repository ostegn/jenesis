package com.ostegn.jenesis.shared.base;

import java.io.Serializable;

/**
 *
 * Defines the persistent object.
 *
 * @author Thiago Ricciardi
 *
 */
public interface IPersistentObject extends Serializable {

	/** Anything needed to do before getting/loading the data */
	void doBeforeLoad();

	/** Anything needed to do before saving the data */
	void doBeforeSave();

	/** Anything needed to do after getting/loading the data */
	void doAfterLoad();

	/** Anything needed to do after saving the data */
	void doAfterSave();

	/** The Primary key, either single or multiple (in second case it should be an internal newly created class) */
	Serializable getPrimaryKey();

}
