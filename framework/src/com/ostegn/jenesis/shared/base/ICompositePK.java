package com.ostegn.jenesis.shared.base;

import java.io.Serializable;

/**
 * Defines a Composite Primary Key
 * @author Thiago Ricciardi
 *
 */
public interface ICompositePK extends Serializable {

}
