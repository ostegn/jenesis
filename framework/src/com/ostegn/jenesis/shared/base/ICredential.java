package com.ostegn.jenesis.shared.base;

import java.util.Collection;

/**
 * Defines the Credential
 * @author Thiago Ricciardi
 *
 */
public interface ICredential extends IPersistentObject {
	
	/** Override this function to execute something when the user signs in */
	public void onSignIn();
	/** Override this function to execute something when the user signs out */
	public void onSignOut();
	
	/**
	 * Get the Jenesis Authorities (similar to Spring's UserDetails Authorities, just in a different way to be compatible with GWT)
	 * @return a string collection of Authorities
	 */
	//Changed from UserDetails (for compatibility with GWT)
	public Collection<String> getJAuthorities();
	//Imported from UserDetails
	/**
	 * The user password (usually encrypted)
	 * @return The (usually encrypted) user's password string
	 */
	public String getPassword();
	/**
	 * The user-name for login purposes
	 * @return the user-name string
	 */
	public String getUsername();
	/**
	 * If the account is not expired
	 * @return true if the account is not expired, false otherwise
	 */
	public boolean isAccountNonExpired();
	/**
	 * If the account is not locked
	 * @return true if the account is not locked, false otherwise
	 */
	public boolean isAccountNonLocked();
	/**
	 * If the credentials have not expired
	 * @return true if the credentials have not expired, false otherwise
	 */
	public boolean isCredentialsNonExpired();
	/**
	 * If the user is enabled
	 * @return true if the user is enabled, false otherwise
	 */
	public boolean isEnabled();

}
