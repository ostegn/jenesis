package com.ostegn.jenesis.shared.base;



/**
 *
 * This class implements the persistent object interface.<br>
 * All persistent objects should be a superclass of this class, if not they must implement IPersistentObject interface.
 *
 * @author Thiago Ricciardi
 *
 * @see IPersistentObject
 */
@SuppressWarnings("serial")
public abstract class PersistentObject implements IPersistentObject {

	/** This method executes immediately before any read only transaction. */
	@Override
	public void doBeforeLoad() {}

	/** This method executes immediately before any read-write transaction. */
	@Override
	public void doBeforeSave() {}

	/** This method executes immediately after any read only transaction. */
	@Override
	public void doAfterLoad() {}

	/** This method executes immediately after any read-write transaction. */
	@Override
	public void doAfterSave() {}

	@Override
	public boolean equals(Object obj) {
		boolean ret = false;
		if (obj != null) {
			if (obj instanceof IPersistentObject) {
				if (((IPersistentObject) obj).getPrimaryKey() != null && this.getPrimaryKey() != null) {
					ret = ((IPersistentObject) obj).getPrimaryKey().equals(this.getPrimaryKey());
				}
			}
		}
		return ret;
	}

	@Override
	public int hashCode() {
		if (getPrimaryKey() != null) {
			return getPrimaryKey().hashCode() + getClass().hashCode();
		}
		else {
			return getClass().hashCode();
		}
	}

}
