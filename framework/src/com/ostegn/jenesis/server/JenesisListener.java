package com.ostegn.jenesis.server;

import org.springframework.web.context.ContextLoaderListener;

/**
 * Implements the default Jenesis listener extending a spring framework listener
 * @author Thiago Ricciardi
 *
 */
public class JenesisListener extends ContextLoaderListener {

}
