package com.ostegn.jenesis.server;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ostegn.jenesis.shared.base.SimpleConstants;
import com.ostegn.jenesis.shared.base.SimpleFunctions;


/**
 * 
 * This filter class is configured in <b>web.xml</b> and controls the cache attributes.
 * 
 * @author Thiago Ricciardi
 *
 */
public class CacheControlFilter implements Filter {
	
	private HashMap<String, Long> cacheHash = new HashMap<String, Long>();

	@Override
	public void destroy() {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
		if (request instanceof HttpServletRequest) {
			String path = ((HttpServletRequest) request).getRequestURI();
			for (String regex : cacheHash.keySet()) {
				if (path.matches("(?i)" + regex)) {
					addCacheControl((HttpServletResponse) response, cacheHash.get(regex));
				}
			}
		}
	}

	
	@Override
	public void init(FilterConfig config) throws ServletException {
		for (Enumeration<String> e = config.getInitParameterNames(); e.hasMoreElements();) {
			String param = e.nextElement();
			Long value = SimpleFunctions.getLongValue(config.getInitParameter(param));
			if (value != null) {
				cacheHash.put(param, value);
			}
		}
	}
	
	/**
	 * Apply the cache control adding the correct headers to the response
	 * @param response the servlet response to add the headers to
	 * @param sec the cache expiration time in seconds
	 */
	public static void addCacheControl(HttpServletResponse response, long sec) {
		if (!(sec < 0)) {
			response.setDateHeader("Expires", System.currentTimeMillis() + sec * SimpleConstants.ONE_SECOND_MSEC);
			response.setHeader("Cache-Control", "max-age=" + sec);
		}
	}

}
