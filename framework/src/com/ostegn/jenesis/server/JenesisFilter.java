package com.ostegn.jenesis.server;

import org.springframework.web.filter.DelegatingFilterProxy;

/**
 * Implements the default Jenesis filter extending a spring framework filter
 * @author Thiago Ricciardi
 *
 */
public class JenesisFilter extends DelegatingFilterProxy {
	
}
