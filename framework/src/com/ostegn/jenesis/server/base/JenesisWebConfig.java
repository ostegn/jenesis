package com.ostegn.jenesis.server.base;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ostegn.jenesis.server.security.JenesisPasswordEncoder;

/**
 * Provides a convenient base for configuring web<br>
 * In addition to {@link JenesisWebConfigBase} definitions,
 * sets {@link JenesisPasswordEncoder} as the password encoder.<br>
 * 
 * Allows further configuration by overriding methods
 * @author Thiago Ricciardi
 *
 */
@Configuration
public abstract class JenesisWebConfig extends JenesisWebConfigBase {
	
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		if (passwordEncoder == null) passwordEncoder = new JenesisPasswordEncoder();
		return passwordEncoder;
	}
	private PasswordEncoder passwordEncoder;
	
}
