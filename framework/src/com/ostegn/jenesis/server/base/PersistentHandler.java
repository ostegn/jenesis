package com.ostegn.jenesis.server.base;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ostegn.jenesis.shared.base.GeneratedValue;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesis.shared.base.SimpleFunctions;

/**
 *
 * This class is used as a main transactional class on this framework.<br>
 * Normally all transactions are made thru this class' processes.
 *
 * @author Thiago Ricciardi
 *
 * @see IPersistentObject
 * @see SessionFactory
 * @see EntityManager
 * @see MongoOperations
 *
 */
@Repository
@Transactional(readOnly=true)
public class PersistentHandler implements PersistentOperations {

	/** This class main logger */
	protected static transient Log logger = LogFactory.getLog(PersistentHandler.class);

	protected PersistentHandler() {}

	/** The MongoOperations object */
	@Autowired(required=false)
	protected MongoOperations mongo;

	/** The SessionFactory object */
	@Autowired(required=false)
	protected SessionFactory hibernateSessionFactory;

	@Override
	public Session getHibernateSession() {
		if (hibernateSessionFactory != null)
			return hibernateSessionFactory.getCurrentSession();
		else
			throw new IllegalStateException("Trying to persist a Hibernate class, but no Hibernate has been supplied.");
	}

	@Override
	public EntityManager getEntityManager() {
		return getHibernateSession();
	}

	@Override
	public MongoOperations getMongoOperations() {
		if (mongo != null)
			return mongo;
		else
			throw new IllegalStateException("Trying to persist a Mongo class, but no Mongo has been supplied.");
	}

	@Override
	public <T extends IPersistentObject> boolean isHibernate(Class<T> c) {
		return (c != null) ? c.isAnnotationPresent(Entity.class) :  false;
	}
	@Override
	public <T extends IPersistentObject> boolean isHibernate(T p) {
		return (p != null) ? isHibernate(p.getClass()) : false;
	}
	@Override
	public <T extends IPersistentObject> boolean isHibernate(List<T> l) {
		return (l != null) ? ( (l.size() > 0) ? isHibernate(l.get(0)) : false) : false;
	}

	@Override
	public <T extends IPersistentObject> boolean isMongo (Class<T> c) {
		return (c != null) ? c.isAnnotationPresent(Document.class) :  false;
	}
	@Override
	public <T extends IPersistentObject> boolean isMongo(T p) {
		return (p != null) ? isMongo(p.getClass()) : false;
	}
	@Override
	public <T extends IPersistentObject> boolean isMongo(List<T> l) {
		return (l != null) ? ( (l.size() > 0) ? isMongo(l.get(0)) : false) : false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> T get(T p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("Get persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		T ret = null;
		if (p.getPrimaryKey() != null) {
			if (isHibernate(p)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, get()", p));
				ret = (T) getHibernateSession().get(p.getClass(), p.getPrimaryKey());
			}
			else if (isMongo(p)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, findById()", p));
				ret = (T) getMongoOperations().findById(p.getPrimaryKey(), p.getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
			}
		}
		else if (logger.isInfoEnabled()) {
			logger.info(String.format("Trying to get a persistent object with null PK: %1$s", p));
		}
		if (ret != null) ret.doAfterLoad();
		return ret;
	}

	@Override
	@Transactional(readOnly=false)
	public <T extends IPersistentObject> T save(T p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("Save persistent object %1$s doBeforeSave()", p));
		p = doBeforeSave(p);
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, saveOrUpdate()", p));
			getHibernateSession().saveOrUpdate(p);
			p.doAfterSave();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, save()", p));
			getMongoOperations().save(p);
			p.doAfterSave();
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		return p;
	}

	@Override
	@Transactional(readOnly=false)
	public <T extends IPersistentObject> T del(T p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("Delete persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, delete()", p));
			getHibernateSession().delete(p);
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, remove()", p));
			getMongoOperations().remove(p);
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		return p;
	}

	@Override
	@Transactional(readOnly=false)
	public <T extends IPersistentObject> T insert(T p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("Insert persistent object %1$s doBeforeSave()", p));
		p = doBeforeSave(p);
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, save()", p));
			getHibernateSession().save(p);
			p.doAfterSave();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, insert()", p));
			getMongoOperations().insert(p);
			p.doAfterSave();
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		return p;
	}

	@Override
	@Transactional(readOnly=false)
	public <T extends IPersistentObject> T update(T p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("Update persistent object %1$s doBeforeSave()", p));
		p = doBeforeSave(p);
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, update()", p));
			getHibernateSession().update(p);
			p.doAfterSave();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, save()", p));
			getMongoOperations().save(p);
			p.doAfterSave();
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		return p;
	}

	@Override
	public <T extends IPersistentObject> List<T> getAll(Class<T> c) {
		List<T> ret = null;
		if (isHibernate(c)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("GetAll persistent objects, class %1$s is Hibernate", c));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<T> q = cb.createQuery(c);
			q.select(q.from(c));
			ret = createQuery(q).getResultList();
		}
		else if (isMongo(c)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("GetAll persistent objects, class %1$s is Mongo, findAll()", c));
			ret = getMongoOperations().findAll(c);
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist a class that is neither Hibernate nor Mongo: %1$s", c));
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getAll(T p) {
		return (List<T>) getAll(p.getClass());
	}

	@Override
	public Long countAll(Class<? extends IPersistentObject> c) {
		Long ret = null;
		if (isHibernate(c)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("countAll persistent objects, class %1$s is Hibernate", c));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<Long> q = cb.createQuery(Long.class);
			q.select(cb.count(q.from(c)));
			ret = createQuery(q).getSingleResult();
		}
		else if (isMongo(c)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("countAll persistent objects, class %1$s is Mongo", c));
			ret = getMongoOperations().count(new Query(), c);
			//if (ret == null) ret = 0L;
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist a class that is neither Hibernate nor Mongo: %1$s", c));
		}
		return ret;
	}

	@Override
	public Long countAll(IPersistentObject p) {
		return countAll(p.getClass());
	}

	@Override
	public <T extends IPersistentObject> List<T> getAllLatest(T p) throws IllegalArgumentException {
		return getLatest(p, 0, 0);
	}

	@Override
	public <T extends IPersistentObject> T getLatest(T p) throws IllegalArgumentException {
		List<T> latest = getLatest(p, 0, 1);
		if (latest.size() > 0) return latest.get(0);
		else return null;
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getLatest(T p, int firstRow, int maxRows) throws IllegalArgumentException {
		List<T> ret = null;
		Field versionField = getVersionField(p);
		if (versionField != null) {
			if (logger.isDebugEnabled()) logger.debug(String.format("getLatest persistent object %1$s doBeforeLoad()", p));
			p.doBeforeLoad();
			if (isHibernate(p)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s persistent object(s) $2$s from %3$s to %4$s - Hibernate", maxRows, p, firstRow, firstRow+maxRows));
				CriteriaBuilder cb = getCriteriaBuilder();
				CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(p.getClass());
				Root<T> r = (Root<T>) q.from(p.getClass());
				q.select(r);
				q.where(getNotNullPredicate(p, cb, r));
				q.orderBy(cb.desc(r.get(versionField.getName())));
				TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
				if (maxRows > 0) tq.setMaxResults(maxRows);
				ret = tq.getResultList();
			}
			else if (isMongo(p)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s persistent object(s) $2$s from %3$s to %4$s - Mongo", maxRows, p, firstRow, firstRow+maxRows));
				ret = (List<T>) getMongoOperations().find(new Query(addNotNullCriteria(p)).with(new Sort(Direction.DESC, versionField.getName())).skip(firstRow).limit(maxRows), p.getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
			}
		}
		else {
			throw new IllegalArgumentException(String.format("To use latest methods, the class %1$s must have a @Version field." , p.getClass()));
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	public <T extends IPersistentObject> List<T> getAllLatest(List<T> l) throws IllegalArgumentException {
		return getLatest(l, 0, 0);
	}
	@Override
	public <T extends IPersistentObject> T getLatest(List<T> l) throws IllegalArgumentException {
		List<T> latest = getLatest(l, 0, 1);
		if (latest.size() > 0) return latest.get(0);
		else return null;
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getLatest(List<T> l, int firstRow, int maxRows) throws IllegalArgumentException {
		List<T> ret = null;
		if ( (l != null) ? (l.size() > 0) : false) {
			Field versionField = getVersionField(l.get(0));
			if (versionField != null) {
				if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s list doBeforeLoad()", l));
				for (T p : l) if (p != null) p.doBeforeLoad();
				if (isHibernate(l)) {
					if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s list $2$s from %3$s to %4$s - Hibernate", maxRows, l, firstRow, firstRow+maxRows));
					CriteriaBuilder cb = getCriteriaBuilder();
					CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(l.get(0).getClass());
					Root<T> r = (Root<T>) q.from(l.get(0).getClass());
					List<Predicate> pl = new ArrayList<Predicate>();
					for (T p : l) {
						pl.add(getNotNullPredicate(p, cb, r));
					}
					q.select(r);
					q.where(cb.or(pl.toArray(new Predicate[l.size()])));
					q.orderBy(cb.desc(r.get(versionField.getName())));
					TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
					if (maxRows > 0) tq.setMaxResults(maxRows);
					ret = tq.getResultList();
				}
				else if (isMongo(l)) {
					if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s list $2$s from %3$s to %4$s - Mongo", maxRows, l, firstRow, firstRow+maxRows));
					Criteria c = null;
					for (T p : l) {
						if (c != null) c = c.orOperator(addNotNullCriteria(p));
						else c = addNotNullCriteria(p);
					}
					ret = (List<T>) getMongoOperations().find(new Query(c).with(new Sort(Direction.DESC, versionField.getName())).skip(firstRow).limit(maxRows), l.get(0).getClass());
				}
				else if (logger.isWarnEnabled()) {
					logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", l));
				}
			}
			else {
				throw new IllegalArgumentException(String.format("To use latest methods, the class %1$s must have a @Version field." , l.get(0).getClass()));
			}
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	public <T extends IPersistentObject> List<T> getAllFirst(T p) throws IllegalArgumentException {
		return getFirst(p, 0, 0);
	}

	@Override
	public <T extends IPersistentObject> T getFirst(T p) throws IllegalArgumentException {
		List<T> first = getFirst(p, 0, 1);
		if (first.size() > 0) return first.get(0);
		else return null;
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getFirst(T p, int firstRow, int maxRows) throws IllegalArgumentException {
		List<T> ret = null;
		Field versionField = getVersionField(p);
		if (versionField != null) {
			if (logger.isDebugEnabled()) logger.debug(String.format("getFirst persistent object %1$s doBeforeLoad()", p));
			p.doBeforeLoad();
			if (isHibernate(p)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s persistent object(s) $2$s from %3$s to %4$s - Hibernate", maxRows, p, firstRow, firstRow+maxRows));
				CriteriaBuilder cb = getCriteriaBuilder();
				CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(p.getClass());
				Root<T> r = (Root<T>) q.from(p.getClass());
				q.select(r);
				q.where(getNotNullPredicate(p, cb, r));
				q.orderBy(cb.asc(r.get(versionField.getName())));
				TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
				if (maxRows > 0) tq.setMaxResults(maxRows);
				ret = tq.getResultList();
			}
			else if (isMongo(p)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s persistent object(s) $2$s from %3$s to %4$s - Mongo", maxRows, p, firstRow, firstRow+maxRows));
				ret = (List<T>) getMongoOperations().find(new Query(addNotNullCriteria(p)).with(new Sort(Direction.ASC, versionField.getName())).skip(firstRow).limit(maxRows), p.getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
			}
		}
		else {
			throw new IllegalArgumentException(String.format("To use first methods, the class %1$s must have a @Version field." , p.getClass()));
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	public <T extends IPersistentObject> List<T> getAllFirst(List<T> l) throws IllegalArgumentException {
		return getFirst(l, 0, 0);
	}
	@Override
	public <T extends IPersistentObject> T getFirst(List<T> l) throws IllegalArgumentException {
		List<T> latest = getFirst(l, 0, 1);
		if (latest.size() > 0) return latest.get(0);
		else return null;
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getFirst(List<T> l, int firstRow, int maxRows) throws IllegalArgumentException {
		List<T> ret = null;
		if ( (l != null) ? (l.size() > 0) : false) {
			Field versionField = getVersionField(l.get(0));
			if (versionField != null) {
				if (logger.isDebugEnabled()) logger.debug(String.format("getFirst %1$s list doBeforeLoad()", l));
				for (T p : l) if (p != null) p.doBeforeLoad();
				if (isHibernate(l)) {
					if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s list $2$s from %3$s to %4$s - Hibernate", maxRows, l, firstRow, firstRow+maxRows));
					CriteriaBuilder cb = getCriteriaBuilder();
					CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(l.get(0).getClass());
					Root<T> r = (Root<T>) q.from(l.get(0).getClass());
					List<Predicate> pl = new ArrayList<Predicate>();
					for (T p : l) {
						pl.add(getNotNullPredicate(p, cb, r));
					}
					q.select(r);
					q.where(cb.or(pl.toArray(new Predicate[pl.size()])));
					q.orderBy(cb.asc(r.get(versionField.getName())));
					TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
					if (maxRows > 0) tq.setMaxResults(maxRows);
					ret = tq.getResultList();
				}
				else if (isMongo(l)) {
					if (logger.isDebugEnabled()) logger.debug(String.format("getLatest %1$s list $2$s from %3$s to %4$s - Mongo", maxRows, l, firstRow, firstRow+maxRows));
					Criteria c = null;
					for (T p : l) {
						if (c != null) c = c.orOperator(addNotNullCriteria(p));
						else c = addNotNullCriteria(p);
					}
					ret = (List<T>) getMongoOperations().find(new Query(c).with(new Sort(Direction.ASC, versionField.getName())).skip(firstRow).limit(maxRows), l.get(0).getClass());
				}
				else if (logger.isWarnEnabled()) {
					logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", l));
				}
			}
			else {
				throw new IllegalArgumentException(String.format("To use first methods, the class %1$s must have a @Version field." , l.get(0).getClass()));
			}
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getList(T p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("getList persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		List<T> ret = null;
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, findByExample()", p));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(p.getClass());
			Root<T> r = (Root<T>) q.from(p.getClass());
			q.select(r);
			q.where(getNotNullPredicate(p, cb, r));
			ret = createQuery(q).getResultList();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, find() with createExampleQuery", p));
			Query q = createExampleQuery(p);
			if (q != null) {
				ret = (List<T>) getMongoOperations().find(q, p.getClass());
			}
			if (ret == null) ret = new ArrayList<T>();
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getList(T p, int firstRow, int maxRows) {
		if (logger.isDebugEnabled()) logger.debug(String.format("getList persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		List<T> ret = null;
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, findByExample(), first: %2$s, max: %3$s", p, firstRow, maxRows));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(p.getClass());
			Root<T> r = (Root<T>) q.from(p.getClass());
			q.select(r);
			q.where(getNotNullPredicate(p, cb, r));
			TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
			if (maxRows > 0) tq.setMaxResults(maxRows);
			ret = tq.getResultList();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, find() with createExampleQuery, skip: %2$s, limit: %3$s", p, firstRow, maxRows));
			Query q = createExampleQuery(p).skip(firstRow).limit(maxRows);
			if (q != null) {
				ret = (List<T>) getMongoOperations().find(q, p.getClass());
			}
			if (ret == null) ret = new ArrayList<T>();
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	public <T extends IPersistentObject> List<T> getList(List<T> l) {
		return getList(l, 0, 0);
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getList(List<T> l, int firstRow, int maxRows) {
		List<T> ret = null;
		if ( (l != null) ? (l.size() > 0) : false) {
			if (logger.isDebugEnabled()) logger.debug(String.format("getList %1$s list doBeforeLoad()", l));
			for (T p : l) if (p != null) p.doBeforeLoad();
			if (isHibernate(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent list %1$s is Hibernate, findByCriteria() example restrictions, first: %2$s, max: %3$s", l, firstRow, maxRows));
				CriteriaBuilder cb = getCriteriaBuilder();
				CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(l.get(0).getClass());
				Root<T> r = (Root<T>) q.from(l.get(0).getClass());
				List<Predicate> pl = new ArrayList<Predicate>();
				for (T p : l) {
					pl.add(getNotNullPredicate(p, cb, r));
				}
				q.select(r);
				q.where(cb.or(pl.toArray(new Predicate[pl.size()])));
				ret = createQuery(q).setFirstResult(firstRow).setMaxResults(maxRows).getResultList();
			}
			else if (isMongo(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent list %1$s is Mongo, find() example criteria, first: %2$s, max: %3$s", l, firstRow, maxRows));
				Criteria c = null;
				for (IPersistentObject p : l) {
					if (c != null) c = c.orOperator(addNotNullCriteria(p));
					else c = addNotNullCriteria(p);
				}
				ret = (List<T>) getMongoOperations().find(new Query(c).skip(firstRow).limit(maxRows), l.get(0).getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", l));
			}
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long countList(IPersistentObject p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("countList persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		Long ret = null;
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, rowCount()", p));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<Long> q = cb.createQuery(Long.class);
			Root<IPersistentObject> r = (Root<IPersistentObject>) q.from(p.getClass());
			q.select(cb.count(r));
			q.where(getNotNullPredicate(p, cb, r));
			ret = createQuery(q).getSingleResult();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, count()", p));
			Query q = createExampleQuery(p);
			if (q != null) {
				ret = getMongoOperations().count(q, p.getClass());
			}
			if (ret == null) ret = 0L;
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long countList(List<IPersistentObject> l) {
		Long ret = null;
		if ( (l != null) ? (l.size() > 0) : false) {
			if (logger.isDebugEnabled()) logger.debug(String.format("countList %1$s list doBeforeLoad()", l));
			for (IPersistentObject p : l) if (p != null) p.doBeforeLoad();
			if (isHibernate(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent %1$s list is Hibernate, rowCount()", l));
				CriteriaBuilder cb = getCriteriaBuilder();
				CriteriaQuery<Long> q = cb.createQuery(Long.class);
				Root<IPersistentObject> r = (Root<IPersistentObject>) q.from(l.get(0).getClass());
				List<Predicate> pl = new ArrayList<Predicate>();
				for (IPersistentObject p : l) {
					pl.add(getNotNullPredicate(p, cb, r));
				}
				q.select(cb.count(r));
				q.where(cb.or(pl.toArray(new Predicate[pl.size()])));
				ret = createQuery(q).getSingleResult();
			}
			else if (isMongo(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent %1$s list is Mongo, count()", l));
				Criteria c = null;
				for (IPersistentObject p : l) {
					if (c != null) c = c.orOperator(addNotNullCriteria(p));
					else c = addNotNullCriteria(p);
				}
				ret = getMongoOperations().count(new Query(c), l.get(0).getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", l));
			}
		}
		return ret;
	}

	@Override
	public <T extends IPersistentObject> List<T> getLike(T p) {
		return getLike(p, 0, 0);
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getLike(T p, int firstRow, int maxRows) {
		List<T> ret = null;
		if (logger.isDebugEnabled()) logger.debug(String.format("getLike persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, findByCriteria() example restrictions, first: %2$s, max: %3$s", p, firstRow, maxRows));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(p.getClass());
			Root<T> r = (Root<T>) q.from(p.getClass());
			q.select(r);
			q.where(getNotNullPredicate(p, cb, r, true));
			TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
			if (maxRows > 0) tq.setMaxResults(maxRows);
			ret = tq.getResultList();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, find() example criteria, first: %2$s, max: %3$s", p, firstRow, maxRows));
			ret = (List<T>) getMongoOperations().find(new Query(addNotNullCriteria(p, true)).skip(firstRow).limit(maxRows), p.getClass());
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	// TODO Use only one function for getLike and getList, something like private getListLike
	@Override
	public <T extends IPersistentObject> List<T> getLike(List<T> l) {
		return getLike(l, 0, 0);
	}
	@Override
	@SuppressWarnings("unchecked")
	public <T extends IPersistentObject> List<T> getLike(List<T> l, int firstRow, int maxRows) {
		List<T> ret = null;
		if ( (l != null) ? (l.size() > 0) : false) {
			if (logger.isDebugEnabled()) logger.debug(String.format("getLike %1$s list doBeforeLoad()", l));
			for (T p : l) if (p != null) p.doBeforeLoad();
			if (isHibernate(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent list %1$s is Hibernate, findByCriteria() example restrictions, first: %2$s, max: %3$s", l, firstRow, maxRows));
				CriteriaBuilder cb = getCriteriaBuilder();
				CriteriaQuery<T> q = (CriteriaQuery<T>) cb.createQuery(l.get(0).getClass());
				Root<T> r = (Root<T>) q.from(l.get(0).getClass());
				List<Predicate> pl = new ArrayList<Predicate>();
				for (T p : l) {
					pl.add(getNotNullPredicate(p, cb, r, true));
				}
				q.select(r);
				q.where(cb.or(pl.toArray(new Predicate[pl.size()])));
				TypedQuery<T> tq = createQuery(q).setFirstResult(firstRow);
				if (maxRows > 0) tq.setMaxResults(maxRows);
				ret = tq.getResultList();
			}
			else if (isMongo(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent list %1$s is Mongo, find() example criteria, first: %2$s, max: %3$s", l, firstRow, maxRows));
				Criteria c = null;
				for (T p : l) {
					if (c != null) c = c.orOperator(addNotNullCriteria(p, true));
					else c = addNotNullCriteria(p, true);
				}
				ret = (List<T>) getMongoOperations().find(new Query(c).skip(firstRow).limit(maxRows), l.get(0).getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", l));
			}
		}
		if (ret != null) for (T po : ret) po.doAfterLoad();
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long countLike(IPersistentObject p) {
		if (logger.isDebugEnabled()) logger.debug(String.format("countLike persistent object %1$s doBeforeLoad()", p));
		p.doBeforeLoad();
		Long ret = null;
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Hibernate, rowCount()", p));
			CriteriaBuilder cb = getCriteriaBuilder();
			CriteriaQuery<Long> q = cb.createQuery(Long.class);
			Root<IPersistentObject> r = (Root<IPersistentObject>) q.from(p.getClass());
			q.select(cb.count(r));
			q.where(getNotNullPredicate(p, cb, r, true));
			ret = createQuery(q).getSingleResult();
		}
		else if (isMongo(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Persistent object %1$s is Mongo, count()", p));
			Query q = new Query(addNotNullCriteria(p, true));
			if (q != null) {
				ret = getMongoOperations().count(q, p.getClass());
			}
			//if (ret == null) ret = 0L;
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", p));
		}
		return ret;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long countLike(List<IPersistentObject> l) {
		Long ret = null;
		if ( (l != null) ? (l.size() > 0) : false) {
			if (logger.isDebugEnabled()) logger.debug(String.format("countLike persistent list %1$s doBeforeLoad()", l));
			for (IPersistentObject p : l) if (p != null) p.doBeforeLoad();
			if (isHibernate(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent list %1$s is Hibernate, rowCount()", l));
				CriteriaBuilder cb = getCriteriaBuilder();
				CriteriaQuery<Long> q = cb.createQuery(Long.class);
				Root<IPersistentObject> r = (Root<IPersistentObject>) q.from(l.get(0).getClass());
				List<Predicate> pl = new ArrayList<Predicate>();
				for (IPersistentObject p : l) {
					pl.add(getNotNullPredicate(p, cb, r, true));
				}
				q.select(cb.count(r));
				q.where(cb.or(pl.toArray(new Predicate[pl.size()])));
				ret = createQuery(q).getSingleResult();
			}
			else if (isMongo(l)) {
				if (logger.isDebugEnabled()) logger.debug(String.format("Persistent list %1$s is Mongo, count()", l));
				Criteria c = null;
				for (IPersistentObject p : l) {
					if (c != null) c = c.orOperator(addNotNullCriteria(p, true));
					else c = addNotNullCriteria(p, true);
				}
				ret = getMongoOperations().count(new Query(c), l.get(0).getClass());
			}
			else if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to persist an object that is neither Hibernate nor Mongo: %1$s", l));
			}
		}
		return ret;
	}

	@Override
	@Transactional(readOnly=false)
	public <T extends IPersistentObject> List<T> saveList(List<T> l) {
		List<T> ret = new ArrayList<T>();
		for (T p : l) {
			ret.add(save(p));
		}
		return ret;
	}

	@Override
	@Transactional(readOnly=false)
	public <T extends IPersistentObject> List<T> delList(List<T> l) {
		List<T> ret = new ArrayList<T>();
		for (T p : l) {
			ret.add(del(p));
		}
		return ret;
	}

	@Override
	public void evict(IPersistentObject p) {
		if (isHibernate(p)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Evicting Hibernate object %1$s", p));
			getHibernateSession().evict(p);
		}
		else if (logger.isWarnEnabled()) {
			logger.warn(String.format("Trying to evict an object that is not Hibernate: %1$s", p));
		}
	}

	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return getEntityManager().getCriteriaBuilder();
	}

	@Override
	public <T> TypedQuery<T> createQuery(CriteriaQuery<T> q) {
		return getEntityManager().createQuery(q);
	}

	@Override
	public <T> javax.persistence.Query createQuery(CriteriaUpdate<T> u) {
		return getEntityManager().createQuery(u);
	}

	@Override
	public <T> javax.persistence.Query createQuery(CriteriaDelete<T> d) {
		return getEntityManager().createQuery(d);
	}

	@Override
	@Deprecated
	public DetachedCriteria createCriteria(IPersistentObject p) {
		return createCriteria(p.getClass());
	}
	@Override
	@Deprecated
	public DetachedCriteria createCriteria(Class<? extends IPersistentObject> c) {
		if (isHibernate(c)) {
			if (logger.isDebugEnabled()) logger.debug(String.format("Creating Hibernate DetachedCriteria for class %1$s", c));
			return DetachedCriteria.forClass(c);
		}
		else {
			if (logger.isWarnEnabled()) {
				logger.warn(String.format("Trying to createCriteria for a class that is not Hibernate: %1$s", c));
			}
			return null;
		}
	}

	@Override
	@Deprecated
	@SuppressWarnings("unchecked")
	public List<? extends IPersistentObject> findByCriteria(DetachedCriteria dc) {
		return dc.getExecutableCriteria(getHibernateSession()).list();
	}
	@Override
	@Deprecated
	@SuppressWarnings("unchecked")
	public List<? extends IPersistentObject> findByCriteria(DetachedCriteria dc, int firstResult, int maxResults) {
		return dc.getExecutableCriteria(getHibernateSession()).setFirstResult(firstResult).setMaxResults(maxResults).list();
	}

	@Override
	@Deprecated
	public Object uniqueResult(DetachedCriteria dc) {
		return dc.getExecutableCriteria(getHibernateSession()).uniqueResult();
	}

	/* ################ Non PersistentOperations Methods ################ */

	/**
	 * Override this function to change the default automatic behavior when saving objects.<br>
	 * By default, this method implements the {@link GeneratedValue} annotation only.
	 * @param p the {@link IPersistentObject} to change
	 * @return the {@link IPersistentObject} changed
	 */
	protected <T extends IPersistentObject> T doBeforeSave(T p) {
		List<Field> fields = getAllFields(p);
		for (Field field : fields) {
			if (field.isAnnotationPresent(GeneratedValue.class)) {
				p = generateColumn(p, field);
			}
		}
		p.doBeforeSave();
		return p;
	}

	/**
	 * Creates a {@link Query} by example for {@link Document} objects
	 * @param p the {@link IPersistentObject}
	 * @return Mongo {@link Query}
	 */
	public Query createExampleQuery(IPersistentObject p) {
		Query ret = null;
		Criteria c = createExampleCriteria(p);
		if (c != null) {
			ret = new Query(c);
		}
		return ret;
	}

	/**
	 * Creates a {@link Criteria} by example for {@link Document} objects
	 * @param o the Mongo object
	 * @return Mongo {@link Criteria}
	 */
	public Criteria createExampleCriteria(Object o) {
		return createExampleCriteria(o, null, null);
	}
	/**
	 * Creates a {@link Criteria} by example for {@link Document} objects
	 * @param o the value of key or a mongo object
	 * @param key the field key value
	 * @return Mongo {@link Criteria}
	 */
	public Criteria createExampleCriteria(Object o, String key) {
		return createExampleCriteria(o, key, null);
	}
	/**
	 * Creates a {@link Criteria} by example for {@link Document} objects
	 * @param o the value of key or a mongo object
	 * @param key the field key value
	 * @param c the additional {@link Criteria} to concatenate
	 * @return Mongo {@link Criteria}
	 */
	public Criteria createExampleCriteria(Object o, String key, Criteria c) {
		Criteria ret = c;
		if (o != null) {
			if (key == null) key = "";
			Object mo = getMongoOperations().getConverter().convertToMongoType(o);
			if (o.equals(mo)) {
				if (ret == null) ret = Criteria.where(key).is(o);
				else ret.and(key).is(o);
			}
			else if (o instanceof Number) {
				if (ret == null) ret = Criteria.where(key).is(mo);
				else ret.and(key).is(mo);
			}
			else {
				for (Field field : getAllFields(o)) {
					Method get = null;
					Object value = null;
					try {
						if (isFieldPersistent(field)) {
							get = o.getClass().getMethod("get" + getMethodName(field));
							value = get.invoke(o);
							if (value != null) {
								ret = createExampleCriteria(value, ((key.length() > 0) ? (key + ".") : "") + getColumnName(field), ret);
							}
						}
					} catch (Exception e) {
						if (logger.isWarnEnabled()) {
							logger.warn(String.format("Problem when creating ExampleCriteria for %1$s using method [%2$s] value [%3$s] key [%4$s] criteria [%5$s]", o, get, value, key, ret), e);
						}
						continue;
					}
				}
			}
		}
		return ret;
	}

	/**
	 * Override this method to change the default behavior of column generation for {@link GeneratedValue}
	 * @param p the {@link IPersistentObject} to generate the column for
	 * @param f the {@link Field} representing the column
	 * @return the {@link IPersistentObject} with the modified field
	 */
	protected <T extends IPersistentObject> T generateColumn(T p, Field f) {
		GeneratedValue g = f.getAnnotation(GeneratedValue.class);
		if (g == null) {
			if (logger.isWarnEnabled()) logger.warn(String.format("Trying to generateColumn with a field that is not annotated with %1$s", GeneratedValue.class.getName()));
			return p;
		}
		if (f.getType().getGenericSuperclass() != null ? !f.getType().getGenericSuperclass().equals(Number.class) : false) {
			if (logger.isWarnEnabled()) logger.warn(String.format("Trying to generateColumn with a field that is not a Number [%1$s]", f.getType()));
			return p;
		}

		String column = getColumnName(f);

		String method = getMethodName(f);
		String setMethod = "set" + method;
		String getMethod = "get" + method;

		@SuppressWarnings("unchecked")
		Class<? extends Number> paramClass = (Class<? extends Number>) f.getType();

		try {
			Method get = p.getClass().getMethod(getMethod);
			if (get.invoke(p) == null) {

				Method set = p.getClass().getMethod(setMethod, paramClass);
				if (set != null) {
					if (set.getParameterTypes().length > 0) {

						BigInteger next = BigInteger.ONE;
						switch (g.strategy()) {

							case MAX:
								if (isHibernate(p)) {
									CriteriaBuilder cb = getCriteriaBuilder();
									CriteriaQuery<? extends Number> q = cb.createQuery(paramClass);
									Root<? extends IPersistentObject> r = q.from(p.getClass());
									q.select(cb.max(r.get(column)));
									q.where(addPKPredicate(p, cb, r));
									next = SimpleFunctions.getBigIntegerValue(createQuery(q).getSingleResult());
								}
								else if (isMongo(p)) {
									Query q = new Query(addPKCriteria(p)).with(new Sort(Direction.DESC, column));
									IPersistentObject np = getMongoOperations().findOne(q, p.getClass());
									next = SimpleFunctions.getBigIntegerValue(get.invoke(np));
								}
								if (next == null) next = BigInteger.ZERO;
								next = next.add(BigInteger.ONE);
								break;

							case FILL_BLANK:
								next = findBlank(p, f);
								break;

						}

						if (paramClass.equals(BigInteger.class)) {
							set.invoke(p, next);
						}
						else if (paramClass.equals(BigDecimal.class)) {
							set.invoke(p, new BigDecimal(next));
						}
						else if (paramClass.equals(Long.class)) {
							set.invoke(p, next.longValue());
						}
						else if (paramClass.equals(Integer.class)) {
							set.invoke(p, next.intValue());
						}
						else if (paramClass.equals(Short.class)) {
							set.invoke(p, next.shortValue());
						}

					}
				}

			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(String.format("Error generating value for [%1$s] using Field [%2$s]", p, f), e);
			}
		}
		return p;
	}

	//TODO: Optimize this algorithm to get data from database only 1 time.
	private BigInteger findBlank(IPersistentObject p, Field f, BigInteger[] minmax) {
		Class<? extends IPersistentObject> clazz = p.getClass();

		BigInteger ret = BigInteger.ONE;

		String propertyName = getColumnName(f);
		String method = getMethodName(f);
		String getMethod = "get" + method;

		@SuppressWarnings("unchecked")
		Class<? extends Number> paramClass = (Class<? extends Number>) f.getType();

		try {
			Method get = p.getClass().getMethod(getMethod);

			BigInteger min = BigInteger.ZERO;
			BigInteger max = BigInteger.ZERO;
			if (minmax != null) {
				min = minmax[0];
				if (min == null) min = BigInteger.ZERO;
				max = minmax[1];
				if (max == null) max = BigInteger.ZERO;
			}
			else {
				minmax = new BigInteger[2];
			}

			if (max == BigInteger.ZERO) {
				if (isHibernate(p)) {
					CriteriaBuilder cb = getCriteriaBuilder();
					CriteriaQuery<? extends Number> q = cb.createQuery(paramClass);
					Root<? extends IPersistentObject> r = q.from(clazz);
					q.select(cb.max(r.get(propertyName)));
					q.where(addPKPredicate(p, cb, r));
					BigInteger rst = SimpleFunctions.getBigIntegerValue(createQuery(q).getSingleResult());
					if (rst != null) max = rst;
				}
				else if (isMongo(p)) {
					Query q = new Query(addPKCriteria(p)).with(new Sort(Direction.DESC, propertyName));
					IPersistentObject np = getMongoOperations().findOne(q, p.getClass());
					if (np != null) max = SimpleFunctions.getBigIntegerValue(get.invoke(np));
				}
			}

			if (!max.equals(BigInteger.ZERO)) { //max != 0
				if (max.equals(min)) { //max == min
					ret = min;
				}
				else {
					BigInteger count = BigInteger.ZERO;
					if (isHibernate(p)) {
						CriteriaBuilder cb = getCriteriaBuilder();
						CriteriaQuery<Long> q = cb.createQuery(Long.class);
						Root<? extends IPersistentObject> r = q.from(clazz);
						q.select(cb.count(r.get(propertyName)));
						q.where(cb.and(addPKPredicate(p, cb, r), cb.and(cb.le(r.get(propertyName), max), cb.ge(r.get(propertyName), min))));
						count = SimpleFunctions.getBigIntegerValue(createQuery(q).getSingleResult());
					}
					else if (isMongo(p)) {
						Query q = new Query(addPKCriteria(p));
						q.addCriteria(Criteria.where(propertyName).lte(max).and(propertyName).gte(min));
						count = SimpleFunctions.getBigIntegerValue(getMongoOperations().count(q, p.getClass()));
					}
					if (count.equals(BigInteger.ZERO)) { //count == 0
						ret = min;
					}
					else if (count.compareTo(max.subtract(min)) < 0) { // count < (max - min)
						minmax[0] = min;
						minmax[1] = min.add(max.subtract(min).shiftRight(1)); //min + ((max - min) >> 1); //min + ((max - min) / 2);
						ret = findBlank(p, f, minmax);
						if (ret.compareTo(minmax[1]) > 0) { //ret > minmax[1]
							minmax[0] = ret;
							minmax[1] = max;
							ret = findBlank(p, f, minmax);
						}
					}
					else {
						ret = max.add(BigInteger.ONE);
					}

				}
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(String.format("Error finding a blank value for [%1$s] using Field [%2$s]", p, f), e);
			}
		}
		return ret;
	}
	/**
	 * Override this method to change the default behavior of column generation {@link GeneratedValue} FILL_BLANK
	 * @param p the {@link IPersistentObject} to generate the column for
	 * @param f the {@link Field} representing the column
	 * @return the number found
	 */
	protected BigInteger findBlank(IPersistentObject p, Field f) {
		return findBlank(p, f, null);
	}

	/* ################ Static Methods ################ */

	/**
	 * Override this method to change the way to get object fields
	 * @param o the object to get the field list from
	 * @return a list of {@link Field}
	 */
	protected static List<Field> getAllFields(Object o) {
		List<Field> ret = new ArrayList<Field>();
		if (o != null) {
			ret.addAll(getAllFields(o.getClass(), null));
		}
		return ret;
	}
	private static List<Field> getAllFields(Class<?> clazz, List<Field> fields) {
		List<Field> ret = new ArrayList<Field>();
		if (fields != null) ret.addAll(fields);
		if (clazz != null) {
			ret.addAll(Arrays.asList(clazz.getDeclaredFields()));
			if (clazz.getSuperclass() != null) {
				ret.addAll(getAllFields(clazz.getSuperclass(), fields));
			}
		}
		return ret;
	}

	/**
	 * Override this to change the filter of persistent fields
	 * @param field the field from a persistent object
	 * @return true if the field should be considered persistent, false otherwise
	 */
	protected static Boolean isFieldPersistent(Field field) {
		return !field.isAnnotationPresent(Transient.class) && !field.isAnnotationPresent(javax.persistence.Transient.class) && !field.isSynthetic();
	}

	/**
	 * Override this method to change the way to get version fields
	 * @param o the object to get version field from
	 * @return a {@link Field} object
	 */
	protected static Field getVersionField(Object o) {
		Field ret = null;
		if (o != null) {
			List<Field> fields = getAllFields(o);
			for (Field field : fields) {
				if (field.isAnnotationPresent(Version.class)) {
					ret = field;
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * Override this method to change the way to get ID fields
	 * @param o the object to get ID field from
	 * @return a {@link Field} object
	 */
	protected static List<Field> getIdFields(Object o) {
		List<Field> ret = null;
		if (o != null) {
			ret = new ArrayList<Field>();
			List<Field> fields = getAllFields(o);
			for (Field field : fields) {
				if (field.isAnnotationPresent(Id.class)) {
					ret.add(field);
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * Override this method to change the way to get column names
	 * @param f the field to extract column name from
	 * @return the column name
	 */
	protected static String getColumnName(Field f) {
		String ret = null;
		if (f != null) {
			ret = f.getName();
			Column ca = f.getAnnotation(Column.class);
			if ( (ca != null) ? ( (ca.name() != null) ? (ca.name().length() > 0) : false ) : false ) ret = ca.name();
		}
		return ret;
	}

	/**
	 * Override this method to change the way to get method names
	 * @param f the field to extract method name from
	 * @return the method name (without get or set)
	 */
	protected static String getMethodName(Field f) {
		String ret = null;
		if (f != null) {
			ret = f.getName();
			ret = ret.substring(0, 1).toUpperCase() + ( (ret.length() > 1) ? ret.substring(1).toLowerCase() : "" );
		}
		return ret;
	}

	/**
	 * To be used with {@link Entity} objects only.<br>
	 * Create a {@link Predicate} with the {@link IPersistentObject} primary keys
	 * @param p the {@link IPersistentObject} to extract primary keys and it's values
	 * @param cb the {@link CriteriaBuilder}
	 * @param r the {@link Root} table
	 * @return the {@link Predicate} containing the primary keys and values
	 */
	public static <T extends IPersistentObject> Predicate addPKPredicate(IPersistentObject p, CriteriaBuilder cb, Root<T> r) {
		return addPKPredicate(p, cb, r, null);
	}
	/**
	 * To be used with {@link Entity} objects only.<br>
	 * Add an <b>AND</b> {@link Predicate} with the {@link IPersistentObject} primary keys to an existing predicate
	 * @param p the {@link IPersistentObject} to extract primary keys and it's values
	 * @param cb the {@link CriteriaBuilder}
	 * @param r the {@link Root} table
	 * @param pr the {@link Predicate} to add to returning predicate
	 * @return the {@link Predicate} containing the primary keys and values
	 */
	public static <T extends IPersistentObject> Predicate addPKPredicate(IPersistentObject p, CriteriaBuilder cb, Root<T> r, Predicate pr) {
		if (p != null && cb != null && r != null) {
			if (pr == null) pr = cb.conjunction();
			List<Predicate> l = new ArrayList<Predicate>();
			List<Field> idFields = getIdFields(p);
			for (Field idField : idFields) {
				Method get = null;
				Object value = null;
				try {
					get = p.getClass().getMethod("get" + getMethodName(idField));
					value = get.invoke(p);
				} catch (Exception e) {
					if (logger.isErrorEnabled()) {
						logger.error(String.format("Error getting PK value from [%1$s] using Method [%2$s]", p, get), e);
					}
				}
				if (value != null) {
					l.add(cb.equal(r.get(getColumnName(idField)), value));
				}
			}
			pr = cb.and(pr, cb.and(l.toArray(new Predicate[l.size()])));
		}
		return pr;
	}

	/**
	 * To be used with {@link Document} objects only.<br>
	 * Create a {@link Criteria} with the {@link IPersistentObject} primary keys
	 * @param p the {@link IPersistentObject} to extract primary keys and it's values
	 * @return the {@link Criteria} containing the primary keys and values
	 */
	public static Criteria addPKCriteria(IPersistentObject p) {
		return addPKCriteria(p, null);
	}
	/**
	 * To be used with {@link Document} objects only.<br>
	 * Add an <b>AND</b> {@link Criteria} with the {@link IPersistentObject} primary keys to an existing predicate
	 * @param p the {@link IPersistentObject} to extract primary keys and it's values
	 * @param c the {@link Criteria} to add to returning criteria
	 * @return the {@link Criteria} containing the primary keys and values
	 */
	public static Criteria addPKCriteria(IPersistentObject p, Criteria c) {
		Criteria ret = c;
		List<Field> idFields = getIdFields(p);
		for (Field idField : idFields) {
			Method get = null;
			Object value = null;
			try {
				get = p.getClass().getMethod("get" + getMethodName(idField));
				value = get.invoke(p);
			} catch (Exception e) {
				if (logger.isErrorEnabled()) {
					logger.error(String.format("Error getting PK value from [%1$s] using Method [%2$s]", p, get), e);
				}
			}
			if (value != null) {
				if (ret == null) ret = Criteria.where(getColumnName(idField)).is(value);
				else ret.and(getColumnName(idField)).is(value);
			}
		}
		return ret;
	}

	/**
	 * To be used with {@link Entity} objects only.<br>
	 * Create a {@link Predicate} with the {@link IPersistentObject} non null fields/values using <b>equal</b> comparison
	 * @param p the {@link IPersistentObject} to extract non null values
	 * @param cb the {@link CriteriaBuilder}
	 * @param r the {@link Root} table
	 * @return the {@link Predicate} containing the non null fields/values
	 */
	public static <T extends IPersistentObject> Predicate getNotNullPredicate(IPersistentObject p, CriteriaBuilder cb, Root<T> r) {
		return getNotNullPredicate(p, cb, r, false);
	}
	/**
	 * To be used with {@link Entity} objects only.<br>
	 * Create a {@link Predicate} with the {@link IPersistentObject} non null fields/values
	 * @param p the {@link IPersistentObject} to extract non null values
	 * @param cb the {@link CriteriaBuilder}
	 * @param r the {@link Root} table
	 * @param like (true) or <b>equal</b> (false) comparison
	 * @return the {@link Predicate} containing the non null fields/values
	 */
	public static <T extends IPersistentObject> Predicate getNotNullPredicate(IPersistentObject p, CriteriaBuilder cb, Root<T> r, boolean like) {
		Predicate ret = cb.conjunction();
		List<Predicate> l = new ArrayList<Predicate>();
		for (Field field : getAllFields(p)) {
			if (isFieldPersistent(field)) {
				Method get = null;
				Object value = null;
				try {
					get = p.getClass().getMethod("get" + getMethodName(field));
					value = get.invoke(p);
				} catch (Exception e) {
					if (logger.isErrorEnabled()) {
						logger.error(String.format("Error getting object value from [%1$s] using Method [%2$s]", p, get), e);
					}
				}
				if (value != null) {
					if (like && value instanceof String) {
						l.add(cb.like(cb.lower(r.get(getColumnName(field))), String.format("%%%1$s%%", value).toLowerCase()));
					}
					else {
						l.add(cb.equal(r.get(getColumnName(field)), value));
					}
				}
			}
		}
		if (l.size() > 0) ret = cb.and(l.toArray(new Predicate[l.size()]));
		return ret;
	}

	/**
	 * To be used with {@link Document} objects only.<br>
	 * Create a {@link Criteria} with the {@link IPersistentObject} non null fields/values using <b>equal</b> comparison
	 * @param p the {@link IPersistentObject} to extract non null values
	 * @return the {@link Criteria} containing the non null fields/values
	 */
	public static Criteria addNotNullCriteria(IPersistentObject p) {
		return addNotNullCriteria(p, null);
	}
	/**
	 * To be used with {@link Document} objects only.<br>
	 * Add an <b>AND</b> {@link Criteria} with the {@link IPersistentObject} non null fields/values using <b>equal</b> comparison
	 * @param p the {@link IPersistentObject} to extract non null values
	 * @param c the {@link Criteria} to add to returning criteria
	 * @return the {@link Criteria} containing the non null fields/values
	 */
	public static Criteria addNotNullCriteria(IPersistentObject p, Criteria c) {
		return addNotNullCriteria(p, c, false);
	}
	/**
	 * To be used with {@link Document} objects only.<br>
	 * Create a {@link Criteria} with the {@link IPersistentObject} non null fields/values
	 * @param p the {@link IPersistentObject} to extract non null values
	 * @param like (true) or <b>equal</b> (false) comparison
	 * @return the {@link Criteria} containing the non null fields/values
	 */
	public static Criteria addNotNullCriteria(IPersistentObject p, boolean like) {
		return addNotNullCriteria(p, null, false);
	}
	/**
	 * To be used with {@link Document} objects only.<br>
	 * Add an <b>AND</b> {@link Criteria} with the {@link IPersistentObject} non null fields/values
	 * @param p the {@link IPersistentObject} to extract non null values
	 * @param c the {@link Criteria} to add to returning criteria
	 * @param like (true) or <b>equal</b> (false) comparison
	 * @return the {@link Criteria} containing the non null fields/values
	 */
	public static Criteria addNotNullCriteria(IPersistentObject p, Criteria c, boolean like) {
		Criteria ret = c;
		for (Field field : getAllFields(p)) {
			if (isFieldPersistent(field)) {
				Method get = null;
				Object value = null;
				try {
					get = p.getClass().getMethod("get" + getMethodName(field));
					value = get.invoke(p);
				} catch (Exception e) {
					if (logger.isErrorEnabled()) {
						logger.error(String.format("Error getting object value from [%1$s] using Method [%2$s]", p, get), e);
					}
				}
				if (value != null) {
					if (like && (value instanceof String)) {
						if (ret == null) ret = Criteria.where(getColumnName(field)).regex((String) value, "ixs");
						else ret.and(getColumnName(field)).regex((String) value, "ixs");
					}
					else {
						if (ret == null) ret = Criteria.where(getColumnName(field)).is(value);
						else ret.and(getColumnName(field)).is(value);
					}
				}
			}
		}
		return ret;
	}

}
