package com.ostegn.jenesis.server.base;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * E-mail sender helper class.
 *
 * @author Thiago Ricciardi
 *
 */
public class EmailSender {

	protected static transient Log logger = LogFactory.getLog(EmailSender.class);

	/**
	 * Creates an e-mail sender.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @param _type The e-mail content type.
	 * @param _ssl If SSL is needed.
	 *
	 * @see Types
	 */
	public EmailSender(String _host, Integer _port, String _usr, String _pwd, String _type, Boolean _ssl) {
		host = _host;
		port = _port;
		usr = _usr;
		pwd = _pwd;
		type = _type;
		ssl = _ssl;
	}

	/**
	 * Creates an e-mail sender.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @param _type The e-mail content type.
	 *
	 * @see Types
	 */
	public EmailSender(String _host, Integer _port, String _usr, String _pwd, String _type) {
		host = _host;
		port = _port;
		usr = _usr;
		pwd = _pwd;
		type = _type;
	}
	/**
	 * Creates an e-mail sender.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 */
	public EmailSender(String _host, Integer _port, String _usr, String _pwd) {
		host = _host;
		port = _port;
		usr = _usr;
		pwd = _pwd;
	}
	/**
	 * Creates an e-mail sender.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _type The e-mail content type.
	 *
	 * @see Types
	 */
	public EmailSender(String _host, Integer _port, String _type) {
		host = _host;
		port = _port;
		type = _type;
	}
	/**
	 * Creates an e-mail sender.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 */
	public EmailSender(String _host, Integer _port) {
		host = _host;
		port = _port;
	}
	/**
	 * Creates an e-mail sender.
	 * @param _type The e-mail content type.
	 *
	 * @see Types
	 */
	public EmailSender(String _type) {
		type = _type;
	}
	/**
	 * Creates an e-mail sender.
	 */
	public EmailSender() {}

	/**
	 * Class used to define the main content types.<br>
	 * Note that the content type is a String field, so you do not need to stick to the types shown here.
	 * @author Thiago Ricciardi
	 */
	public static class Types {

		/** Plain text type, no rich format */
		public static final String PLAIN = "text/plain; charset=UTF-8";
		/** HTML type, use this if you need rich formatting */
		public static final String HTML =  "text/html; charset=UTF-8";

	}

	/** Sender e-mail address. */
	public InternetAddress from;
	/** Recipient e-mail address. */
	public InternetAddress to;
	/** Reply-to e-mail address. */
	public InternetAddress replyto;
	/** E-mail subject. */
	public String subject;
	/** E-mail body. */
	public String body;
	/** E-mail host server. */
	public String host;
	/** Port to communicate with e-mail host server. */
	public Integer port;
	/** User to authenticate at the e-mail host server. */
	public String usr;
	/** Password for the user. */
	public String pwd;
	/** The e-mail content type. */
	public String type;
	/** If SSL is needed. */
	public Boolean ssl;

	/**
	 * Send the e-mail using the parameters passed plus the parameters configured before.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _type The e-mail content type.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public void send(InternetAddress _from, InternetAddress _to, String _subject, String _body, String _type) throws MessagingException {
		from = _from;
		to = _to;
		subject = _subject;
		body = _body;
		type = _type;
		send();
	}
	/**
	 * Send the e-mail using the parameters passed plus the parameters configured before.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _type The e-mail content type.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public void send(String _from, String _to, String _subject, String _body, String _type) throws UnsupportedEncodingException, MessagingException {
		send(createAddress(_from), createAddress(_to), _subject, _body, _type);
	}
	/**
	 * Send the e-mail using the parameters passed plus the parameters configured before.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public void send(InternetAddress _from, InternetAddress _to, String _subject, String _body) throws MessagingException {
		send(_from, _to, _subject, _body, type);
	}
	/**
	 * Send the e-mail using the parameters passed plus the parameters configured before.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public void send(String _from, String _to, String _subject, String _body) throws UnsupportedEncodingException, MessagingException {
		send(createAddress(_from), createAddress(_to), _subject, _body, type);
	}
	/**
	 * Send the e-mail using the parameters configured before.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public void send() throws MessagingException {
		sendSimple(from, to, replyto, subject, body, host, port, usr, pwd, type, ssl);
	}

	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @param _type The e-mail content type.
	 * @param _ssl If SSL is needed.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, InternetAddress _replyto, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd, String _type, Boolean _ssl) throws MessagingException {

		// Default values
		if (_host == null) _host = "localhost";
		if (_type == null) _type = Types.PLAIN;
		if (_port == null) _port = 25;
		if (_ssl == null) _ssl = false;

		String protocol = _ssl ? "smtps" : "smtp";

		// Properties
		Properties props = new Properties();
		props.put("mail." + protocol + ".host", _host);
		props.put("mail." + protocol + ".port", _port);
		props.put("mail.from", _from.toString());
		props.put("mail." + protocol + ".starttls.enable", true);

		// Mail session to send mails
		Session mailSession;
		if (_usr != null && _pwd != null) { // With auth
			props.put("mail." + protocol + ".auth", true);
			mailSession = Session.getInstance(props, new SMTPAuth(_usr, _pwd));
		}
		else { // Without auth
			//_usr = "";
			//_pwd = "";
			mailSession = Session.getInstance(props);
		}

		// Message
		MimeMessage msg = new MimeMessage(mailSession);
		//msg.setFrom(_from);
		if (_replyto != null) msg.setReplyTo(new InternetAddress[] {_replyto});
		msg.setFrom();
		msg.setRecipient(RecipientType.TO, _to);
		msg.setSubject(_subject, "UTF-8");
		msg.setSentDate(new Date());
		msg.setContent(_body, _type);

		// Send mail through transport
		Transport mTrsp = mailSession.getTransport(protocol);
		if (logger.isDebugEnabled()) logger.debug(String.format("Connecting to [%1$s:%2$s] with auth [%3$s] ssl [%4$s]", _host, _port, (_usr != null && _pwd != null), _ssl));
		try {
			if (_usr != null && _pwd != null) {
				mTrsp.connect(_usr, _pwd);
			}
			else {
				mTrsp.connect();
			}
			msg.saveChanges();
			if (logger.isDebugEnabled()) {
				logger.debug(
						String.format("Sending message from [%1$s] to [%2$s] using [%3$s:%4$s] sender [%5$s] auth [%6$s] ssl [%7$s]",
								msg.getFrom(), msg.getAllRecipients(), _host, _port, msg.getSender(), (_usr != null && _pwd != null), _ssl));
			}
			mTrsp.sendMessage(msg, msg.getAllRecipients());
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(String.format("Error trying to send message using [%1$s:%2$s] auth [%3$s] ssl [%4$s]", _host, _port, (_usr != null && _pwd != null), _ssl), e);
			}
		} finally {
			mTrsp.close();
		}

	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd The e-mail content type.
	 * @param _ssl If SSL is needed.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd, Boolean _ssl) throws MessagingException {
		sendSimple(_from, _to, null, _subject, _body, _host, _port, _usr, _pwd, null, _ssl);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd The e-mail content type.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd) throws MessagingException {
		sendSimple(_from, _to, null, _subject, _body, _host, _port, _usr, _pwd, null, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _type The e-mail content type.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, String _subject, String _body, String _host, Integer _port, String _type) throws MessagingException {
		sendSimple(_from, _to, null, _subject, _body, _host, _port, null, null, _type, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @throws MessagingException
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, String _subject, String _body, String _host, Integer _port) throws MessagingException {
		sendSimple(_from, _to, null, _subject, _body, _host, _port, null, null, null, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _type The e-mail content type.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, String _subject, String _body, String _type) throws MessagingException {
		sendSimple(_from, _to, null, _subject, _body, null, null, null, null, _type, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(InternetAddress _from, InternetAddress _to, String _subject, String _body) throws MessagingException {
		sendSimple(_from, _to, null, _subject, _body, null, null, null, null, null, null);
	}

	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @param _type The e-mail content type.
	 * @param _ssl If SSL is needed.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd, String _type, Boolean _ssl) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(createAddress(_from), createAddress(_to), null, _subject, _body, _host, _port, _usr, _pwd, _type, _ssl);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @param _type The e-mail content type.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd, String _type) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(createAddress(_from), createAddress(_to), null, _subject, _body, _host, _port, _usr, _pwd, _type, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @param _ssl If SSL is needed.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd, Boolean _ssl) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(_from, _to, _subject, _body, _host, _port, _usr, _pwd, null, _ssl);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _usr User to authenticate at the e-mail host server.
	 * @param _pwd Password for the user.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _host, Integer _port, String _usr, String _pwd) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(_from, _to, _subject, _body, _host, _port, _usr, _pwd, null, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @param _type The e-mail content type.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _host, Integer _port, String _type) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(_from, _to, _subject, _body, _host, _port, null, null, _type, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _host E-mail host server.
	 * @param _port Port to communicate with e-mail host server.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _host, Integer _port) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(_from, _to, _subject, _body, _host, _port, null, null, null, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @param _type The e-mail content type.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body, String _type) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(_from, _to, _subject, _body, null, null, null, null, _type, null);
	}
	/**
	 * Simple send without need to instance.
	 * @param _from Sender e-mail address.
	 * @param _to Recipient e-mail address.
	 * @param _subject E-mail subject.
	 * @param _body E-mail body.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @throws MessagingException Problem sending the e-mail.
	 */
	public static void sendSimple(String _from, String _to, String _subject, String _body) throws AddressException, UnsupportedEncodingException, MessagingException {
		sendSimple(_from, _to, _subject, _body, null, null, null, null, null, null);
	}

	/**
	 * Creates an {@link InternetAddress} given the e-mail and name provided.
	 * @param email E-mail to generate the InternetAddress.
	 * @param name Name to generate the InternetAddress.
	 * @return The InternetAddress instance to be used anywhere.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @see InternetAddress
	 */
	public static InternetAddress createAddress(String email, String name) throws AddressException, UnsupportedEncodingException {
		if (name != null) {
			return new InternetAddress(email, name, "UTF-8");
		}
		else {
			return new InternetAddress(email);
		}
	}
	/**
	 * Creates an {@link InternetAddress} given the e-mail and name provided.
	 * @param email E-mail to generate the InternetAddress.
	 * @return The InternetAddress instance to be used anywhere.
	 * @throws AddressException Problem converting the e-mails to InternetAddress.
	 * @throws UnsupportedEncodingException Problem converting the e-mails to InternetAddress.
	 * @see InternetAddress
	 */
	public static InternetAddress createAddress(String email) throws AddressException, UnsupportedEncodingException {
		return createAddress(email, null);
	}

	private static class SMTPAuth extends Authenticator {

		private String _usr;
		private String _pwd;

		public SMTPAuth(String usr, String pwd) {
			_usr = usr;
			_pwd = pwd;
		}

		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(_usr, _pwd);
		}

	}

}
