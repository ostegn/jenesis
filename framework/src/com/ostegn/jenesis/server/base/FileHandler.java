package com.ostegn.jenesis.server.base;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * A File handler helper class
 * @author Thiago Ricciardi
 *
 */
public class FileHandler {
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator", "\r\n");
	private static final String DEFAULT_ENCODING = "UTF-8";
	
	private File file;
	
	/**
	 * Create a new FileHandler with a string path
	 * @param path the string path
	 */
	public FileHandler(String path) {
		init(new File(path));
	}
	
	/**
	 * Create a new FileHandler with a java File reference
	 * @param file the java File reference
	 */
	public FileHandler(File file) {
		init(file);
	}
	
	private void init(File file) {
		this.file = file;
	}
	
	/**
	 * Read the entire file as a byte array
	 * @return the file contents as a byte array
	 * @throws IOException if an I/O error occurs while reading the file
	 */
	public byte[] readBytes() throws IOException {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		ArrayList<Byte> bytes = new ArrayList<Byte>();
		try {
			int r = bis.read();
			while (r != -1) {
				bytes.add(Byte.valueOf((byte) r));
				r = bis.read();
			}
		}
		finally {
			bis.close();
		}
		byte[] ret = new byte[bytes.size()];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = bytes.get(i);
		}
		return ret;
	}
	
	/**
	 * Read the entire file as a string using UTF-8 encoding
	 * @return the file contents as a string
	 * @throws IOException if an I/O error occurs while reading the file
	 */
	public String readString() throws IOException {
		return readString(DEFAULT_ENCODING);
	}
	
	/**
	 * Read the entire file as a string using the defined encoding
	 * @param encoding the string encoding ex.: "UTF-8"
	 * @return the file contents as a string
	 * @throws IOException if an I/O error occurs while reading the file
	 */
	public String readString(String encoding) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
		StringBuilder sb = new StringBuilder();
		String buffer = new String();
		try {
			while ( (buffer = br.readLine()) != null ) {
				sb.append(buffer);
				sb.append(LINE_SEPARATOR);
			}
		} finally {
			br.close();
		}
		return sb.toString();
	}
	
	/**
	 * Write the file contents as string, overwriting if already exists
	 * @param string the file contents as a string
	 * @throws IOException if an I/O error occurs while writing the file
	 */
	public void write(String string) throws IOException {
		write(string, false);
	}
	/**
	 * Write the file contents as string, defining overwriting if already exists
	 * @param string the file contents as a string
	 * @param append if the contents are appended or overwrite
	 * @throws IOException if an I/O error occurs while writing the file
	 */
	public void write(String string, boolean append) throws IOException {
		if (!file.exists()) file.createNewFile();
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, append)));
		try {
			out.write(string);
		}
		finally {
			out.close();
		}
	}
	
	/**
	 * Write the file contents as a byte array
	 * @param bytes the file contents as a byte array
	 * @throws IOException if an I/O error occurs while writing the file
	 */
	public void write(byte[] bytes) throws IOException {
		if (!file.exists()) file.createNewFile();
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
		try {
			bos.write(bytes);
		}
		finally {
			bos.close();
		}
	}

}
