package com.ostegn.jenesis.server.base;

import java.util.Calendar;

/**
 * 
 * Collection of useful functions <b>(server only)</b>.
 * 
 * @author Thiago Ricciardi
 *
 */
public class ServerFunctions {
	
	/**
	 * Sets the time for a given calendar.
	 * @param target The calendar representing the date/time to be changed.
	 * @param hour The hour to set.
	 * @param min The minute to set.
	 * @param sec The second to set.
	 * @param millisec The milliseconds to set.
	 * @return The same input calendar changed (for chain purposes).
	 */
	public static Calendar setTime(Calendar target, Integer hour, Integer min, Integer sec, Integer millisec) {
		target.set(Calendar.HOUR_OF_DAY, hour);
		target.set(Calendar.MINUTE, min);
		target.set(Calendar.SECOND, sec);
		target.set(Calendar.MILLISECOND, millisec);
		return target;
	}
	
	/**
	 * Sets the minimum time for the given calendar day.
	 * @param target The calendar representing the date/time to be changed.
	 * @return The same input calendar (for chain purposes).
	 */
	public static Calendar setTimeMin(Calendar target) {
		return setTime(target, 0, 0, 0, 0);
	}
	/**
	 * Sets the maximum time for the given calendar day.
	 * @param target The calendar representing the date/time to be changed.
	 * @return The same input calendar (for chain purposes).
	 */
	public static Calendar setTimeMax(Calendar target) {
		return setTime(target, 23, 59, 59, 999);
	}
	
	/**
	 * Creates a new calendar with the time given for an input base calendar.
	 * @param base The calendar representing the date/time of the new calendar to be created.
	 * @param hour The hour to set.
	 * @param min The minute to set.
	 * @param sec The second to set.
	 * @param millisec The milliseconds to set.
	 * @return A new calendar object with the changes.
	 */
	public static Calendar getTime(Calendar base, Integer hour, Integer min, Integer sec, Integer millisec) {
		Calendar ret = Calendar.getInstance();
		ret.setTimeInMillis(base.getTimeInMillis());
		return setTime(ret, hour, min, sec, millisec);
	}
	
	/**
	 * Creates a new calendar with the minimum time for the given input day.
	 * @param base The calendar representing the date/time of the new calendar to be created.
	 * @return A new calendar object with the changes.
	 */
	public static Calendar getTimeMin(Calendar base) {
		Calendar ret = Calendar.getInstance();
		ret.setTimeInMillis(base.getTimeInMillis());
		return setTimeMin(ret);
	}
	/**
	 * Creates a new calendar with the maximum time for the given input day.
	 * @param base The calendar representing the date/time of the new calendar to be created.
	 * @return A new calendar object with the changes.
	 */
	public static Calendar getTimeMax(Calendar base) {
		Calendar ret = Calendar.getInstance();
		ret.setTimeInMillis(base.getTimeInMillis());
		return setTimeMax(ret);
	}

}
