package com.ostegn.jenesis.server.base;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.data.mongodb.core.MongoOperations;

import com.ostegn.jenesis.shared.base.IPersistentObject;

/**
 *
 * This class defines a main transactional class of this framework.<br>
 * Implemented by {@link PersistentHandler}
 *
 * @author Thiago Ricciardi
 *
 * @see PersistentHandler
 * @see IPersistentObject
 * @see EntityManager
 * @see MongoOperations
 *
 */
public interface PersistentOperations {

	/**
	 * Gets the Hibernate Session used to do all the YesSQL transactions.<br>
	 * Useful if you want to configure or change something before or after the processes.
	 * @return The Hibernate Session object used to do the transactions.
	 */
	public Session getHibernateSession();

	/**
	 * Gets the EntityManager used to do all the YesSQL transactions.<br>
	 * Useful if you want to configure or change something before or after the processes.
	 * @return The EntityManager object used to do the transactions.
	 */
	public EntityManager getEntityManager();

	/**
	 * Gets the MongoOperations used to do all the NoSQL transactions<br>
	 * Useful if you want to configure or change something before or after the processes.
	 * @return The MongoOperations object used to do the transactions.
	 */
	public MongoOperations getMongoOperations();

	/**
	 * Check if the persistent class is persisted by Hibernate
	 * @param c The class to be checked
	 * @return true if the class is persisted by Hibernate, false otherwise
	 */
	public <T extends IPersistentObject> boolean isHibernate(Class<T> c);

	/**
	 * Check if the persistent object is persisted by Hibernate
	 * @param p The persistent object to be checked
	 * @return true if the persistent object is persisted by Hibernate, false otherwise
	 */
	public <T extends IPersistentObject> boolean isHibernate(T p);

	/**
	 * Check if the persistent object list is persisted by Hibernate
	 * @param l The persistent object list to be checked
	 * @return true if the persistent object list is persisted by Hibernate, false otherwise
	 */
	public <T extends IPersistentObject> boolean isHibernate(List<T> l);

	/**
	 * Check if the persistent class is persisted by MongoDB
	 * @param c The class to be checked
	 * @return true if the class is persisted by MongoDB, false otherwise
	 */
	public <T extends IPersistentObject> boolean isMongo (Class<T> c);
	/**
	 * Check if the persistent object is persisted by MongoDB
	 * @param p The persistent object to be checked
	 * @return true if the persistent object is persisted by MongoDB, false otherwise
	 */
	public <T extends IPersistentObject> boolean isMongo(T p);
	/**
	 * Check if the persistent object list is persisted by MongoDB
	 * @param l The persistent object list to be checked
	 * @return true if the persistent object list is persisted by MongoDB, false otherwise
	 */
	public <T extends IPersistentObject> boolean isMongo(List<T> l);

	/**
	 * Gets the persistent object from the database.
	 * @param p The non persisted or out-dated persistent object.
	 * @return The persisted persistent object.
	 */
	public <T extends IPersistentObject> T get(T p);

	/**
	 * Saves (Inserts or Updates) the persistent object to the database.
	 * @param p The persistent object to be saved.
	 * @return The persistent object that was saved (it can be different).
	 */
	public <T extends IPersistentObject> T save(T p);

	/**
	 * Deletes the persistent object from the database.
	 * @param p The persistent object to be deleted.
	 * @return The persistent object that was deleted (for chain purposes).
	 */
	public <T extends IPersistentObject> T del(T p);

	/**
	 * Saves (Inserts) the persistent object to the database.
	 * @param p The persistent object to be saved.
	 * @return The persistent object that was saved (it can be different).
	 */
	public <T extends IPersistentObject> T insert(T p);

	/**
	 * Saves (Updates) the persistent object to the database.
	 * @param p The persistent object to be saved.
	 * @return The persistent object that was saved (it can be different).
	 */
	public <T extends IPersistentObject> T update(T p);

	/**
	 * Dumps the complete list of persisted objects from the database (all the table).<br>
	 * Be careful, this method can take some time to load from big tables.
	 * @param c The persistent object's class of the objects to get.
	 * @return The list of all objects persisted.
	 */
	public <T extends IPersistentObject> List<T> getAll(Class<T> c);

	/**
	 * Dumps the complete list of persisted objects from the database (all the table).<br>
	 * Be careful, this method can take some time to load from big tables.
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @return The list of all objects persisted.
	 */
	public <T extends IPersistentObject> List<T> getAll(T p);

	/**
	 * Counts all table rows.
	 * @param c The persistent object's class of the objects to get.
	 * @return The number of all objects persisted.
	 */
	public Long countAll(Class<? extends IPersistentObject> c);

	/**
	 * Counts all table rows.
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @return The number of all objects persisted.
	 */
	public Long countAll(IPersistentObject p);

	/**
	 * Gets the list of persisted objects from the database given the object of example (if all parameters are null, gets the whole table) ordered from newest to oldest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param p The persistent object of example.
	 * @return The list of persistent objects that match the example.
	 */
	public <T extends IPersistentObject> List<T> getAllLatest(T p);

	/**
	 * Gets the list of persisted objects from the database given the list of objects of example (if all parameters are null, gets the whole table) ordered from newest to oldest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param l The list of persistent objects of example.
	 * @return The list of persistent objects that match the examples.
	 */
	public <T extends IPersistentObject> List<T> getAllLatest(List<T> l);

	/**
	 * Gets the latest persisted object from the database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the latest of the whole table).
	 * @return The latest persisted object that match the example.
	 */
	public <T extends IPersistentObject> T getLatest(T p);

	/**
	 * Gets the latest persisted object from the database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the latest of the whole table).
	 * @return The latest persisted object that match the examples.
	 */
	public <T extends IPersistentObject> T getLatest(List<T> l);

	/**
	 * Gets the limited list of the latest persisted objects from database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the latest persisted objects that match the example.
	 */
	public <T extends IPersistentObject> List<T> getLatest(T p, int firstRow, int maxRows);

	/**
	 * Gets the limited list of the latest persisted objects from database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the latest persisted objects that match the examples.
	 */
	public <T extends IPersistentObject> List<T> getLatest(List<T> l, int firstRow, int maxRows);

	/**
	 * Gets the list of persisted objects from the database given the object of example (if all parameters are null, gets the whole table) ordered from oldest to newest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param p The persistent object of example.
	 * @return The list of persistent objects that match the example.
	 */
	public <T extends IPersistentObject> List<T> getAllFirst(T p);

	/**
	 * Gets the list of persisted objects from the database given the list of objects of example (if all parameters are null, gets the whole table) ordered from oldest to newest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param l The list of persistent objects of example.
	 * @return The list of persistent objects that match the examples.
	 */
	public <T extends IPersistentObject> List<T> getAllFirst(List<T> l);

	/**
	 * Gets the first persisted object from the database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the first of the whole table).
	 * @return The first persisted object that match the example.
	 */
	public <T extends IPersistentObject> T getFirst(T p);

	/**
	 * Gets the first persisted object from the database according to the versioning system given the list of objects of example.
	 * @param l The persistent object of example (if all parameters are null gets the first of the whole table).
	 * @return The first persisted object that match the examples.
	 */
	public <T extends IPersistentObject> T getFirst(List<T> l);

	/**
	 * Gets the limited list of the first persisted objects from database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the first persisted objects that match the example.
	 */
	public <T extends IPersistentObject> List<T> getFirst(T p, int firstRow, int maxRows);

	/**
	 * Gets the limited list of the first persisted objects from database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the first persisted objects that match the examples.
	 */
	public <T extends IPersistentObject> List<T> getFirst(List<T> l, int firstRow, int maxRows);

	/**
	 * Gets a list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @return A list of persisted objects that matches the example.
	 */
	public <T extends IPersistentObject> List<T> getList(T p);

	/**
	 * Gets a list of persistent objects given the list of objects of example.
	 * @param l  The list of persistent objects of example.
	 * @return A list of persisted objects that match the examples.
	 */
	public <T extends IPersistentObject> List<T> getList(List<T> l);

	/**
	 * Gets a limited list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that matches the example.
	 */
	public <T extends IPersistentObject> List<T> getList(T p, int firstRow, int maxRows);

	/**
	 * Gets a limited list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match the examples.
	 */
	public <T extends IPersistentObject> List<T> getList(List<T> l, int firstRow, int maxRows);

	/**
	 * Counts a list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @return The number of persisted objects that matches the example.
	 */
	public Long countList(IPersistentObject p);

	/**
	 * Counts a list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return The number of persisted objects that matches the examples.
	 */
	public Long countList(List<IPersistentObject> l);

	/**
	 * Gets a list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @return A list of persisted objects that match like the example.
	 */
	public <T extends IPersistentObject> List<T> getLike(T p);

	/**
	 * Gets a list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return A list of persisted objects that match like the examples.
	 */
	public <T extends IPersistentObject> List<T> getLike(List<T> l);

	/**
	 * Gets a limited list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match like the example.
	 */
	public <T extends IPersistentObject> List<T> getLike(T p, int firstRow, int maxRows);

	/**
	 * Gets a limited list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match like the examples.
	 */
	public <T extends IPersistentObject> List<T> getLike(List<T> l, int firstRow, int maxRows);

	/**
	 * Counts a list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @return The number of persisted objects that matches like the example.
	 */
	public Long countLike(IPersistentObject p);

	/**
	 * Counts a list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return The number of persisted objects that matches like the examples.
	 */
	public Long countLike(List<IPersistentObject> l);

	/**
	 * Saves the entire list of persistent objects.<br>
	 * Basically: for each element -> save it!.
	 * @param l The list of persistent objects to save.
	 * @return The saved list of objects.
	 */
	public <T extends IPersistentObject> List<T> saveList(List<T> l);

	/**
	 * Deletes an entire list of persistent objects.
	 * Basically: for each element -> delete it!.
	 * @param l The list of persistent objects to delete.
	 * @return The list of objects deleted.
	 */
	public <T extends IPersistentObject> List<T> delList(List<T> l);

	/**
	 * Evicts the persistent object from transactions.<br>
	 * The evicted persistent object turns into a normal object.
	 * @param p The persistent object to be evicted.
	 */
	public void evict(IPersistentObject p);

	/**
	 * Used with CriteriaQuery and TypedQuery to create a more elaborated query.
	 * @return a CriteriaBuilder instance
	 * @see CriteriaQuery
	 * @see TypedQuery
	 */
	public CriteriaBuilder getCriteriaBuilder();

	/**
	 * Create a TypedQuery instance from a CriteriaQuery to execute a criteria query
	 * @param q the CriteriaQuery
	 * @return the new TypedQuery instance
	 * @see CriteriaBuilder
	 */
	public <T> TypedQuery<T> createQuery(CriteriaQuery<T> q);

	/**
	 * Create a Query instance from a CriteriaUpdate to execute a criteria update query
	 * @param u the CriteriaUpdate
	 * @return the new Query instance
	 * @see CriteriaBuilder
	 */
	public <T> Query createQuery(CriteriaUpdate<T> u);

	/**
	 * Create a Query instance from a CriteriaDelete to execute a criteria delete query
	 * @param d the CriteriaDelete
	 * @return the new Query instance
	 * @see CriteriaBuilder
	 */
	public <T> Query createQuery(CriteriaDelete<T> d);

	/**
	 * Creates a criteria to use with that persistent object class.<br>
	 * For advanced database uses. (Searches, grouping, etc).
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @return The Criteria object that can be used.
	 */
	@Deprecated
	public DetachedCriteria createCriteria(IPersistentObject p);
	/**
	 * Creates a criteria to use with that persistent object class.<br>
	 * For advanced database uses. (Searches, grouping, etc).
	 * @param c The persistent object's class.
	 * @return The Criteria object that can be used.
	 */
	@Deprecated
	public DetachedCriteria createCriteria(Class<? extends IPersistentObject> c);

	/**
	 * Does a search using the criteria created.
	 * @param dc The Criteria used to do the search.
	 * @return A list containing elements that match the criteria.
	 */
	@Deprecated
	public List<? extends IPersistentObject> findByCriteria(DetachedCriteria dc);

	/**
	 * Does a search using the criteria created.
	 * @param dc The Criteria used to do the search.
	 * @param firstResult The index of the first result returned (0 based).
	 * @param maxResults The maximum number of items to return.
	 * @return A list containing elements that match the criteria.
	 */
	@Deprecated
	public List<? extends IPersistentObject> findByCriteria(DetachedCriteria dc, int firstResult, int maxResults);

	/**
	 * Returns a single result of a criteria search. (Normally used with aggregations, like MAX and COUNT)
	 * @param dc The criteria used to get the result.
	 * @return A single object, it can be of any type. ie.: count normally returns an Integer object, max usually returns the same type of the column.
	 */
	@Deprecated
	public Object uniqueResult(DetachedCriteria dc);

}
