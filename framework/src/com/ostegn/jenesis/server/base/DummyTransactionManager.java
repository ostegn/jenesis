package com.ostegn.jenesis.server.base;

import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionStatus;

@SuppressWarnings("serial")
public class DummyTransactionManager  extends AbstractPlatformTransactionManager {

	public DummyTransactionManager() {}

	@Override
	protected Object doGetTransaction() throws TransactionException {
		return null;
	}

	@Override
	protected void doBegin(Object transaction, TransactionDefinition definition) throws TransactionException {
		
	}

	@Override
	protected void doCommit(DefaultTransactionStatus status) throws TransactionException {
		
	}

	@Override
	protected void doRollback(DefaultTransactionStatus status) throws TransactionException {
		
	}

}
