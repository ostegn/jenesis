package com.ostegn.jenesis.server.base;

import java.util.Iterator;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.DataSourceLookupFailureException;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate5.HibernateOperations;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ClassUtils;

import com.mongodb.ClientSessionOptions;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientException;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.ClientSession;

/**
 * Provides a convenient base for configuring web security<br>
 * Add auto-wired {@linkplain UserDetailsService} and {@link PasswordEncoder}<br>
 * Create a {@link DataSource} from properties attributes (JNDI with properties fallback)<br>
 * Create a {@link FactoryBean} as factory bean for Hibernate<br><br>
 * 
 * Allows further configuration by overriding methods
 * @author Thiago Ricciardi
 *
 */
@Configuration
@EnableWebSecurity
@EnableAspectJAutoProxy
@EnableTransactionManagement
@EnableGlobalMethodSecurity(securedEnabled=true)
@ComponentScan({"com.ostegn.jenesis.server.base", "com.ostegn.jenesis.server.security","com.ostegn.jenesis.server"})
public abstract class JenesisWebConfigBase extends WebSecurityConfigurerAdapter {
	
	protected transient Log logger = LogFactory.getLog(ClassUtils.getUserClass(getClass()));
	
	@Autowired protected Environment env;
	
	@Autowired protected UserDetailsService userDetailsService;
	@Autowired protected PasswordEncoder passwordEncoder;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info(String.format("Using UserDetailsService: %1$s", ClassUtils.getUserClass(userDetailsService.getClass())));
			logger.info(String.format("Using PasswordEncoder: %1$s", ClassUtils.getUserClass(passwordEncoder.getClass())));
		}
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}
	
	/**
	 * Override this method to add persistent packages to be scanned
	 * @return a string array of packages to scan
	 */
	protected abstract String[] packagesToScan();
	
	/**
	 * Override this method to load different Hibernate properties
	 * @return A set of hibernate properties
	 */
	protected Properties getHibernateProperties() {
		if (hibernateProperties == null) {
			String dialect = env.getProperty("sql.dialect");
			hibernateProperties = new Properties();
			for (Iterator<PropertySource<?>> it = ((AbstractEnvironment) env).getPropertySources().iterator(); it.hasNext();) {
				PropertySource<?> propertySource = it.next();
				if (propertySource instanceof MapPropertySource) {
					hibernateProperties.putAll(((MapPropertySource) propertySource).getSource());
				}
			}
			if (dialect != null) hibernateProperties.putIfAbsent("hibernate.dialect", dialect);
			hibernateProperties.putIfAbsent("hibernate.hbm2ddl.auto", "validate");
			hibernateProperties.putIfAbsent("hibernate.id.new_generator_mappings", "true");
		}
		return hibernateProperties;
	}
	private Properties hibernateProperties;
	
	/**
	 * Override this method to configure a DataSource object for Hibernate<br>
	 * This method get the property jndi.sql to find a JNDI DataSource from server's configuration. If it doesn't, then fallback to a properties configuration. 
	 * @return a DataSource object representing the SQL data source
	 */
	@Bean
	public DataSource dataSource() {
		DataSource dataSource = null;
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		String jndi = env.getProperty("jndi.datasource");
		if (jndi != null) {
			try {
				dataSource = dataSourceLookup.getDataSource(jndi);
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("Data source [%1$s] JNDI is: %2$s", dataSource, jndi));
				}
			}
			catch (DataSourceLookupFailureException ex) {
				if (logger.isWarnEnabled()) {
					logger.warn(String.format("JNDI Data source [%1$s] was not found, falling back to properties configuration: %2$s", jndi, ex.getLocalizedMessage()));
				}
			}
		}
		if (dataSource == null) {
			String url = env.getProperty("sql.url");
			String user = env.getProperty("sql.user");
			String password = env.getProperty("sql.password");
			String driver = env.getProperty("sql.driver");
			if (url != null && user != null && password != null && driver != null) {
				dataSource = new DriverManagerDataSource();
				DriverManagerDataSource ds = (DriverManagerDataSource) dataSource;
				ds.setUrl(url);
				ds.setUsername(user);
				ds.setPassword(password);
				ds.setDriverClassName(driver);
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("Data source [%1$s] URL is: %2$s", dataSource, ds.getUrl()));
				}
			}
		}
		return dataSource;
	}
	
	/**
	 * Override this method to create a Hibernate Configuration
	 * @return a Hibernate {@link LocalSessionFactoryBuilder} bean
	 */
	@Bean
	public org.hibernate.cfg.Configuration hibernateConfig() {
		LocalSessionFactoryBuilder sessionFactoryBuilder = null;
		DataSource dataSource = dataSource();
		String[] packagesToScan = packagesToScan();
		if (dataSource != null && packagesToScan != null) {
			sessionFactoryBuilder = new LocalSessionFactoryBuilder(dataSource);
			sessionFactoryBuilder.scanPackages(packagesToScan);
			sessionFactoryBuilder.addProperties(getHibernateProperties());
		}
		return sessionFactoryBuilder;
	}
	
	/**
	 * Override this method to build a Hibernate Session Factory
	 * @return a Hibernate Session Factory
	 */
	@Bean
	public SessionFactory sessionFactory() {
		SessionFactory sessionFactory = null;
		org.hibernate.cfg.Configuration config = hibernateConfig();
		if (config != null) {
			sessionFactory = config.buildSessionFactory();
		}
		if (sessionFactory == null && logger.isWarnEnabled()) logger.warn("Hibernate SessionFactory was not configured correctly, YesSQL is not available.");
		return sessionFactory;
	}
	
	/**
	 * This method is deprecated, use sessionFactory().getCurrentSession() instead.<br><br>
	 * Override this method to configure a Hibernate Operations instance
	 * @return a {@link HibernateTemplate} bean
	 */
	@Bean
	@Deprecated
	public HibernateOperations hibernateOperations() {
		HibernateOperations hibernateOperations = null;
		SessionFactory sessionFactory = sessionFactory();
		if (sessionFactory != null) hibernateOperations = new HibernateTemplate(sessionFactory);
		if (hibernateOperations == null && logger.isInfoEnabled()) logger.info("HibernateOperations was not configured.");
		return hibernateOperations;
	}
	
	/**
	 * Override this method to configure the Transaction Manager instance
	 * @return a {@link PlatformTransactionManager} bean
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {
		PlatformTransactionManager transactionManager = null;
		SessionFactory sessionFactory = sessionFactory();
		MongoDbFactory mongoDbFactory = mongoDbFactory();
		PlatformTransactionManager hibernateTM = null;
		PlatformTransactionManager mongoTM = null;
		if (sessionFactory != null) hibernateTM = new HibernateTransactionManager(sessionFactory);
		if (mongoDbFactory != null) {
			ClientSession session = null;
			try {
				session = mongoDbFactory.getSession(ClientSessionOptions.builder().build());
			} catch (MongoClientException e) {
				if (logger.isWarnEnabled()) {
					logger.warn(String.format("Unable to get Mongo session. Mongo transactions are not available: %1$s", e.getMessage()));
				}
			}
			if (session != null) {
				session.close();
				mongoTM = new MongoTransactionManager(mongoDbFactory);
			}
		}
		if (hibernateTM != null && mongoTM != null) transactionManager = new ChainedTransactionManager(hibernateTM, mongoTM);
		else if (hibernateTM != null) transactionManager = hibernateTM;
		else if (mongoTM != null) transactionManager = mongoTM;
		else transactionManager = new DummyTransactionManager();
		if (transactionManager instanceof DummyTransactionManager && logger.isWarnEnabled()) logger.warn("No Transaction Manager available, configuring a DummyTransactionManager.");
		if (logger.isInfoEnabled()) logger.info(String.format("Transaction manager is %1$s", ClassUtils.getUserClass(transactionManager.getClass())));
		return transactionManager;
	}
	
	/**
	 * Override this method to configure a MongoOperations instance
	 * @return a {@link MongoTemplate} bean
	 */
	@Bean
	public MongoOperations mongoOperations() {
		MongoOperations mongoOperations = null;
		MongoDbFactory mongoDbFactory = mongoDbFactory();
		if (mongoDbFactory != null) {
			mongoOperations = new MongoTemplate(mongoDbFactory);
		}
		if (mongoOperations == null && logger.isWarnEnabled()) {
			logger.warn("MongoOperations was not configured correctly, NoSQL is not available.");
		}
		return mongoOperations;
	}
	
	/**
	 * Override this method to configure a MongoDbFactory instance
	 * @return a {@link SimpleMongoDbFactory} bean
	 */
	@Bean
	public MongoDbFactory mongoDbFactory() {
		MongoDbFactory mongoDbFactory = null;
		MongoClientURI mongoClientURI = mongoClientURI();
		if (mongoClientURI != null) {
			String mongoDB = env.getProperty("mongo.db");
			try {
				if ( (mongoDB != null) ? mongoDB.length() > 0 : false ) {
					mongoDbFactory = new SimpleMongoDbFactory(new MongoClient(mongoClientURI), mongoDB);
				}
				else {
					mongoDbFactory = new SimpleMongoDbFactory(mongoClientURI);
				}
			}
			catch (MongoException ex) {
				if (logger.isErrorEnabled()) {
					logger.error(String.format("%1$s thrown while building MongoDbFactory: %2$s", ex.getClass(), ex.getLocalizedMessage()), ex);
				}
			}
		}
		return mongoDbFactory;
	}
	
	/**
	 * Override this method to configure a MongoClientURI instance
	 * @return a {@link MongoClientURI} bean
	 */
	@Bean
	public MongoClientURI mongoClientURI() {
		String mongoURI = env.getProperty("mongo.uri");
		MongoClientURI mongoClientURI = null;
		if ( (mongoURI != null) ? mongoURI.length() > 0 : false ) {
			mongoClientURI = new MongoClientURI(mongoURI);
		}
		if (logger.isDebugEnabled()) logger.debug(String.format("Mongo URI is: %1$s", mongoClientURI));
		return mongoClientURI;
	}

}
