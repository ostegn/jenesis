package com.ostegn.jenesis.server;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.server.rpc.RPCRequest;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * Implements the basic Jenesis servlet extending the GWT remote service servlet
 * @author Thiago Ricciardi
 *
 */
public class JenesisServlet extends RemoteServiceServlet {

	private static final long serialVersionUID = 1L;

	protected transient Log logger = LogFactory.getLog(ClassUtils.getUserClass(getClass()));

	@Override
	public void init() {
		if (logger.isDebugEnabled()) {
			logger.debug("Spring <-> GWT service deployed");
		}
	}

	@Override
	public String processCall(String payload) throws SerializationException {
		// First, check for possible XSRF situation
		checkPermutationStrongName();
		try {
			Object handler = getBean(getThreadLocalRequest());
			RPCRequest rpcRequest = RPC.decodeRequest(payload, handler.getClass(), this);
			onAfterRequestDeserialized(rpcRequest);
			if (logger.isDebugEnabled()) {
				logger.debug("Invoking " + ClassUtils.getUserClass(handler.getClass()).getName() + "." + rpcRequest.getMethod().getName());
			}
			return RPC.invokeAndEncodeResponse(handler, rpcRequest.getMethod(), rpcRequest.getParameters(), rpcRequest.getSerializationPolicy(), rpcRequest.getFlags());
		} catch (IncompatibleRemoteServiceException ex) {
			log("An IncompatibleRemoteServiceException was thrown while processing this call.", ex);
			return RPC.encodeResponseForFailure(null, ex);
		} catch (RpcTokenException tokenException) {
			log("An RpcTokenException was thrown while processing this call.", tokenException);
			return RPC.encodeResponseForFailedRequest(null, tokenException);
		}
	}

	/**
	 * Determine Spring bean to handle request based on request URL, e.g. a
	 * request ending in /myService will be handled by bean with name
	 * "myService".
	 *
	 * @param request the servlet request
	 * @return handler bean
	 */
	protected Object getBean(HttpServletRequest request) throws IllegalStateException, IllegalArgumentException, BeansException {
		String service = getService(request);
		Object bean = getBean(service);
		if (!(bean instanceof RemoteService)) {
			throw new IllegalArgumentException("Spring bean is not a GWT RemoteService: " + service + " (" + bean + ")");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Bean for service " + service + " is " + bean);
		}
		return bean;
	}

	/**
	 * Parse the service name from the request URL.
	 *
	 * @param request the servlet request
	 * @return bean name
	 */
	protected String getService(HttpServletRequest request) {
		String url = request.getRequestURI();
		String service = url.substring(url.lastIndexOf("/") + 1);
		if (logger.isDebugEnabled()) {
			logger.debug("Service for URL " + url + " is " + service);
		}
		return service;
	}

	/**
	 * Look up a spring bean with the specified name in the current web
	 * application context.
	 *
	 * @param name Bean name
	 * @return the Bean
	 */
	protected Object getBean(String name) throws IllegalStateException, IllegalArgumentException, BeansException {
		WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		if (applicationContext == null) {
			throw new IllegalStateException("No Spring web application context found");
		}
		if (!applicationContext.containsBean(name)) {
			throw new IllegalArgumentException("Spring bean not found: " + name);
		}
		return applicationContext.getBean(name);
	}

}
