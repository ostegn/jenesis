package com.ostegn.jenesis.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.shared.base.ICredential;

/**
 * 
 * This is a simple implementation of an UserDetails service, part of spring framework.<br>
 * It's used with Jenesis Framework to simplify things.
 * 
 * @author Thiago Ricciardi
 *
 */
@Repository
@Transactional(readOnly=true)
@Configuration
@ComponentScan("com.ostegn.jenesis.server.base")
public abstract class JenesisAuth implements UserDetailsService {
	
	@Autowired
	protected PersistentOperations persistentHandler;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		ICredential c = getCredentialByID(username);
		if (c == null) throw new UsernameNotFoundException("Credential '" + username + "' not found!");
		return new JenesisUserDetails(c);
	}
	
	/**
	 * Gets the credential given a user ID.
	 * @param id The user ID (can be an e-mail, username, phone number, etc... Depends on the system needs.)
	 * @return A credential, or {@code null} if not found.
	 */
	public abstract ICredential getCredentialByID(String id);

}
