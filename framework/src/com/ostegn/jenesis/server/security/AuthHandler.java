package com.ostegn.jenesis.server.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.shared.base.ICredential;

/**
 * This class implements the basic Authentication of Jenesis Framework
 * @author Thiago Ricciardi
 * @see AuthenticationSuccessHandler
 * @see AuthenticationFailureHandler
 * @see LogoutSuccessHandler
 */
@Configuration
@ComponentScan("com.ostegn.jenesis.server.base")
public class AuthHandler implements AuthenticationSuccessHandler, AuthenticationFailureHandler, LogoutSuccessHandler {
	
	@Autowired private PersistentOperations ph;
	
	private RequestCache requestCache = new HttpSessionRequestCache();
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<auth-response result=\"FAILURE\" message=\"" + exception.getMessage() + "\"/>");
		out.flush();
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		//Session -> request.getSession().getId()
		
		String redirect = "";
		if (requestCache != null) {
			SavedRequest savedRequest = requestCache.getRequest(request, response);
			if (savedRequest != null) redirect = savedRequest.getRedirectUrl();
		}
		
		ICredential c = AuthHelper.getCredential(authentication);
		if (c != null) {
			c = ph.get(c);
			c.onSignIn();
			ph.save(c);
		}
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<auth-response result=\"SUCCESS\" redirect=\"" + redirect + "\"/>");
		out.flush();
	}

	/**
	 * Set the request cache to be used with redirection
	 * @param requestCache the request cache
	 */
	public void setRequestCache(RequestCache requestCache) {
		this.requestCache = requestCache;
	}

	@Override
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		ICredential c = AuthHelper.getCredential(authentication, false);
		if (c != null) {
			c = ph.get(c);
			c.onSignOut();
			ph.save(c);
		}
		
		response.sendRedirect(request.getContextPath() + "/");
		
	}
	
}
