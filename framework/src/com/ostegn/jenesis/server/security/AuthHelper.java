package com.ostegn.jenesis.server.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ostegn.jenesis.shared.base.ICredential;

/**
 * A basic authentication helper class
 * @author Thiago Ricciardi
 *
 */
public class AuthHelper {
	
	/**
	 * Get the credential of the logged user
	 * @return the credential or null if not authenticated
	 */
	public static ICredential getCredential() {
		ICredential ret = null;
		SecurityContext sc = SecurityContextHolder.getContext();
		if (sc != null) {
			ret = getCredential(sc.getAuthentication());
		}
		return ret;
	}
	
	/**
	 * Get the credential of the logged user given the authentication
	 * @param auth the authentication
	 * @return the credential or null if not authenticated
	 */
	public static ICredential getCredential(Authentication auth) {
		return getCredential(auth, true);
	}
	
	/**
	 * Get the credential of the logged user given the authentication and if we must check for the authentication token
	 * @param auth the authentication
	 * @param verifyAuth if we need to check for the authentication token to return a valid credential
	 * @return If verifyAuth is true: the credential or null if not authenticated.<br>If verifyAuth is false: the credential even if the authentication token has expired or null if no user has logged in.
	 */
	public static ICredential getCredential(Authentication auth, Boolean verifyAuth) {
		ICredential ret = null;
		if ( (auth != null) ? (verifyAuth ? auth.isAuthenticated() : true) : false ) {
			Object principal = auth.getPrincipal();
			if (principal instanceof JenesisUserDetails && principal != null) {
				ret = ((JenesisUserDetails) principal).getCredential();
			}
		}
		return ret;
	}

}
