package com.ostegn.jenesis.server.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.ostegn.jenesis.shared.base.ICredential;

/**
 * Implements the basic user details.<br>
 * <b>If you use the Jenesis security package, you will need that your credentials implement {@link ICredential}
 * @author Thiago Ricciardi
 *
 */
@SuppressWarnings("serial")
public class JenesisUserDetails implements UserDetails {
	
	private ICredential c;
	
	/** Create a new instance of JenesisUserDetails with an empty credential */
	public JenesisUserDetails() {}
	/**
	 * Create a new instance of JenesisUserDetails with a credential defined
	 * @param credential the credential
	 */
	public JenesisUserDetails(ICredential credential) {
		c = credential;
	}
	
	/**
	 * Get the credential
	 * @return the credential
	 */
	public ICredential getCredential() {
		return c;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		ArrayList<SimpleGrantedAuthority> ret = new ArrayList<SimpleGrantedAuthority>();
		for (String role : c.getJAuthorities()) {
			ret.add(new SimpleGrantedAuthority(role));
		}
		return ret;
	}

	@Override
	public String getPassword() {
		return c.getPassword();
	}

	@Override
	public String getUsername() {
		return c.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return c.isAccountNonExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return c.isAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return c.isCredentialsNonExpired();
	}

	@Override
	public boolean isEnabled() {
		return c.isEnabled();
	}

}
