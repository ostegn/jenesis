package com.ostegn.jenesis.server.security;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class implements a basic Jenesis encryption
 * @author Thiago Ricciardi
 *
 */
public final class JenesisCrypt {
	
	private static final String ALGORITHM = "SHA-256";
	
	/**
	 * Encrypt the string using Jenesis Encryption algorithm using no salt object
	 * @param decrypted the decrypted string to be encrypted
	 * @return the decrypted string encrypted by Jenesis Encryption algorithm
	 * @throws NoSuchAlgorithmException if the underlying algorithm used is not supported
	 */
	public static final String EnCrypt(String decrypted) throws NoSuchAlgorithmException
	{
		return EnCrypt(decrypted, null);
	}
	
	/**
	 * Encrypt the string using Jenesis Encryption algorithm using a salt object
	 * @param decrypted the decrypted string to be encrypted
	 * @param salt the salt object
	 * @return the decrypted string encrypted by Jenesis Encryption algorithm
	 * @throws NoSuchAlgorithmException if the underlying algorithm used is not supported
	 */
	public static final String EnCrypt(String decrypted, Object salt) throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance(ALGORITHM);
		md.reset();
		md.update("J3N3S1S".getBytes());
		md.update(decrypted.getBytes());
		md.update((byte) decrypted.hashCode());
		if (salt != null) md.update((byte) salt.hashCode());
		BigInteger bi = new BigInteger(1, md.digest());
		return bi.toString(16);
	}
	
	/**
	 * Encrypt the string using Jenesis Encryption algorithm using no salt object
	 * @param decrypted the decrypted char sequence to be encrypted
	 * @return the decrypted char sequence in a string encrypted by Jenesis Encryption algorithm
	 * @throws NoSuchAlgorithmException if the underlying algorithm used is not supported
	 */
	public static final String EnCrypt(CharSequence decrypted) throws NoSuchAlgorithmException
	{
		return EnCrypt(decrypted.toString());
	}
	
	/**
	 * Encrypt the string using Jenesis Encryption algorithm using a salt object
	 * @param decrypted the decrypted char sequence to be encrypted
	 * @param salt the salt object
	 * @return the decrypted char sequence in a string encrypted by Jenesis Encryption algorithm
	 * @throws NoSuchAlgorithmException if the underlying algorithm used is not supported
	 */
	public static final String EnCrypt(CharSequence decrypted, Object salt) throws NoSuchAlgorithmException
	{
		return EnCrypt(decrypted.toString(), salt);
	}
}