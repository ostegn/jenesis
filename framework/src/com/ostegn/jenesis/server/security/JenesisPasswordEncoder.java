package com.ostegn.jenesis.server.security;

import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Implements the basic password encoder using JenesisCrypt
 * @author Thiago Ricciardi
 * @see JenesisCrypt
 */
public class JenesisPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		try {
			return JenesisCrypt.EnCrypt(rawPassword);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encodedPassword.equalsIgnoreCase(encode(rawPassword));
	}

}
