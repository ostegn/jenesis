package com.ostegn.jenesis.server.task;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Implements the base of all Jenesis Tasks
 * @author Thiago Ricciardi
 *
 */
public abstract class TaskBase implements HasTask {
	
	private Timer timer;
	
	@Override
	public void beforeStartup() throws Exception {}
	
	@Override
	public void startup() throws Exception {
		beforeStartup();
		if (timer == null) timer = new Timer(this.getClass().getName(), true);
		TimerTask task = getTask();
		Date startupTime = getStartupTime();
		Long interval = getIntervalMilliseconds();
		if (interval > 0) {
			while (startupTime.before(new Date())) {
				startupTime.setTime(startupTime.getTime() + interval);
			}
			timer.schedule(task, startupTime, interval);
		}
		else {
			timer.schedule(task, startupTime);
		}
	}
	
	@Override
	public void shutdown() throws Exception {
		timer.cancel();
		timer = null;
		afterShutdown();
	}
	
	@Override
	public void afterShutdown() throws Exception {}
	
	@Override
	public void restart() throws Exception {
		shutdown();
		startup();
	}

}

