package com.ostegn.jenesis.server.task;

import java.util.Date;
import java.util.TimerTask;

/**
 * Define the task interface to be used with Jenesis Tasks
 * @author Thiago Ricciardi
 *
 */
public interface HasTask {
	
	/**
	 * The timer's task to be executed
	 * @return the timer's task to be executed
	 * @throws Exception if some error occur
	 */
	TimerTask getTask() throws Exception;
	
	/**
	 * The task (future) startup date
	 * @return a Date object representing this task startup date
	 * @throws Exception if some error occur
	 */
	Date getStartupTime() throws Exception;
	
	/**
	 * The task time interval in milliseconds
	 * @return an interval in milliseconds
	 * @throws Exception if some error occur
	 */
	Long getIntervalMilliseconds() throws Exception;
	
	/**
	 * Override this function to execute something before the task starts
	 * @throws Exception if some error occur
	 */
	void beforeStartup() throws Exception;
	
	/**
	 * Override this function to change the startup behavior of the task
	 * @throws Exception if some error occur
	 */
	void startup() throws Exception;
	
	/**
	 * Override this function to change the shutdown behavior of the task
	 * @throws Exception if some error occur
	 */
	void shutdown() throws Exception;
	
	/**
	 * Override this function to execute something after the task stops
	 * @throws Exception if some error occur
	 */
	void afterShutdown() throws Exception;
	
	/**
	 * Override this function to recreate the restart operation.<br>
	 * By default this performs a shutdown() followed by a startup().
	 * @throws Exception if some error occur
	 */
	void restart() throws Exception;
	
}
